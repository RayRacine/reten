(*signature PG =
sig

type PGByte8
type PGByte16

type PGInt32
type PGString
end *)

structure PG =
struct

val cancelw1 = Word.fromInt 1234
val cancelw2 = Word.fromInt 6789

val buffer = let val buff = Word8Array.array (8,0)
	     in
		 PackWord32Big (buff,0,cancelw1);
		 PackWord32Big (buff,4,cancelw2);
		 buff
	     end

fun connect port =
    let
	val localhost =
            valOf(NetHostDB.fromString "127.0.0.1")
	val addr = INetSock.toAddr(localhost, port)
	val sock = INetSock.TCP.socket()
		   
	fun call sock =
	    let
		val _    = Socket.connect(sock, addr)
		val msg  = Socket.sendVec(sock, 1000)
		val text = Byte.bytesToString msg
	    in
		print text;
		Socket.close sock
	    end
		handle x => (Socket.close sock; raise x)
    in
	call sock
    end	handle OS.SysErr (msg, _) => raise Fail (msg ^ "\n")

end
