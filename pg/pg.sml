(*signature PG =
sig

type PGByte8
type PGByte16

type PGInt32
type PGString
end *)

structure PG =
struct

structure PW32 = PackWord32Big

val cancelw1 = Word64.fromInt 12346789
val cancelw2 = Word64.fromInt 6789

val buffer = let val buff = Word8Array.array (16,Word8.fromInt 0)
	     in
		 PW32.update (buff,0,Word64.fromInt 80877103);
		 PW32.update (buff,1,Word64.fromInt 0); 
		 buff
	     end

fun connect port =
    let	val localhost = valOf(NetHostDB.fromString "127.0.0.1")
	val addr = INetSock.toAddr(localhost, port)
	val sock = INetSock.TCP.socket()		   
	fun call sock = let val _ = Socket.connect(sock, addr)
			    val _ = Socket.sendArr(sock, Word8ArraySlice.full buffer)
			in
			    Socket.close sock
			end
			    handle x => (Socket.close sock; raise x)
    in
	call sock
    end	handle OS.SysErr (msg, _) => raise Fail (msg ^ "\n")					   
end

val _ = PG.connect 22
