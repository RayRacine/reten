structure Util: UTIL =
struct

type 'a eq = 'a * 'a -> bool

(* Simple function to generate a unique anonymous identifier.
 * genId() -> _:a0 *)
val genId = let val counter = Counter.new()
		val prefix = "_:a"
	    in
		fn () => prefix ^ (Int.toString (Counter.next counter))
	    end

(*  merge an element into a set list of elems *)
fun mergeElement eq x lst =
    let fun merge' x [] = x :: lst
	  | merge' x (l::ls) = if eq (x, l) then lst else merge' x ls 
    in
	merge' x lst
    end
	  
  fun mergeLists _ [] l = l
    | mergeLists _ l [] = l
    | mergeLists eq l1 l2 = foldl (fn (e,l) => mergeElement eq e l) l2 l1


end
