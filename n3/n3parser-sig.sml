signature N3_PARSER = 
sig
    structure D: DOCUMENT

    val n3ParseFile: string -> D.n3document
    val tripleParseFile: string -> D.tripledocument
end
