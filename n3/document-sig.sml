signature DOCUMENT =
sig

    structure D: DECLARATION
    structure Q: QUANTIFIER
    structure N: N3_SIG
    structure T: TRIPLE

    type n3document = {declarations: D.declaration list, quantifiers: Q.quantifier list, n3: N.n3 list}
		      
    type tripledocument = {declarations: D.declaration list, quantifiers: Q.quantifier list, triples: T.triple list}

    val filterN3Document: n3document -> (N.n3 -> bool) -> n3document
			  
end
