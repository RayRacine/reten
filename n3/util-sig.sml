signature UTIL =
sig
    
    type 'a eq = 'a * 'a -> bool

    val genId: unit -> string
    val mergeElement: 'a eq -> 'a -> 'a list -> 'a list
    val mergeLists: 'a eq -> 'a list -> 'a list -> 'a list
					  
end
