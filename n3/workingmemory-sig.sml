signature WORKING_MEMORY =
sig

structure N3: N3_SIG
structure T: TRIPLE
	     
type working_memory
     
val addTriple: working_memory * T.triple -> unit
val addN3: working_memory * N3.n3 -> unit
val fromTripleList: T.triple list -> working_memory
val toN3List: working_memory -> N3.n3 list

end
