structure Property: PROPERTY =
struct

structure N = Node
structure U = Util	
      
type property = N.node * N.node list

		
fun mkProperty pred objs = (pred, objs)

fun propertyValues prop = prop

fun extendProperty (pred, objs) obj = (pred, U.mergeElement N.equal obj objs)

fun predicate (p,_) = p

fun predicateEq ((p1,_),(p2,_)) = N.equal (p1,p2)

(* merge objects for a common property *)
fun mergeObjects prop1 prop2 =
    let val (pred, objs1) = prop1
	val (_ , objs2) = prop2
    in
	(pred, U.mergeLists N.equal objs1 objs2)
    end
	
(************************************************
 * merge a new property into an existing properties
 * if the property does not exist its an add 
 *************************************************)
fun mergeProperty (prop,[]) = [prop]
  | mergeProperty (prop,props) = let fun mp [] _  = prop :: props
				       | mp (p::ps) accum = 
					 if predicateEq (prop,p)
					 then ((mergeObjects prop p)::ps) @ accum
					 else mp ps (p::accum)
				 in
				     mp props []
				 end

fun findObject (pred,objs) selector = List.find selector objs
				       
end
