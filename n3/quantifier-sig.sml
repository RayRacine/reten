signature QUANTIFIER =
sig
    datatype quantifier = ForAll of string list | ForSome of string list
end
