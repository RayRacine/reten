structure N3Tests = 
struct 
  
 (*   fun tests() =			   
      let val t1 = (N3.Lit ":ray", [])
	  val t2 = (N3.Lit ":ray", [(N3.Lit ":eve",[])])
	  val t3 = (N3.Lit ":ray", [(N3.Lit ":eve", [N3.Lit ":racine"])])
	  val t4 = (N3.Lit ":ray", [(N3.Lit ":eve", [N3.Lit ":paul", N3.Lit ":racine"])])
	  val t5 = (N3.Lit ":ray", [(N3.Sym ":eve", [N3.Sym ":paul", N3.Sym ":racine"]),
				    (N3.Sym ":cory", [N3.Sym ":dog", N3.Sym ":racine"])])
      in
	  List.app (N3.ppN3 TextIO.stdOut) [t1,t2,t3,t4,t5]
      end *)
	  
	  

  val p1 = N3.mkProperty (N3.Sym ":isa") [N3.Sym ":man"]
  val p2 = N3.mkProperty (N3.Sym ":owns") [N3.Sym ":car", N3.Sym ":pool"]
  val p3 = N3.mkProperty (N3.Sym ":owns") [N3.Sym ":dog"]
  val p4 = N3.mkProperty (N3.Sym ":isa") [N3.Sym ":man", N3.Sym ":programmer"]

  val n1 = N3.mkN3 (N3.Sym ":ray") [p1,p2]
  val n2 = N3.addProperty n1 p3
  val n3 = N3.addProperty n2 p3
  val n4 = N3.addProperty n3 p4

  val nn1 = N3.mkN3 (N3.Sym ":ray") [p1]
  val nn3 = N3.mkN3 (N3.Sym ":ray") [p3]

  (*  val t5 = (N3.Sym ":ray", [(N3.Sym ":eve", [N3.Sym ":paul", N3.Sym ":racine"]),
				(N3.Sym ":cory", [N3.Sym ":dog", N3.Sym ":racine"])])
      val cls = [(N3.Sym ":isa", [N3.Sym ":man"]),
		 (N3.Sym ":owns", [N3.Sym ":car", N3.Sym ":pool"])]
		
      val t1 = (N3.Sym ":ray", [(N3.Sym ":isa", [N3.Sym ":man"]),
				(N3.Sym ":owns", [N3.Sym ":car", N3.Sym ":pool"])])
	       *)


  val dir = "tests"	    
  val files = [ 
	       (*	       "acl-pf.n3", *)
 	       "animals.n3"
(*	       "animal-simple.n3",
	       "anon-prop.n3",
	       "blue.n3",
	       "red.n3",
	       "green.n3" *)
(*	       "ccppfig2-2a.n3" *)
(*	       "context-object.n3"  can't find where "in" is defined *)
(*	       "contexts.n3"  fails to parse the trival truth statement 
	       "crawlFoaf.n3" *)
(*	       "crawlSeeAlso.n3" 
	       "d11.n3" 
	       "rdfs.n3" *)
(*	       "2002-irs-1040-rules.n3" *)
	       ]
	      
  fun test() = List.map (fn fname => (print ("--> " ^ fname ^ "\n");		 
				      N3Parser.tripleParseFile (dir ^ "/" ^ fname))) files

	   

end

 val _ = (print "here we go\n";
	 N3Tests.test())
