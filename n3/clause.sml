structure Clause: CLAUSE =
struct

structure N = Node

type clause = N.node * N.node

fun mkClause pred obj = (pred, obj)

fun values p = p

fun predicate (p, _) = p

fun object (_, obj) = obj

fun hasPredicate (p,_) n = Node.equal(p,n)

fun hasObject (_,obj) n  = Node.equal(obj,n)

fun predicateEq (p1,_) (p2,_) = Node.equal (p1,p2)

fun objectEq (_,o1) (_,o2) = Node.equal (o1,o2)

fun equal c1 c2 = predicateEq c1 c2 andalso objectEq c1 c2
end
