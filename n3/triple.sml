structure Triple: TRIPLE =
struct

structure N = Node

type triple = N.node * N.node * N.node

fun mkTriple subj pred obj = (subj, pred, obj)
			     
fun tripleValues trip = trip

end
