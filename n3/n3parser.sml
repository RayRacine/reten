
structure N3Parser: N3_PARSER =
struct

  structure D = Document

  structure N3LrVals = N3LrValsFun (structure Token = LrParser.Token)
  structure N3Lex = N3LexFun (structure Tokens = N3LrVals.Tokens)
  structure N3Parser = Join  (structure LrParser = LrParser
			      structure ParserData = N3LrVals.ParserData
			      structure Lex = N3Lex)
		
  (*************************
   * Parse specified N3 file
   *************************)       
  fun n3ParseFile fileName = 
      let val file = TextIO.openIn fileName
	  fun get _ = TextIO.input file
	  fun printError (s,i,_) = TextIO.output(TextIO.stdOut, "Parse error, line " ^ (Int.toString i) ^ ", " ^ s ^ "\n")
	  val lexer = LrParser.Stream.streamify (N3Lex.makeLexer get)
	  val (absyn,_) = N3Parser.parse(30,lexer, printError, ()) 
      in
	  absyn before TextIO.closeIn file
      end
	  
  (************************************
   * Flatten out the N3 nodes
   * Once N3.flattenNodes handles
   * blank node identifies correctly
   * this will be a NTriples flattening.
   *************************************)
  fun tripleParseFile fname = let val {declarations, quantifiers, n3} = n3ParseFile fname
			      in
				  {declarations=declarations, quantifiers=quantifiers,triples=N3.n3sToTriples n3}
			      end
end
