signature N3_SIG =
sig

    structure C: CLAUSE
    structure P: PREDICATE_SET
    structure N: NODE
    structure T: TRIPLE
    
    type n3
	 
    val mkN3: N.node -> P.predicate_set list -> n3

    val tripleToN3: T.triple -> n3

    val n3ToTriples: n3 -> T.triple list

    val n3sToTriples: n3 list -> T.triple list 

    val ppN3: TextIO.outstream -> n3 -> unit
    (* val layoutN3: n3 -> Layout.t *)

    val subject: n3 -> N.node

    val subjectEq: n3 -> n3 -> bool

    val predicateSets: n3 -> P.predicate_set list

    val addPredicateSet: n3 -> P.predicate_set -> n3

    val findPredicateSet: n3 -> N.node -> P.predicate_set option

    val hasClause: n3 -> C.clause -> bool

    val expose: n3 -> N.node * (N.node * N.node list) list
		      
    val merge: n3 -> n3 -> n3
    
end


	     (*    exception NodeError of string *)
	     (*    datatype n3statement = Statement of string * string * string | Declaration of declaration *)
