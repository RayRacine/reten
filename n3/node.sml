structure Node: NODE =
struct

structure Q = Quantifier

datatype node = 
	 Lit of string |
	 Sym of string |
	 Var of string |
	 Number of real |
	 PathList of path list | 
	 PropertyList of (node * node list) list | 
	 BNode of (node * node list) list |
	 Formula of Q.quantifier list * Q.quantifier list * (node * (node * node list) list) list 
and path_segment = Bang | Hat | Node of node
withtype path = path_segment list

(* Obvious FIX ME *)					    			      
fun equal (Lit n1, Lit n2) = n1 = n2
    | equal (Sym s1, Sym s2) = s1 = s2
    | equal (Number n1, Number n2) = Real.== (n1, n2) (* I doubt this means what I think it means *)
    | equal _ = false
		    
fun toString node = 
    case node of
	Lit s => s
      | Var s => s (* prefix with '?' ???*)
      | Sym s => s
      | Number n => Real.toString n
      | PathList p => "PathList"
      | PropertyList _ => "PropertList"
      | BNode bid => "BLANKNODE"
      | Formula _ => "FORMULA"		     

end
