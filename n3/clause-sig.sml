signature CLAUSE =
sig
    structure N: NODE
    
    type clause

    val mkClause:    N.node -> N.node -> clause
    val values:      clause -> N.node * N.node
    val predicate:   clause -> N.node
    val object:      clause -> N.node

    val hasPredicate: clause -> N.node -> bool
    val hasObject:    clause -> N.node -> bool

    val equal:       clause -> clause -> bool
    val predicateEq: clause -> clause -> bool
    val objectEq:    clause -> clause -> bool	 
end
