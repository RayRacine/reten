
structure Quantifier: QUANTIFIER =
struct

datatype quantifier = ForAll of string list | ForSome of string list

end
