signature NODE =
sig

    structure Q: QUANTIFIER

    datatype node = 
	     Lit of string |
	     Sym of string |
	     Var of string |
	     Number of real | 
	     PathList of path list |
	     PropertyList of (node * node list) list |
	     BNode of (node * node list) list |
	     Formula of Q.quantifier list * Q.quantifier list * (node * (node * node list) list) list 
    and path_segment = Bang | Hat | Node of node
    withtype path = path_segment list

    val equal: node * node -> bool
    val toString: node -> string
end
