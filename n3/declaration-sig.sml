signature DECLARATION =
sig
    
    type uri
    type declaration

    val mkDeclaration: string -> uri -> declaration

end
