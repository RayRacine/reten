structure N3Types : sig
    
    type triple
    type declaration
    type n3node

    datatype quantifier = ForAll of string list | ForSome of string list
    datatype n3statement = Statement of triple | Declaration of declaration
    datatype node = 
	     Atom of string |
	     Number of real | 
	     BNode of (node * node list) list |
	     Formula of quantifier list * quantifier list * (node * (node * node list) list) list
		    	     					    	   
end =
struct

type uri = string
type triple = string * string * string
type declaration = {qname: string, uri: uri}

datatype quantifier = ForAll of string list | ForSome of string list
datatype n3statement = Statement of triple | Declaration of declaration
datatype node = 
	 Atom of string |
	 Number of real | 
	 BNode of (node * node list) list |
	 Formula of quantifier list * quantifier list * (node * (node * node list) list) list

type n3node = node * (node * node list) list

end

structure N3ToString =
struct

structure N3T = N3Types

fun nodeToString node = 
    case node of
	N3T.Atom s => s
      | N3T.Number n => Real.toString n
      | N3T.BNode _ => "_b0"
      | N3T.Formula _ => "formula"

end
