
structure N3: N3_SIG  =
struct

  structure U = Util
  structure N = Node
  structure P = PredicateSet
  structure T = Triple
  structure C = Clause

  type n3 = N.node * P.predicate_set list

  fun mkN3 subj props = (subj, props)

  fun tripleToN3 (sub,pred,obj) = (sub,[(pred,[obj])])

  (* Extract data from N3 node *)
  fun subject (subj, _) = subj

  fun predicateSets (_, ps) = ps

  fun expose n3 = n3

  (*************************
   * Flattens a single node. 
   **************************)
  fun n3ToTriples (subj, []) = []
    | n3ToTriples (subj, (pred, []) :: preds) = n3ToTriples (subj, preds)
    | n3ToTriples (subj, (pred, obj::objs) :: preds) = 
      case obj of
	  N.Lit _ => (subj,pred,obj) :: n3ToTriples (subj, (pred, objs) :: preds)
	| N.Number _ => (subj,pred, obj) :: n3ToTriples (subj, (pred, objs) :: preds)
	| N.BNode bnode => let val bid = U.genId()
			   in
			       (subj, pred, N.Sym bid) 
			       :: (n3ToTriples (N.Sym bid, bnode)
				   @ n3ToTriples (subj, (pred, objs) :: preds))
			   end
	| N.Formula _ => (subj, pred, obj) :: n3ToTriples (subj, (pred, objs) :: preds)
	| N.Sym _ => (subj, pred, obj) :: n3ToTriples (subj, (pred,objs) :: preds)
	| N.Var _ => (subj, pred, obj) :: n3ToTriples (subj, (pred,objs) :: preds)
		     
  (* Flatten a list of nodes *)		
  fun n3sToTriples ns = foldl (fn (node, accum) =>  (n3ToTriples node) @ accum) [] ns
			 
  fun findPredicateSet (_,props) node = List.find (fn (pred,_) => N.equal (pred,node)) props

  fun hasClause n3 clause = let val predset = findPredicateSet n3 (C.predicate clause)
			    in
				case predset of
				    SOME ps => P.hasClause ps clause
				  | NONE => false
			    end

  (* add a new clause to an n3 *)
  fun addPredicateSet n3 pset  =
      let val (sub, psets) = n3 
      in
	  (sub, P.merge (pset,psets))
      end

  fun subjectEq (s1,_) (s2,_) = N.equal (s1,s2)

  (* merges n1 props into n2 *)
  fun merge (s1, props1) (s2, props2): n3 =
       (s1, foldl (fn (p,accum) => (P.merge (p,props2))) [] props1)
	
  structure N3PrettyPrint: sig       
      val ppN3: TextIO.outstream -> n3 -> unit
  end =
  struct
    fun nl os = TextIO.output (os,"\n")
    fun space os = TextIO.output (os," ")
    fun indent os = TextIO.output (os,"  ")
    fun writeLn os s = (TextIO.output (os,s); nl os)
    fun terminate os = writeLn os " ."
    fun clauseSep os = TextIO.output (os,"; ")
    fun objSep os = TextIO.output (os,", ")
		    
    fun ppObjects os [] = ()
      | ppObjects os (obj::[]) = TextIO.output (os,(N.toString obj))
      | ppObjects os (obj::objs) = (TextIO.output (os, (N.toString obj));      
				    objSep os;
				    ppObjects os objs)
				   
    fun ppClause os (pred, objs) = (TextIO.output (os,(N.toString pred));
				    space os;
				    ppObjects os objs)
				   
    fun ppClauses _ [] = ()
      | ppClauses os (cl::[]) = (nl os;
				 indent os;
				 ppClause os cl)			     
      | ppClauses os (cl::cls) = (nl os;
				  indent os;
				  ppClause os cl; 
				  clauseSep os;
				  ppClauses os cls)
				 
    fun ppN3 os n3 = (TextIO.output (os,N.toString (subject n3));
		      ppClauses os (predicateSets n3);
		      terminate os)
  end
    
  val ppN3 = N3PrettyPrint.ppN3
		

(* structure LayoutN3 =
struct

fun layoutClauses ([],lo) = lo
  | layoutClauses (

fun layoutN3 n3 = L.align [L.str (N.toString s(subject n3)),
			   L.indent ( *)

	
end
  
