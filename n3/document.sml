structure Document =
struct

structure D = Declaration
structure Q = Quantifier
structure N = N3
structure T = Triple
	    
type n3document = {declarations: D.declaration list, quantifiers: Q.quantifier list, n3: N.n3 list}
		  
type tripledocument = {declarations: D.declaration list, quantifiers: Q.quantifier list, triples: T.triple list}

fun filterN3Document {declarations, quantifiers, n3} filter = {declarations=declarations, quantifiers = quantifiers, n3 = List.filter filter n3}

end
