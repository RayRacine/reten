signature PREDICATE_SET = 
sig

    type predicate_set

    structure N: NODE
    structure C: CLAUSE
    sharing type N.node = C.N.node

    val mkPredicateSet: N.node -> N.node list -> predicate_set

    val values: predicate_set -> N.node * N.node list

    val predicate: predicate_set -> N.node

    val objects: predicate_set -> N.node list

    val extend: predicate_set -> N.node -> predicate_set

    val isPredicate: predicate_set -> N.node -> bool

    val objectSize: predicate_set -> int

    val hasClause: predicate_set -> C.clause -> bool

   (* All second predicate_set is wholly contained within the first predicate_set *)	
    val subsumes: predicate_set -> predicate_set -> bool

    val predicateEq: predicate_set * predicate_set -> bool

    val merge: predicate_set * predicate_set list -> predicate_set list

end
