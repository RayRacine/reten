structure WorkingMemory: WORKING_MEMORY =
struct

exception NoNode

structure N = Node
structure N3 = N3
structure T = Triple
structure P = PredicateSet

structure N3Key = 
     struct

     type hash_key = N.node
		     
     fun hashVal node = case node of 
			    N.Lit s => HashString.hashString s
			  | N.Sym s => HashString.hashString s
			  | _ => raise NoNode
				       
     fun sameKey(N.Lit k1, N.Lit k2) = String.compare(k1,k2) = EQUAL
       | sameKey(N.Sym s1, N.Sym s2) = String.compare(s1,s2) = EQUAL
       | sameKey(_,_) = false
			
     end

structure WM = HashTableFn(N3Key)

type working_memory = N3.n3 WM.hash_table

val DEF_SZ = 5000

fun addN3 (wm,n3) = let val subj = N3.subject n3 
		    in
			case WM.find wm subj of
			    SOME node => WM.insert wm (subj, (N3.merge n3 node))
			  | NONE => WM.insert wm (subj,n3)
		    end

fun addTriple (wm,triple) = let val (subj,pred,obj) = T.tripleValues triple
			   in
			       case WM.find wm subj of
				   SOME node => WM.insert wm (subj, N3.addPredicateSet node (P.mkPredicateSet pred [obj]))
				 | NONE => WM.insert wm (subj,N3.tripleToN3 triple)
			   end

fun fromTripleList triples = let val wm= WM.mkTable (DEF_SZ,NoNode)
			       in
				    List.app (fn triple => addTriple (wm,triple)) triples;
				    wm
			       end

fun toN3List wm = WM.listItems wm

end
