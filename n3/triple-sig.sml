signature TRIPLE =
sig

    structure N: NODE

    type triple = N.node * N.node * N.node

    val mkTriple: N.node -> N.node -> N.node -> triple

    val tripleValues: triple -> N.node * N.node * N.node
end
