structure PredicateSet: PREDICATE_SET =
struct

structure N = Node
structure C = Clause

structure U = Util
      
type predicate_set = N.node * N.node list
		
fun mkPredicateSet pred objs = (pred, objs)

fun values p = p

fun predicate (pred,_) = pred

fun objects (_, objs) = objs

fun objectSize p = List.length (objects p)

fun extend (pred, objs) obj = (pred, U.mergeElement N.equal obj objs)

fun isPredicate (pred, _) n = Node.equal(pred,n)

fun hasClause (pred, objs) clause = let val (clpred, clobj) = C.values clause
				    in
					if N.equal (pred, clpred)
					then Option.isSome (List.find (fn obj => N.equal (clobj,obj)) objs)
					else false
				    end

fun subsumes superset subset = raise General.Fail "Not Implemented."

fun predicate (p,_) = p

fun predicateEq ((p1,_),(p2,_)) = N.equal (p1,p2)

(* merge objects for a common predicate_set *)
fun mergeObjects prop1 prop2 =
    let val (pred, objs1) = prop1
	val (_ , objs2) = prop2
    in
	(pred, U.mergeLists N.equal objs1 objs2)
    end
	
(************************************************
 * merge a new predicate_set into an existing properties
 * if the predicate_set does not exist its an add 
 *************************************************)
fun merge (prop,[]) = [prop]
  | merge (prop,props) = let fun mp [] _  = prop :: props
				       | mp (p::ps) accum = 
					 if predicateEq (prop,p)
					 then ((mergeObjects prop p)::ps) @ accum
					 else mp ps (p::accum)
				 in
				     mp props []
				 end

end
