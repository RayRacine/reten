functor N3LrValsFun(structure Token : TOKEN)
 : sig structure ParserData : PARSER_DATA
       structure Tokens : N3_TOKENS
   end
 = 
struct
structure ParserData=
struct
structure Header = 
struct

structure DOC = Document
structure DEC = Declaration
structure PS = PredicateSet	
structure Q = Quantifier
structure N = Node	    


end
structure LrTable = Token.LrTable
structure Token = Token
local open LrTable in 
val table=let val actionRows =
"\
\\001\000\000\000\
\\001\000\001\000\000\000\000\000\
\\001\000\002\000\155\000\004\000\155\000\008\000\155\000\009\000\155\000\
\\010\000\155\000\011\000\155\000\012\000\155\000\013\000\155\000\
\\014\000\155\000\015\000\155\000\016\000\155\000\017\000\155\000\
\\018\000\155\000\019\000\155\000\020\000\155\000\021\000\155\000\
\\022\000\155\000\023\000\155\000\024\000\155\000\025\000\155\000\
\\026\000\155\000\027\000\155\000\028\000\155\000\000\000\
\\001\000\002\000\035\000\000\000\
\\001\000\002\000\082\000\000\000\
\\001\000\002\000\091\000\023\000\090\000\000\000\
\\001\000\002\000\094\000\000\000\
\\001\000\002\000\101\000\023\000\090\000\000\000\
\\001\000\004\000\022\000\006\000\021\000\007\000\020\000\008\000\019\000\
\\010\000\018\000\012\000\017\000\019\000\016\000\026\000\015\000\
\\027\000\014\000\028\000\013\000\000\000\
\\001\000\004\000\022\000\006\000\021\000\007\000\020\000\008\000\019\000\
\\010\000\018\000\012\000\017\000\019\000\016\000\026\000\015\000\
\\027\000\014\000\028\000\013\000\031\000\044\000\032\000\043\000\000\000\
\\001\000\004\000\022\000\008\000\019\000\010\000\018\000\011\000\053\000\
\\012\000\017\000\014\000\031\000\015\000\030\000\016\000\029\000\
\\017\000\028\000\018\000\027\000\019\000\016\000\021\000\026\000\
\\026\000\015\000\027\000\014\000\028\000\013\000\000\000\
\\001\000\004\000\022\000\008\000\019\000\010\000\018\000\012\000\017\000\
\\014\000\031\000\015\000\030\000\016\000\029\000\017\000\028\000\
\\018\000\027\000\019\000\016\000\021\000\026\000\026\000\015\000\
\\027\000\014\000\028\000\013\000\000\000\
\\001\000\004\000\022\000\008\000\019\000\010\000\018\000\012\000\017\000\
\\019\000\016\000\026\000\015\000\027\000\014\000\028\000\013\000\000\000\
\\001\000\004\000\022\000\008\000\019\000\010\000\018\000\012\000\017\000\
\\019\000\016\000\026\000\015\000\027\000\014\000\028\000\013\000\
\\031\000\044\000\000\000\
\\001\000\004\000\022\000\008\000\019\000\010\000\018\000\012\000\017\000\
\\019\000\016\000\026\000\015\000\027\000\014\000\028\000\013\000\
\\031\000\044\000\032\000\043\000\000\000\
\\001\000\004\000\022\000\008\000\019\000\010\000\018\000\012\000\017\000\
\\019\000\016\000\026\000\015\000\027\000\014\000\028\000\013\000\
\\032\000\043\000\000\000\
\\001\000\004\000\022\000\019\000\016\000\000\000\
\\001\000\004\000\083\000\000\000\
\\001\000\009\000\081\000\000\000\
\\001\000\011\000\079\000\000\000\
\\001\000\013\000\074\000\000\000\
\\001\000\019\000\057\000\000\000\
\\001\000\022\000\088\000\000\000\
\\001\000\030\000\073\000\000\000\
\\103\000\000\000\
\\104\000\004\000\022\000\008\000\019\000\010\000\018\000\012\000\017\000\
\\019\000\016\000\026\000\015\000\027\000\014\000\028\000\013\000\000\000\
\\105\000\000\000\
\\106\000\000\000\
\\107\000\031\000\044\000\000\000\
\\108\000\031\000\044\000\032\000\043\000\000\000\
\\109\000\031\000\044\000\000\000\
\\110\000\004\000\022\000\008\000\019\000\010\000\018\000\012\000\017\000\
\\019\000\016\000\026\000\015\000\027\000\014\000\028\000\013\000\000\000\
\\111\000\000\000\
\\112\000\000\000\
\\113\000\000\000\
\\114\000\000\000\
\\115\000\002\000\078\000\000\000\
\\116\000\000\000\
\\117\000\004\000\022\000\008\000\019\000\010\000\018\000\012\000\017\000\
\\019\000\016\000\026\000\015\000\027\000\014\000\028\000\013\000\000\000\
\\118\000\000\000\
\\119\000\000\000\
\\120\000\000\000\
\\121\000\000\000\
\\122\000\000\000\
\\123\000\000\000\
\\124\000\000\000\
\\125\000\000\000\
\\126\000\000\000\
\\127\000\000\000\
\\128\000\000\000\
\\129\000\000\000\
\\130\000\000\000\
\\131\000\000\000\
\\132\000\020\000\087\000\000\000\
\\133\000\020\000\087\000\023\000\086\000\000\000\
\\134\000\000\000\
\\135\000\023\000\086\000\000\000\
\\136\000\000\000\
\\137\000\000\000\
\\138\000\000\000\
\\139\000\000\000\
\\140\000\000\000\
\\141\000\000\000\
\\142\000\000\000\
\\143\000\000\000\
\\144\000\000\000\
\\145\000\000\000\
\\146\000\024\000\034\000\025\000\033\000\000\000\
\\147\000\000\000\
\\148\000\000\000\
\\149\000\000\000\
\\150\000\000\000\
\\151\000\000\000\
\\152\000\000\000\
\\153\000\000\000\
\\154\000\000\000\
\\156\000\000\000\
\\157\000\000\000\
\\159\000\000\000\
\\160\000\004\000\022\000\008\000\019\000\010\000\018\000\012\000\017\000\
\\019\000\016\000\026\000\015\000\027\000\014\000\028\000\013\000\000\000\
\\161\000\000\000\
\\162\000\000\000\
\\163\000\000\000\
\\164\000\000\000\
\\165\000\005\000\047\000\029\000\046\000\000\000\
\\166\000\000\000\
\\167\000\000\000\
\\168\000\000\000\
\\169\000\000\000\
\\170\000\000\000\
\\171\000\000\000\
\"
val actionRowNumbers =
"\008\000\011\000\073\000\065\000\
\\076\000\067\000\003\000\027\000\
\\071\000\046\000\009\000\084\000\
\\074\000\072\000\082\000\015\000\
\\010\000\012\000\000\000\021\000\
\\081\000\058\000\012\000\050\000\
\\012\000\064\000\063\000\062\000\
\\012\000\061\000\068\000\012\000\
\\012\000\031\000\041\000\044\000\
\\029\000\030\000\025\000\026\000\
\\047\000\016\000\016\000\083\000\
\\016\000\023\000\020\000\014\000\
\\036\000\035\000\019\000\078\000\
\\079\000\018\000\004\000\017\000\
\\066\000\054\000\022\000\059\000\
\\069\000\070\000\032\000\042\000\
\\028\000\045\000\024\000\040\000\
\\005\000\043\000\086\000\085\000\
\\077\000\013\000\034\000\037\000\
\\038\000\002\000\080\000\075\000\
\\048\000\006\000\053\000\052\000\
\\012\000\011\000\060\000\088\000\
\\016\000\087\000\033\000\039\000\
\\049\000\051\000\056\000\055\000\
\\007\000\057\000\089\000\090\000\
\\001\000"
val gotoT =
"\
\\001\000\100\000\002\000\010\000\003\000\009\000\005\000\008\000\
\\009\000\007\000\010\000\006\000\015\000\005\000\016\000\004\000\
\\020\000\003\000\022\000\002\000\025\000\001\000\000\000\
\\005\000\008\000\011\000\023\000\013\000\022\000\015\000\005\000\
\\016\000\004\000\020\000\021\000\022\000\002\000\000\000\
\\000\000\
\\000\000\
\\000\000\
\\021\000\030\000\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\003\000\040\000\005\000\008\000\009\000\039\000\010\000\006\000\
\\015\000\005\000\016\000\004\000\020\000\003\000\022\000\002\000\
\\025\000\001\000\026\000\038\000\027\000\037\000\028\000\036\000\
\\029\000\035\000\030\000\034\000\000\000\
\\004\000\043\000\000\000\
\\000\000\
\\000\000\
\\000\000\
\\005\000\008\000\007\000\049\000\010\000\048\000\015\000\005\000\
\\016\000\004\000\020\000\003\000\022\000\002\000\025\000\001\000\
\\028\000\047\000\030\000\034\000\031\000\046\000\000\000\
\\005\000\008\000\011\000\050\000\013\000\022\000\015\000\005\000\
\\016\000\004\000\020\000\021\000\022\000\002\000\000\000\
\\005\000\008\000\015\000\005\000\016\000\004\000\019\000\053\000\
\\020\000\052\000\022\000\002\000\000\000\
\\006\000\054\000\000\000\
\\000\000\
\\000\000\
\\000\000\
\\005\000\008\000\015\000\005\000\016\000\004\000\017\000\057\000\
\\020\000\056\000\022\000\002\000\000\000\
\\000\000\
\\005\000\008\000\015\000\005\000\016\000\004\000\020\000\058\000\
\\022\000\002\000\000\000\
\\000\000\
\\000\000\
\\000\000\
\\005\000\008\000\015\000\005\000\016\000\004\000\020\000\059\000\
\\022\000\002\000\000\000\
\\000\000\
\\000\000\
\\005\000\008\000\015\000\005\000\016\000\004\000\020\000\060\000\
\\022\000\002\000\000\000\
\\005\000\008\000\015\000\005\000\016\000\004\000\020\000\061\000\
\\022\000\002\000\000\000\
\\005\000\008\000\009\000\062\000\010\000\006\000\015\000\005\000\
\\016\000\004\000\020\000\003\000\022\000\002\000\025\000\001\000\000\000\
\\000\000\
\\000\000\
\\027\000\064\000\029\000\035\000\030\000\063\000\000\000\
\\029\000\065\000\000\000\
\\005\000\008\000\009\000\066\000\010\000\006\000\015\000\005\000\
\\016\000\004\000\020\000\003\000\022\000\002\000\025\000\001\000\000\000\
\\000\000\
\\000\000\
\\022\000\068\000\023\000\067\000\000\000\
\\022\000\068\000\023\000\069\000\000\000\
\\000\000\
\\022\000\070\000\000\000\
\\000\000\
\\000\000\
\\005\000\008\000\009\000\074\000\010\000\006\000\015\000\005\000\
\\016\000\004\000\020\000\003\000\022\000\002\000\025\000\001\000\
\\027\000\073\000\029\000\035\000\030\000\063\000\000\000\
\\008\000\075\000\000\000\
\\000\000\
\\000\000\
\\000\000\
\\005\000\008\000\015\000\005\000\016\000\004\000\019\000\078\000\
\\020\000\052\000\022\000\002\000\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\012\000\083\000\018\000\082\000\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\029\000\065\000\000\000\
\\000\000\
\\000\000\
\\000\000\
\\024\000\087\000\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\005\000\008\000\009\000\090\000\010\000\006\000\015\000\005\000\
\\016\000\004\000\020\000\003\000\022\000\002\000\025\000\001\000\
\\029\000\065\000\000\000\
\\000\000\
\\000\000\
\\005\000\008\000\007\000\091\000\010\000\048\000\015\000\005\000\
\\016\000\004\000\020\000\003\000\022\000\002\000\025\000\001\000\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\012\000\093\000\000\000\
\\000\000\
\\005\000\008\000\015\000\005\000\016\000\004\000\017\000\094\000\
\\020\000\056\000\022\000\002\000\000\000\
\\005\000\008\000\011\000\095\000\013\000\022\000\015\000\005\000\
\\016\000\004\000\020\000\021\000\022\000\002\000\000\000\
\\000\000\
\\000\000\
\\022\000\096\000\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\018\000\097\000\000\000\
\\000\000\
\\024\000\098\000\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\"
val numstates = 101
val numrules = 69
val s = ref "" and index = ref 0
val string_to_int = fn () => 
let val i = !index
in index := i+2; Char.ord(String.sub(!s,i)) + Char.ord(String.sub(!s,i+1)) * 256
end
val string_to_list = fn s' =>
    let val len = String.size s'
        fun f () =
           if !index < len then string_to_int() :: f()
           else nil
   in index := 0; s := s'; f ()
   end
val string_to_pairlist = fn (conv_key,conv_entry) =>
     let fun f () =
         case string_to_int()
         of 0 => EMPTY
          | n => PAIR(conv_key (n-1),conv_entry (string_to_int()),f())
     in f
     end
val string_to_pairlist_default = fn (conv_key,conv_entry) =>
    let val conv_row = string_to_pairlist(conv_key,conv_entry)
    in fn () =>
       let val default = conv_entry(string_to_int())
           val row = conv_row()
       in (row,default)
       end
   end
val string_to_table = fn (convert_row,s') =>
    let val len = String.size s'
        fun f ()=
           if !index < len then convert_row() :: f()
           else nil
     in (s := s'; index := 0; f ())
     end
local
  val memo = Array.array(numstates+numrules,ERROR)
  val _ =let fun g i=(Array.update(memo,i,REDUCE(i-numstates)); g(i+1))
       fun f i =
            if i=numstates then g i
            else (Array.update(memo,i,SHIFT (STATE i)); f (i+1))
          in f 0 handle Subscript => ()
          end
in
val entry_to_action = fn 0 => ACCEPT | 1 => ERROR | j => Array.sub(memo,(j-2))
end
val gotoT=Array.fromList(string_to_table(string_to_pairlist(NT,STATE),gotoT))
val actionRows=string_to_table(string_to_pairlist_default(T,entry_to_action),actionRows)
val actionRowNumbers = string_to_list actionRowNumbers
val actionT = let val actionRowLookUp=
let val a=Array.fromList(actionRows) in fn i=>Array.sub(a,i) end
in Array.fromList(map actionRowLookUp actionRowNumbers)
end
in LrTable.mkLrTable {actions=actionT,gotos=gotoT,numRules=numrules,
numStates=numstates,initialState=STATE 0}
end
end
local open Header in
type pos = int
type arg = unit
structure MlyValue = 
struct
datatype svalue = VOID | ntVOID of unit | LANGCODE of  (string)
 | LITERAL of  (string) | VARIABLE of  (string)
 | NUMERIC_LITERAL of  (real) | QNAME of  (string)
 | EXPLICIT_URI of  (string)
 | FORMULA_CONTENT of  (Q.quantifier list*Q.quantifier list*N3.n3 list)
 | UNIVERSAL of  (Q.quantifier) | EXISTENTIAL of  (Q.quantifier)
 | UNIVERSAL_S of  (Q.quantifier list)
 | EXISTENTIAL_S of  (Q.quantifier list)
 | QUANTIFIERS of  (Q.quantifier list) | SUBJECT of  (N.node)
 | SYMBOL_CSL_TAIL of  (string list) | SYMBOL_CSL of  (string list)
 | SYMBOL of  (string) | PATH_TAIL of  (N.node list)
 | PATH of  (N.node) | PATH_LIST of  (N.node)
 | OBJECT_TAIL of  (N.node list) | OBJECT of  (N.node list)
 | BLANK_NODE of  (PS.predicate_set list) | NODE of  (N.node)
 | PROP of  (N.node) | VERB of  (N.node)
 | PROPERTY_LIST_TAIL of  (PS.predicate_set list)
 | PROPERTY_LIST of  (PS.predicate_set list) | STATEMENT of  (N3.n3)
 | STATEMENTS of  (N3.n3 list) | STATEMENT_TAIL of  (N3.n3 list)
 | STATEMENT_LIST of  (N3.n3 list) | BARENAME_CSL of  (string list)
 | STRING_LITERAL of  (string) | DTLANG of  (string)
 | DECLARATION of  (DEC.declaration)
 | DECLARATION_S of  (DEC.declaration list)
 | N3DOCUMENT of  (DOC.n3document)
end
type svalue = MlyValue.svalue
type result = DOC.n3document
end
structure EC=
struct
open LrTable
val is_keyword =
fn (T 5) => true | (T 6) => true | (T 7) => true | (T 8) => true | (T 
9) => true | (T 10) => true | (T 11) => true | (T 12) => true | (T 13)
 => true | (T 14) => true | (T 20) => true | (T 15) => true | (T 16)
 => true | (T 17) => true | (T 22) => true | (T 23) => true | (T 24)
 => true | (T 1) => true | (T 28) => true | (T 32) => true | (T 30)
 => true | (T 31) => true | _ => false
val preferred_change = 
nil
val noShift = 
fn (T 0) => true | _ => false
val showTerminal =
fn (T 0) => "EOF"
  | (T 1) => "PERIOD"
  | (T 2) => "BARENAME"
  | (T 3) => "EXPLICIT_URI"
  | (T 4) => "AT"
  | (T 5) => "PREFIX"
  | (T 6) => "KEYWORDS"
  | (T 7) => "LPAREN"
  | (T 8) => "RPAREN"
  | (T 9) => "LBRAC"
  | (T 10) => "RBRAC"
  | (T 11) => "LCURL"
  | (T 12) => "RCURL"
  | (T 13) => "AT_A"
  | (T 14) => "AT_HAS"
  | (T 15) => "EQUAL"
  | (T 16) => "LEFT_ARROW"
  | (T 17) => "RIGHT_ARROW"
  | (T 18) => "QNAME"
  | (T 19) => "SEMI"
  | (T 20) => "AT_IS"
  | (T 21) => "AT_OF"
  | (T 22) => "COMMA"
  | (T 23) => "HAT"
  | (T 24) => "BANG"
  | (T 25) => "NUMERIC_LITERAL"
  | (T 26) => "VARIABLE"
  | (T 27) => "LITERAL"
  | (T 28) => "LANG_OP"
  | (T 29) => "LANGCODE"
  | (T 30) => "AT_FOR_SOME"
  | (T 31) => "AT_FOR_ALL"
  | (T 32) => "AT_THIS"
  | (T 33) => "PEAKS"
  | _ => "bogus-term"
local open Header in
val errtermvalue=
fn _ => MlyValue.VOID
end
val terms = (T 0) :: (T 1) :: (T 2) :: (T 4) :: (T 5) :: (T 6) :: (T 7
) :: (T 8) :: (T 9) :: (T 10) :: (T 11) :: (T 12) :: (T 13) :: (T 14)
 :: (T 15) :: (T 16) :: (T 17) :: (T 19) :: (T 20) :: (T 21) :: (T 22)
 :: (T 23) :: (T 24) :: (T 28) :: (T 30) :: (T 31) :: (T 32) :: (T 33)
 :: nil
end
structure Actions =
struct 
type int = Int.int
exception mlyAction of int
local open Header in
val actions = 
fn (i392:int,defaultPos,stack,
    (()):arg) =>
case (i392,stack)
of (0,(_,(MlyValue.STATEMENTS STATEMENTS,_,STATEMENTS1right))::(_,(
MlyValue.QUANTIFIERS QUANTIFIERS,_,_))::(_,(MlyValue.DECLARATION_S 
DECLARATION_S,DECLARATION_S1left,_))::rest671) => let val result=
MlyValue.N3DOCUMENT((
{declarations = DECLARATION_S, quantifiers = QUANTIFIERS, n3 = STATEMENTS}
))
 in (LrTable.NT 0,(result,DECLARATION_S1left,STATEMENTS1right),rest671
) end
| (1,(_,(MlyValue.QUANTIFIERS QUANTIFIERS,_,QUANTIFIERS1right))::(_,(
MlyValue.DECLARATION_S DECLARATION_S,DECLARATION_S1left,_))::rest671)
 => let val result=MlyValue.N3DOCUMENT((
{declarations = DECLARATION_S, quantifiers = QUANTIFIERS, n3 = []}))
 in (LrTable.NT 0,(result,DECLARATION_S1left,QUANTIFIERS1right),
rest671) end
| (2,(_,(MlyValue.STATEMENTS STATEMENTS,_,STATEMENTS1right))::(_,(
MlyValue.DECLARATION_S DECLARATION_S,DECLARATION_S1left,_))::rest671)
 => let val result=MlyValue.N3DOCUMENT((
{declarations = DECLARATION_S, quantifiers = [], n3 = STATEMENTS}))
 in (LrTable.NT 0,(result,DECLARATION_S1left,STATEMENTS1right),rest671
) end
| (3,(_,(MlyValue.STATEMENTS STATEMENTS,STATEMENTS1left,
STATEMENTS1right))::rest671) => let val result=MlyValue.N3DOCUMENT((
{declarations = [],quantifiers = [], n3 = STATEMENTS}))
 in (LrTable.NT 0,(result,STATEMENTS1left,STATEMENTS1right),rest671)
 end
| (4,(_,(MlyValue.EXISTENTIAL_S EXISTENTIAL_S,_,EXISTENTIAL_S1right))
::(_,(MlyValue.UNIVERSAL_S UNIVERSAL_S,UNIVERSAL_S1left,_))::rest671)
 => let val result=MlyValue.QUANTIFIERS((UNIVERSAL_S @ EXISTENTIAL_S))
 in (LrTable.NT 25,(result,UNIVERSAL_S1left,EXISTENTIAL_S1right),
rest671) end
| (5,(_,(MlyValue.UNIVERSAL_S UNIVERSAL_S,UNIVERSAL_S1left,
UNIVERSAL_S1right))::rest671) => let val result=MlyValue.QUANTIFIERS((
UNIVERSAL_S))
 in (LrTable.NT 25,(result,UNIVERSAL_S1left,UNIVERSAL_S1right),rest671
) end
| (6,(_,(MlyValue.EXISTENTIAL_S EXISTENTIAL_S,EXISTENTIAL_S1left,
EXISTENTIAL_S1right))::rest671) => let val result=MlyValue.QUANTIFIERS
((EXISTENTIAL_S))
 in (LrTable.NT 25,(result,EXISTENTIAL_S1left,EXISTENTIAL_S1right),
rest671) end
| (7,(_,(_,_,PERIOD1right))::(_,(MlyValue.STATEMENT STATEMENT,
STATEMENT1left,_))::rest671) => let val result=MlyValue.STATEMENTS((
[STATEMENT]))
 in (LrTable.NT 8,(result,STATEMENT1left,PERIOD1right),rest671) end
| (8,(_,(MlyValue.STATEMENTS STATEMENTS,_,STATEMENTS1right))::_::(_,(
MlyValue.STATEMENT STATEMENT,STATEMENT1left,_))::rest671) => let val 
result=MlyValue.STATEMENTS((STATEMENT :: STATEMENTS))
 in (LrTable.NT 8,(result,STATEMENT1left,STATEMENTS1right),rest671)
 end
| (9,(_,(MlyValue.STATEMENTS STATEMENTS,_,STATEMENTS1right))::(_,(
MlyValue.EXISTENTIAL_S EXISTENTIAL_S,_,_))::(_,(MlyValue.UNIVERSAL_S 
UNIVERSAL_S,UNIVERSAL_S1left,_))::rest671) => let val result=
MlyValue.FORMULA_CONTENT(((UNIVERSAL_S, EXISTENTIAL_S, STATEMENTS)))
 in (LrTable.NT 30,(result,UNIVERSAL_S1left,STATEMENTS1right),rest671)
 end
| (10,(_,(MlyValue.STATEMENTS STATEMENTS,_,STATEMENTS1right))::(_,(
MlyValue.UNIVERSAL_S UNIVERSAL_S,UNIVERSAL_S1left,_))::rest671) => 
let val result=MlyValue.FORMULA_CONTENT(((UNIVERSAL_S, [], STATEMENTS)
))
 in (LrTable.NT 30,(result,UNIVERSAL_S1left,STATEMENTS1right),rest671)
 end
| (11,(_,(MlyValue.STATEMENT_LIST STATEMENT_LIST,STATEMENT_LIST1left,
STATEMENT_LIST1right))::rest671) => let val result=
MlyValue.FORMULA_CONTENT((([], [], STATEMENT_LIST)))
 in (LrTable.NT 30,(result,STATEMENT_LIST1left,STATEMENT_LIST1right),
rest671) end
| (12,(_,(MlyValue.STATEMENT STATEMENT,STATEMENT1left,STATEMENT1right)
)::rest671) => let val result=MlyValue.STATEMENT_LIST(([STATEMENT]))
 in (LrTable.NT 6,(result,STATEMENT1left,STATEMENT1right),rest671) end
| (13,(_,(MlyValue.STATEMENT_TAIL STATEMENT_TAIL,_,
STATEMENT_TAIL1right))::(_,(MlyValue.STATEMENT STATEMENT,
STATEMENT1left,_))::rest671) => let val result=MlyValue.STATEMENT_LIST
((STATEMENT :: STATEMENT_TAIL))
 in (LrTable.NT 6,(result,STATEMENT1left,STATEMENT_TAIL1right),rest671
) end
| (14,(_,(_,PERIOD1left,PERIOD1right))::rest671) => let val result=
MlyValue.STATEMENT_TAIL(([]))
 in (LrTable.NT 7,(result,PERIOD1left,PERIOD1right),rest671) end
| (15,(_,(MlyValue.STATEMENT_LIST STATEMENT_LIST,_,
STATEMENT_LIST1right))::(_,(_,PERIOD1left,_))::rest671) => let val 
result=MlyValue.STATEMENT_TAIL((STATEMENT_LIST))
 in (LrTable.NT 7,(result,PERIOD1left,STATEMENT_LIST1right),rest671)
 end
| (16,(_,(MlyValue.SYMBOL_CSL SYMBOL_CSL,_,SYMBOL_CSL1right))::(_,(_,
AT_FOR_ALL1left,_))::rest671) => let val result=MlyValue.UNIVERSAL((
Q.ForAll SYMBOL_CSL))
 in (LrTable.NT 29,(result,AT_FOR_ALL1left,SYMBOL_CSL1right),rest671)
 end
| (17,(_,(MlyValue.UNIVERSAL UNIVERSAL,UNIVERSAL1left,UNIVERSAL1right)
)::rest671) => let val result=MlyValue.UNIVERSAL_S(([UNIVERSAL]))
 in (LrTable.NT 27,(result,UNIVERSAL1left,UNIVERSAL1right),rest671)
 end
| (18,(_,(MlyValue.UNIVERSAL UNIVERSAL,_,UNIVERSAL1right))::(_,(
MlyValue.UNIVERSAL_S UNIVERSAL_S,UNIVERSAL_S1left,_))::rest671) => 
let val result=MlyValue.UNIVERSAL_S((UNIVERSAL :: UNIVERSAL_S))
 in (LrTable.NT 27,(result,UNIVERSAL_S1left,UNIVERSAL1right),rest671)
 end
| (19,(_,(MlyValue.SYMBOL_CSL SYMBOL_CSL,_,SYMBOL_CSL1right))::(_,(_,
AT_FOR_SOME1left,_))::rest671) => let val result=MlyValue.EXISTENTIAL(
(Q.ForSome SYMBOL_CSL))
 in (LrTable.NT 28,(result,AT_FOR_SOME1left,SYMBOL_CSL1right),rest671)
 end
| (20,(_,(MlyValue.EXISTENTIAL EXISTENTIAL,EXISTENTIAL1left,
EXISTENTIAL1right))::rest671) => let val result=MlyValue.EXISTENTIAL_S
(([EXISTENTIAL]))
 in (LrTable.NT 26,(result,EXISTENTIAL1left,EXISTENTIAL1right),rest671
) end
| (21,(_,(MlyValue.EXISTENTIAL EXISTENTIAL,_,EXISTENTIAL1right))::(_,(
MlyValue.EXISTENTIAL_S EXISTENTIAL_S,EXISTENTIAL_S1left,_))::rest671)
 => let val result=MlyValue.EXISTENTIAL_S((
EXISTENTIAL :: EXISTENTIAL_S))
 in (LrTable.NT 26,(result,EXISTENTIAL_S1left,EXISTENTIAL1right),
rest671) end
| (22,(_,(MlyValue.DECLARATION DECLARATION,DECLARATION1left,
DECLARATION1right))::rest671) => let val result=MlyValue.DECLARATION_S
(([DECLARATION]))
 in (LrTable.NT 1,(result,DECLARATION1left,DECLARATION1right),rest671)
 end
| (23,(_,(MlyValue.DECLARATION DECLARATION,_,DECLARATION1right))::(_,(
MlyValue.DECLARATION_S DECLARATION_S,DECLARATION_S1left,_))::rest671)
 => let val result=MlyValue.DECLARATION_S((DECLARATION::DECLARATION_S)
)
 in (LrTable.NT 1,(result,DECLARATION_S1left,DECLARATION1right),
rest671) end
| (24,(_,(_,_,PERIOD1right))::(_,(MlyValue.BARENAME_CSL BARENAME_CSL,_
,_))::(_,(_,KEYWORDS1left,_))::rest671) => let val result=
MlyValue.DECLARATION((DEC.mkDeclaration "__EMPTY" "__BARENAME_CSL"))
 in (LrTable.NT 2,(result,KEYWORDS1left,PERIOD1right),rest671) end
| (25,(_,(_,_,PERIOD1right))::(_,(MlyValue.EXPLICIT_URI EXPLICIT_URI,_
,_))::(_,(MlyValue.QNAME QNAME,_,_))::(_,(_,PREFIX1left,_))::rest671)
 => let val result=MlyValue.DECLARATION((
DEC.mkDeclaration QNAME EXPLICIT_URI))
 in (LrTable.NT 2,(result,PREFIX1left,PERIOD1right),rest671) end
| (26,(_,(MlyValue.PROPERTY_LIST PROPERTY_LIST,_,PROPERTY_LIST1right))
::(_,(MlyValue.SUBJECT SUBJECT,SUBJECT1left,_))::rest671) => let val 
result=MlyValue.STATEMENT((N3.mkN3 SUBJECT  PROPERTY_LIST))
 in (LrTable.NT 9,(result,SUBJECT1left,PROPERTY_LIST1right),rest671)
 end
| (27,(_,(MlyValue.PROPERTY_LIST_TAIL PROPERTY_LIST_TAIL,_,
PROPERTY_LIST_TAIL1right))::(_,(MlyValue.OBJECT_TAIL OBJECT_TAIL,_,_))
::(_,(MlyValue.OBJECT OBJECT,_,_))::(_,(MlyValue.VERB VERB,VERB1left,_
))::rest671) => let val result=MlyValue.PROPERTY_LIST((
PS.mkPredicateSet  VERB (OBJECT @ OBJECT_TAIL) :: PROPERTY_LIST_TAIL))
 in (LrTable.NT 10,(result,VERB1left,PROPERTY_LIST_TAIL1right),rest671
) end
| (28,(_,(MlyValue.PROPERTY_LIST_TAIL PROPERTY_LIST_TAIL,_,
PROPERTY_LIST_TAIL1right))::(_,(MlyValue.OBJECT OBJECT,_,_))::(_,(
MlyValue.VERB VERB,VERB1left,_))::rest671) => let val result=
MlyValue.PROPERTY_LIST((
PS.mkPredicateSet  VERB OBJECT  :: PROPERTY_LIST_TAIL))
 in (LrTable.NT 10,(result,VERB1left,PROPERTY_LIST_TAIL1right),rest671
) end
| (29,(_,(_,_,OBJECT_TAIL1right))::(_,(MlyValue.OBJECT OBJECT,_,_))::(
_,(MlyValue.VERB VERB,VERB1left,_))::rest671) => let val result=
MlyValue.PROPERTY_LIST(([PS.mkPredicateSet VERB OBJECT ]))
 in (LrTable.NT 10,(result,VERB1left,OBJECT_TAIL1right),rest671) end
| (30,(_,(MlyValue.OBJECT OBJECT,_,OBJECT1right))::(_,(MlyValue.VERB 
VERB,VERB1left,_))::rest671) => let val result=MlyValue.PROPERTY_LIST(
([PS.mkPredicateSet VERB OBJECT]))
 in (LrTable.NT 10,(result,VERB1left,OBJECT1right),rest671) end
| (31,(_,(MlyValue.PROPERTY_LIST PROPERTY_LIST,_,PROPERTY_LIST1right))
::(_,(_,SEMI1left,_))::rest671) => let val result=
MlyValue.PROPERTY_LIST_TAIL((PROPERTY_LIST))
 in (LrTable.NT 11,(result,SEMI1left,PROPERTY_LIST1right),rest671) end
| (32,(_,(MlyValue.OBJECT OBJECT,_,OBJECT1right))::(_,(_,COMMA1left,_)
)::rest671) => let val result=MlyValue.OBJECT_TAIL((OBJECT))
 in (LrTable.NT 17,(result,COMMA1left,OBJECT1right),rest671) end
| (33,(_,(MlyValue.OBJECT_TAIL OBJECT_TAIL,_,OBJECT_TAIL1right))::(_,(
MlyValue.OBJECT OBJECT,_,_))::(_,(_,COMMA1left,_))::rest671) => let 
val result=MlyValue.OBJECT_TAIL((OBJECT @ OBJECT_TAIL))
 in (LrTable.NT 17,(result,COMMA1left,OBJECT_TAIL1right),rest671) end
| (34,(_,(MlyValue.PATH PATH,PATH1left,PATH1right))::rest671) => let 
val result=MlyValue.VERB((PATH))
 in (LrTable.NT 12,(result,PATH1left,PATH1right),rest671) end
| (35,(_,(MlyValue.PATH PATH,_,PATH1right))::(_,(_,AT_HAS1left,_))::
rest671) => let val result=MlyValue.VERB((PATH))
 in (LrTable.NT 12,(result,AT_HAS1left,PATH1right),rest671) end
| (36,(_,(_,_,AT_OF1right))::(_,(MlyValue.PATH PATH,_,_))::(_,(_,
AT_IS1left,_))::rest671) => let val result=MlyValue.VERB((PATH))
 in (LrTable.NT 12,(result,AT_IS1left,AT_OF1right),rest671) end
| (37,(_,(_,AT_A1left,AT_A1right))::rest671) => let val result=
MlyValue.VERB((N.Lit "@a"))
 in (LrTable.NT 12,(result,AT_A1left,AT_A1right),rest671) end
| (38,(_,(_,EQUAL1left,EQUAL1right))::rest671) => let val result=
MlyValue.VERB((N.Lit "EQUAL"))
 in (LrTable.NT 12,(result,EQUAL1left,EQUAL1right),rest671) end
| (39,(_,(_,LEFT_ARROW1left,LEFT_ARROW1right))::rest671) => let val 
result=MlyValue.VERB((N.Lit "LARROW"))
 in (LrTable.NT 12,(result,LEFT_ARROW1left,LEFT_ARROW1right),rest671)
 end
| (40,(_,(_,RIGHT_ARROW1left,RIGHT_ARROW1right))::rest671) => let val 
result=MlyValue.VERB((N.Lit "RARROW"))
 in (LrTable.NT 12,(result,RIGHT_ARROW1left,RIGHT_ARROW1right),rest671
) end
| (41,(_,(MlyValue.PATH PATH,PATH1left,PATH1right))::rest671) => let 
val result=MlyValue.SUBJECT((PATH))
 in (LrTable.NT 24,(result,PATH1left,PATH1right),rest671) end
| (42,(_,(MlyValue.PATH PATH,PATH1left,PATH1right))::rest671) => let 
val result=MlyValue.OBJECT(([PATH]))
 in (LrTable.NT 16,(result,PATH1left,PATH1right),rest671) end
| (43,(_,(MlyValue.NODE NODE,NODE1left,NODE1right))::rest671) => let 
val result=MlyValue.PATH((N.Node NODE))
 in (LrTable.NT 19,(result,NODE1left,NODE1right),rest671) end
| (44,(_,(MlyValue.PATH_TAIL PATH_TAIL,_,PATH_TAIL1right))::(_,(
MlyValue.NODE NODE,NODE1left,_))::rest671) => let val result=
MlyValue.PATH((N.PathList ((N.Node NODE) ::PATH_TAIL)))
 in (LrTable.NT 19,(result,NODE1left,PATH_TAIL1right),rest671) end
| (45,(_,(MlyValue.PATH PATH,_,PATH1right))::(_,(_,BANG1left,_))::
rest671) => let val result=MlyValue.PATH_TAIL((
N.PathList [N.Bang,PATH]))
 in (LrTable.NT 20,(result,BANG1left,PATH1right),rest671) end
| (46,(_,(MlyValue.PATH PATH,_,PATH1right))::(_,(_,HAT1left,_))::
rest671) => let val result=MlyValue.PATH_TAIL((N.PathList [N.Hat,PATH]
))
 in (LrTable.NT 20,(result,HAT1left,PATH1right),rest671) end
| (47,(_,(MlyValue.STRING_LITERAL STRING_LITERAL,STRING_LITERAL1left,
STRING_LITERAL1right))::rest671) => let val result=MlyValue.NODE((
N.Lit STRING_LITERAL))
 in (LrTable.NT 14,(result,STRING_LITERAL1left,STRING_LITERAL1right),
rest671) end
| (48,(_,(MlyValue.NUMERIC_LITERAL NUMERIC_LITERAL,
NUMERIC_LITERAL1left,NUMERIC_LITERAL1right))::rest671) => let val 
result=MlyValue.NODE((N.Number NUMERIC_LITERAL))
 in (LrTable.NT 14,(result,NUMERIC_LITERAL1left,NUMERIC_LITERAL1right)
,rest671) end
| (49,(_,(MlyValue.SYMBOL SYMBOL,SYMBOL1left,SYMBOL1right))::rest671)
 => let val result=MlyValue.NODE((N.Sym SYMBOL))
 in (LrTable.NT 14,(result,SYMBOL1left,SYMBOL1right),rest671) end
| (50,(_,(MlyValue.VARIABLE VARIABLE,VARIABLE1left,VARIABLE1right))::
rest671) => let val result=MlyValue.NODE((N.Var VARIABLE))
 in (LrTable.NT 14,(result,VARIABLE1left,VARIABLE1right),rest671) end
| (51,(_,(_,_,RPAREN1right))::(_,(MlyValue.PATH_LIST PATH_LIST,_,_))::
(_,(_,LPAREN1left,_))::rest671) => let val result=MlyValue.NODE((
PATH_LIST))
 in (LrTable.NT 14,(result,LPAREN1left,RPAREN1right),rest671) end
| (52,(_,(_,_,RBRAC1right))::(_,(MlyValue.PROPERTY_LIST PROPERTY_LIST,
_,_))::(_,(_,LBRAC1left,_))::rest671) => let val result=MlyValue.NODE(
(N.PropertyList PROPERTY_LIST))
 in (LrTable.NT 14,(result,LBRAC1left,RBRAC1right),rest671) end
| (53,(_,(MlyValue.BLANK_NODE BLANK_NODE,BLANK_NODE1left,
BLANK_NODE1right))::rest671) => let val result=MlyValue.NODE((
N.BNode BLANK_NODE))
 in (LrTable.NT 14,(result,BLANK_NODE1left,BLANK_NODE1right),rest671)
 end
| (54,(_,(_,_,RCURL1right))::(_,(MlyValue.FORMULA_CONTENT 
FORMULA_CONTENT,_,_))::(_,(_,LCURL1left,_))::rest671) => let val 
result=MlyValue.NODE((N.Formula FORMULA_CONTENT))
 in (LrTable.NT 14,(result,LCURL1left,RCURL1right),rest671) end
| (55,(_,(_,_,RBRAC1right))::(_,(MlyValue.PROPERTY_LIST PROPERTY_LIST,
_,_))::(_,(_,LBRAC1left,_))::rest671) => let val result=
MlyValue.BLANK_NODE((PROPERTY_LIST))
 in (LrTable.NT 15,(result,LBRAC1left,RBRAC1right),rest671) end
| (56,(_,(_,_,RBRAC1right))::(_,(_,LBRAC1left,_))::rest671) => let 
val result=MlyValue.BLANK_NODE((
 [PS.mkPredicateSet (N.Sym ":FIXME_NIL") [(N.Lit ":FIXME_NIL")]] ))
 in (LrTable.NT 15,(result,LBRAC1left,RBRAC1right),rest671) end
| (57,(_,(MlyValue.PATH PATH,PATH1left,PATH1right))::rest671) => let 
val result=MlyValue.PATH_LIST((PATH))
 in (LrTable.NT 18,(result,PATH1left,PATH1right),rest671) end
| (58,(_,(MlyValue.PATH_LIST PATH_LIST,_,PATH_LIST1right))::(_,(
MlyValue.PATH PATH,PATH1left,_))::rest671) => let val result=
MlyValue.PATH_LIST((N.PathList ([PATH,PATH_LIST])))
 in (LrTable.NT 18,(result,PATH1left,PATH_LIST1right),rest671) end
| (59,(_,(MlyValue.EXPLICIT_URI EXPLICIT_URI,EXPLICIT_URI1left,
EXPLICIT_URI1right))::rest671) => let val result=MlyValue.SYMBOL((
EXPLICIT_URI))
 in (LrTable.NT 21,(result,EXPLICIT_URI1left,EXPLICIT_URI1right),
rest671) end
| (60,(_,(MlyValue.QNAME QNAME,QNAME1left,QNAME1right))::rest671) => 
let val result=MlyValue.SYMBOL((QNAME))
 in (LrTable.NT 21,(result,QNAME1left,QNAME1right),rest671) end
| (61,(_,(MlyValue.DTLANG DTLANG,_,DTLANG1right))::(_,(
MlyValue.LITERAL LITERAL,LITERAL1left,_))::rest671) => let val result=
MlyValue.STRING_LITERAL(((LITERAL ^ DTLANG)))
 in (LrTable.NT 4,(result,LITERAL1left,DTLANG1right),rest671) end
| (62,(_,(MlyValue.LITERAL LITERAL,LITERAL1left,LITERAL1right))::
rest671) => let val result=MlyValue.STRING_LITERAL((LITERAL))
 in (LrTable.NT 4,(result,LITERAL1left,LITERAL1right),rest671) end
| (63,(_,(MlyValue.LANGCODE LANGCODE,_,LANGCODE1right))::(_,(_,AT1left
,_))::rest671) => let val result=MlyValue.DTLANG((LANGCODE))
 in (LrTable.NT 3,(result,AT1left,LANGCODE1right),rest671) end
| (64,(_,(MlyValue.SYMBOL SYMBOL,_,SYMBOL1right))::(_,(_,LANG_OP1left,
_))::rest671) => let val result=MlyValue.DTLANG((SYMBOL))
 in (LrTable.NT 3,(result,LANG_OP1left,SYMBOL1right),rest671) end
| (65,(_,(_,_,PERIOD1right))::(_,(MlyValue.SYMBOL SYMBOL,SYMBOL1left,_
))::rest671) => let val result=MlyValue.SYMBOL_CSL(([SYMBOL]))
 in (LrTable.NT 22,(result,SYMBOL1left,PERIOD1right),rest671) end
| (66,(_,(MlyValue.SYMBOL_CSL_TAIL SYMBOL_CSL_TAIL,_,
SYMBOL_CSL_TAIL1right))::(_,(MlyValue.SYMBOL SYMBOL,SYMBOL1left,_))::
rest671) => let val result=MlyValue.SYMBOL_CSL((
SYMBOL :: SYMBOL_CSL_TAIL))
 in (LrTable.NT 22,(result,SYMBOL1left,SYMBOL_CSL_TAIL1right),rest671)
 end
| (67,(_,(MlyValue.SYMBOL_CSL_TAIL SYMBOL_CSL_TAIL,_,
SYMBOL_CSL_TAIL1right))::(_,(MlyValue.SYMBOL SYMBOL,_,_))::(_,(_,
COMMA1left,_))::rest671) => let val result=MlyValue.SYMBOL_CSL_TAIL((
SYMBOL :: SYMBOL_CSL_TAIL))
 in (LrTable.NT 23,(result,COMMA1left,SYMBOL_CSL_TAIL1right),rest671)
 end
| (68,(_,(_,_,PERIOD1right))::(_,(MlyValue.SYMBOL SYMBOL,_,_))::(_,(_,
COMMA1left,_))::rest671) => let val result=MlyValue.SYMBOL_CSL_TAIL((
[SYMBOL]))
 in (LrTable.NT 23,(result,COMMA1left,PERIOD1right),rest671) end
| _ => raise (mlyAction i392)
end
val void = MlyValue.VOID
val extract = fn a => (fn MlyValue.N3DOCUMENT x => x
| _ => let exception ParseInternal
	in raise ParseInternal end) a 
end
end
structure Tokens : N3_TOKENS =
struct
type svalue = ParserData.svalue
type ('a,'b) token = ('a,'b) Token.token
fun EOF (p1,p2) = Token.TOKEN (ParserData.LrTable.T 0,(
ParserData.MlyValue.VOID,p1,p2))
fun PERIOD (p1,p2) = Token.TOKEN (ParserData.LrTable.T 1,(
ParserData.MlyValue.VOID,p1,p2))
fun BARENAME (p1,p2) = Token.TOKEN (ParserData.LrTable.T 2,(
ParserData.MlyValue.VOID,p1,p2))
fun EXPLICIT_URI (i,p1,p2) = Token.TOKEN (ParserData.LrTable.T 3,(
ParserData.MlyValue.EXPLICIT_URI i,p1,p2))
fun AT (p1,p2) = Token.TOKEN (ParserData.LrTable.T 4,(
ParserData.MlyValue.VOID,p1,p2))
fun PREFIX (p1,p2) = Token.TOKEN (ParserData.LrTable.T 5,(
ParserData.MlyValue.VOID,p1,p2))
fun KEYWORDS (p1,p2) = Token.TOKEN (ParserData.LrTable.T 6,(
ParserData.MlyValue.VOID,p1,p2))
fun LPAREN (p1,p2) = Token.TOKEN (ParserData.LrTable.T 7,(
ParserData.MlyValue.VOID,p1,p2))
fun RPAREN (p1,p2) = Token.TOKEN (ParserData.LrTable.T 8,(
ParserData.MlyValue.VOID,p1,p2))
fun LBRAC (p1,p2) = Token.TOKEN (ParserData.LrTable.T 9,(
ParserData.MlyValue.VOID,p1,p2))
fun RBRAC (p1,p2) = Token.TOKEN (ParserData.LrTable.T 10,(
ParserData.MlyValue.VOID,p1,p2))
fun LCURL (p1,p2) = Token.TOKEN (ParserData.LrTable.T 11,(
ParserData.MlyValue.VOID,p1,p2))
fun RCURL (p1,p2) = Token.TOKEN (ParserData.LrTable.T 12,(
ParserData.MlyValue.VOID,p1,p2))
fun AT_A (p1,p2) = Token.TOKEN (ParserData.LrTable.T 13,(
ParserData.MlyValue.VOID,p1,p2))
fun AT_HAS (p1,p2) = Token.TOKEN (ParserData.LrTable.T 14,(
ParserData.MlyValue.VOID,p1,p2))
fun EQUAL (p1,p2) = Token.TOKEN (ParserData.LrTable.T 15,(
ParserData.MlyValue.VOID,p1,p2))
fun LEFT_ARROW (p1,p2) = Token.TOKEN (ParserData.LrTable.T 16,(
ParserData.MlyValue.VOID,p1,p2))
fun RIGHT_ARROW (p1,p2) = Token.TOKEN (ParserData.LrTable.T 17,(
ParserData.MlyValue.VOID,p1,p2))
fun QNAME (i,p1,p2) = Token.TOKEN (ParserData.LrTable.T 18,(
ParserData.MlyValue.QNAME i,p1,p2))
fun SEMI (p1,p2) = Token.TOKEN (ParserData.LrTable.T 19,(
ParserData.MlyValue.VOID,p1,p2))
fun AT_IS (p1,p2) = Token.TOKEN (ParserData.LrTable.T 20,(
ParserData.MlyValue.VOID,p1,p2))
fun AT_OF (p1,p2) = Token.TOKEN (ParserData.LrTable.T 21,(
ParserData.MlyValue.VOID,p1,p2))
fun COMMA (p1,p2) = Token.TOKEN (ParserData.LrTable.T 22,(
ParserData.MlyValue.VOID,p1,p2))
fun HAT (p1,p2) = Token.TOKEN (ParserData.LrTable.T 23,(
ParserData.MlyValue.VOID,p1,p2))
fun BANG (p1,p2) = Token.TOKEN (ParserData.LrTable.T 24,(
ParserData.MlyValue.VOID,p1,p2))
fun NUMERIC_LITERAL (i,p1,p2) = Token.TOKEN (ParserData.LrTable.T 25,(
ParserData.MlyValue.NUMERIC_LITERAL i,p1,p2))
fun VARIABLE (i,p1,p2) = Token.TOKEN (ParserData.LrTable.T 26,(
ParserData.MlyValue.VARIABLE i,p1,p2))
fun LITERAL (i,p1,p2) = Token.TOKEN (ParserData.LrTable.T 27,(
ParserData.MlyValue.LITERAL i,p1,p2))
fun LANG_OP (p1,p2) = Token.TOKEN (ParserData.LrTable.T 28,(
ParserData.MlyValue.VOID,p1,p2))
fun LANGCODE (i,p1,p2) = Token.TOKEN (ParserData.LrTable.T 29,(
ParserData.MlyValue.LANGCODE i,p1,p2))
fun AT_FOR_SOME (p1,p2) = Token.TOKEN (ParserData.LrTable.T 30,(
ParserData.MlyValue.VOID,p1,p2))
fun AT_FOR_ALL (p1,p2) = Token.TOKEN (ParserData.LrTable.T 31,(
ParserData.MlyValue.VOID,p1,p2))
fun AT_THIS (p1,p2) = Token.TOKEN (ParserData.LrTable.T 32,(
ParserData.MlyValue.VOID,p1,p2))
fun PEAKS (p1,p2) = Token.TOKEN (ParserData.LrTable.T 33,(
ParserData.MlyValue.VOID,p1,p2))
end
end
