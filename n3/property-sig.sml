signature PROPERTY = 
sig

    type property

    structure N: NODE

    val mkProperty: N.node -> N.node list -> property
    val propertyValues: property -> N.node * N.node list
    val extendProperty: property -> N.node -> property

    val predicateEq: property * property -> bool
    val mergeProperty: property * property list -> property list

end

