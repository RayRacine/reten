structure Declaration: DECLARATION =
struct

type uri = string
type declaration = {qname: string, uri: uri}	
   
fun mkDeclaration qname uri = {qname=qname, uri=uri}
			      
end
