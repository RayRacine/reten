Parsing anons-01.n3

   ---------------  
Declaration - Prefix [:, <http://example.org/#>]
SimpleStatement [Subject (Path
			  Node { [Property (Verb (Path
						  Node { Symbol (QName :p) }[])
					    Objects [Path
						     Node { Symbol (QName :q) }[]])Property (Verb (Path
												   Node { Symbol (QName :r) }[])
											     Objects [Path
												      Node { Symbol (QName :s) }[]])] }[])]
SimpleStatement [Subject (Path
			  Node { [Property (Verb (Path
						  Node { Symbol (QName :p) }[])
					    Objects [Path
						     Node { Symbol (QName :q) }[]])Property (Verb (Path
												   Node { Symbol (QName :r) }[])
											     Objects [Path
												      Node { [Property (Verb (Path
															      Node { Symbol (QName :s) }[])
															Objects [Path
																 Node { Symbol (QName :t) }[]])] }[]])] }[])]
-------------------------_:1 http:///bravais.org/test# _:2 .
Parsing dtlang-03.n3

   ---------------  
Declaration - Prefix [foaf:, <http://xmlns.com/foaf/0.1/>]
SimpleStatement [Subject (Path
			  Node { [Property (Verb (Path
						  Node { Symbol (QName foaf:name) }[])
					    Objects [Path
						     Node { Literal "Sean B. Palmer" }[]])] }[])]
-------------------------_:1 http:///bravais.org/test# _:2 .
Parsing lists-01.n3

   ---------------  
Declaration - Prefix [:, <http://example.org/#>]
SimpleStatement [Subject (Path Node { Symbol (QName :EmptyList) }[]),
		 Property (Verb (Path Node { Symbol (QName :predicate) }[])
			   Objects [Path Node { PathList  [] }[]])]
-------------------------_:1 http:///bravais.org/test# _:2 .
FIXME :EmptyList http:///bravais.org/test# _:2 .
Parsing literals-01.n3

   ---------------  
Declaration - Prefix [:, <http://example.org/#>]
SimpleStatement [Subject (Path Node { Literal "Subject literal" }[]),
		 Property (Verb (Path Node { Symbol (QName :p) }[])
			   Objects [Path Node { Symbol (QName :q) }[]])]
-------------------------