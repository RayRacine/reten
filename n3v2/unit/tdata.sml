structure TData: sig
    
    structure N3A: N3_AST
		   
    val qnode: N3A.node
    val qpathNoSegs: N3A.path
    val dummyStmt: N3A.statement
(*    val dummyTriple: NT.triple *)
		     
end =
struct
  
  structure N3A = N3Ast
  structure NT = NTriple
		  
  val qnode = N3A.Symbol (N3A.S.QName (QName.fromString ":ray"))
	      
  val qpathNoSegs = N3A.Path (qnode, [])


(*   val dummyTriple = NT.new qnode qnode qnode *)

  val dummyStmt = N3A.SimpleStatement (qpathNoSegs, [N3A.Property (N3A.AKW, [qpathNoSegs])])
		    
end
