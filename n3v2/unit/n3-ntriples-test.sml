structure N3NTriplesTest =
struct


structure N3A = N3Ast
structure N3NT = N3NTriples
structure TD = TData

fun testSegments () = let val (flatnode,trips) = N3NT.node TD.qnode
		      in
			  N3NT.segments (flatnode, [], trips)
		      end

fun testPath () = N3NT.path TD.qpathNoSegs

fun testStatement () = N3NT.statement TD.dummyStmt

end
