signature N3_AST = 
sig

    structure U: URI
    structure Q: QNAME

    type variable = string
    type numeric
    type langcode
    type barename

    type subject

    structure S: sig
	datatype symbol = Uri of  U.uri
			| QName of Q.qname
    end

    datatype dtlang = Language of langcode
		    | DatumType of S.symbol

    type literal = string * dtlang option

    datatype node = Symbol of S.symbol
		  | Formula of statement list
		  | Variable of variable
		  | Numeric of  numeric
		  | Literal of literal
		  | Anonymous of property list 
		  | PathList of path list   

    and path_segment = Bang of node
		     | Hat of node

    and path = Path of node * path_segment list

    and  verb = VPath of path
              | AKW 
	      | IsOfKW of path
	      | HasKW of path
	      | EQUAL
	      | LEFT_DARROW
	      | RIGHT_DARROW

    and declaration = Prefix of Q.qname * U.uri
                    | Keywords of barename list 

    and statement = Declaration of declaration
		  | Universal of S.symbol list 
		  | Existential of S.symbol list 
		  | SimpleStatement of path * property list

    and property = Property of (verb * path list) 

    datatype document = Statements of statement list 

    val layoutDocument: document -> Layout.t

end
