signature N3_TOKENS =
sig
type ('a,'b) token
type svalue
val AT_PREFIX:  'a * 'a -> (svalue,'a) token
val AT_KEYWORDS:  'a * 'a -> (svalue,'a) token
val AT_FORSOME:  'a * 'a -> (svalue,'a) token
val AT_FORALL:  'a * 'a -> (svalue,'a) token
val LANGCODE: (string) *  'a * 'a -> (svalue,'a) token
val LANG_OP:  'a * 'a -> (svalue,'a) token
val TYPE_OP:  'a * 'a -> (svalue,'a) token
val LITERAL: (string) *  'a * 'a -> (svalue,'a) token
val VARIABLE: (string) *  'a * 'a -> (svalue,'a) token
val NUMERIC_LITERAL: (real) *  'a * 'a -> (svalue,'a) token
val BANG:  'a * 'a -> (svalue,'a) token
val HAT:  'a * 'a -> (svalue,'a) token
val COMMA:  'a * 'a -> (svalue,'a) token
val AT_OF:  'a * 'a -> (svalue,'a) token
val AT_IS:  'a * 'a -> (svalue,'a) token
val SEMI:  'a * 'a -> (svalue,'a) token
val QNAME: (string) *  'a * 'a -> (svalue,'a) token
val RIGHT_ARROW:  'a * 'a -> (svalue,'a) token
val LEFT_ARROW:  'a * 'a -> (svalue,'a) token
val EQUAL:  'a * 'a -> (svalue,'a) token
val AT_HAS:  'a * 'a -> (svalue,'a) token
val AT_A:  'a * 'a -> (svalue,'a) token
val RCURL:  'a * 'a -> (svalue,'a) token
val LCURL:  'a * 'a -> (svalue,'a) token
val RBRAC:  'a * 'a -> (svalue,'a) token
val LBRAC:  'a * 'a -> (svalue,'a) token
val RPAREN:  'a * 'a -> (svalue,'a) token
val LPAREN:  'a * 'a -> (svalue,'a) token
val KEYWORDS:  'a * 'a -> (svalue,'a) token
val PREFIX:  'a * 'a -> (svalue,'a) token
val EXPLICIT_URI: (string) *  'a * 'a -> (svalue,'a) token
val BARENAME:  'a * 'a -> (svalue,'a) token
val PERIOD:  'a * 'a -> (svalue,'a) token
val EOF:  'a * 'a -> (svalue,'a) token
end
signature N3_LRVALS=
sig
structure Tokens : N3_TOKENS
structure ParserData:PARSER_DATA
sharing type ParserData.Token.token = Tokens.token
sharing type ParserData.svalue = Tokens.svalue
end
