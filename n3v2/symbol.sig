signature SYMBOL =
sig

type symbol

structure U: URI
structure Q: QNAME

val fromUri: U.uri -> symbol
val fromQName: Q.qname -> symbol

val toString: symbol -> string
val layout: symbol -> Layout.t
    
end
