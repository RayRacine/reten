structure Test =
struct

  val TEST_DIR = "test2"

  val testFiles = ["anons-01.n3",   "dtlang-03.n3",    "lists-01.n3",  
		   "literals-01.n3",  "numeric-02.n3",  "paths-02.n3",
		   "simple-03.n3",   "verbs-01.n3",
		   "anons-02.n3",  "dtlang-04.n3",    "lists-02.n3",  
		   "literals-02.n3",  "numeric-03.n3",  "paths-03.n3",      
		   "simple-04.n3",  "verbs-02.n3",
		   "anons-03.n3",   "dtlang-05.n3",   "lists-03.n3",  
		   "literals-03.n3",  "numeric-04.n3",  "simple-05.n3",     
		   "verbs-03.n3", "courses.n3",    "formulae-01.n3", 
		   "lists-04.n3",  "literals-04.n3",  "numeric-05.n3",  
		   "rules-simple.n3",  "simple-06.n3",   "verbs-04.n3",
		   "dtlang-01.n3",  "formulae-02.n3",  "lists-05.n3",  
		   (*"literals-05.n3",  "simple-01.n3",*)   
		   "simple-07.n3",     "verbs-05.n3",
		   "dtlang-02.n3",  "formulae-03.n3",  "lists-06.n3",  
		   "numeric-01.n3",   "paths-01.n3",    (*"simple-02.n3",*)     
		   "this-rules.n3",  "verbs-06.n3"]

  fun buildPath fname = OS.Path.joinDirFile {dir=TEST_DIR,file=fname}

  fun announce fname = (print "Parsing ";
			print fname;
			print "\n\n   ---------------  \n")

  fun parsePrint file = let val _ = announce file
			    val ast = N3Parser.n3ParseFile (buildPath file)
			in
			    Layout.outputln (N3Ast.layoutDocument ast, TextIO.stdOut)
(*			    print "-------------------------\n" *)
(*			    let val trips = N3NTriples.document ast
			    in
				List.app (fn t => Layout.outputln (NTriple.layout t, TextIO.stdOut)) trips
			    end *)
			end

  fun test () = List.app parsePrint testFiles

  fun testOne fname = parsePrint fname

end

(* val _ = Test.test() *)
