signature QNAME =
sig

type qname

val fromString: string -> qname
val fromPrefixValue: string -> string -> qname

val components: qname -> {prefix: string, name: string}
val isAnon: qname -> bool
val isLocal: qname ->bool

val equal: qname * qname -> bool

val toString: qname -> string
val layout: qname -> Layout.t

end
