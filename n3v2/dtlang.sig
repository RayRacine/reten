signature DT_LANG =
sig
    
    structure S: SYMBOL
		 
    datatype dtlang = Language of langcode
		    | DatumType of S.symbol

    val toString: dtlang -> string
				   
end
