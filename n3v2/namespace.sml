functor NamespaceFn (structure U: URI) :> NAMESPACE 
	where type U.uri = U.uri  = 
struct

structure U = U
	      
type prefix = string

datatype namespace = NS of {uri: U.uri, prefix: prefix}

fun new (uri: U.uri) pre = NS {uri = uri, prefix = pre}

fun uri (NS {uri,...}) = uri

fun prefix (NS {prefix,...}) = prefix

fun values (NS {uri, prefix}) = (uri,prefix)

end

structure Namespace = NamespaceFn (structure U = Uri)

