functor RDFSyntaxFn ( structure NS: NAMESPACE ) :> RDF_SYNTAX
 	where type NS.U.uri = NS.U.uri =
struct

structure NS = NS

val rdfUri = NS.U.fromString "http://www.w3.org/1999/02/22-rdf-syntax-ns#"
val rdfsUri = NS.U.fromString "http://www.w3.org/2000/01/rdf-schema#"
val owlUri = NS.U.fromString "http://www.w3.org/2002/07/owl#"
val logUri = NS.U.fromString "http://www.w3.org/2000/10/swap/log#"

val rdfns = NS.new rdfUri "rdf"
val rdfsns = NS.new rdfsUri "rdfs"
val owlns = NS.new owlUri "owl"
val logns = NS.new logUri "log"

val rdfType = NS.U.append rdfUri "type"
val owlSameAs = NS.U.append owlUri "sameAs"
val logImplies = NS.U.append logUri "implies"

end

structure RDFSyntax = RDFSyntaxFn ( structure NS = Namespace )


