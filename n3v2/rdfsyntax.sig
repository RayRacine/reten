signature RDF_SYNTAX =
sig

    structure NS: NAMESPACE
		  
    val rdfUri: NS.U.uri		
    val rdfsUri: NS.U.uri		 
    val owlUri: NS.U.uri
    val logUri: NS.U.uri
		
    val rdfns: NS.namespace
    val rdfsns: NS.namespace
    val owlns: NS.namespace

    val rdfType: NS.U.uri
    val owlSameAs: NS.U.uri
    val logImplies: NS.U.uri

end
