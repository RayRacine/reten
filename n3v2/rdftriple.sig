signature NTRIPLE =
sig

structure L: LITERAL
structure U: URI
structure NID: NODE_ID

structure S: sig
    type subject
    val fromUri: U.uri -> subject
    val fromNodeId: NID.node_id -> subject
end

structure P: sig
    type predicate
    val fromUri: U.uri -> predicate
end


structure O: sig
    type object
    val fromUri: U.uri -> object
    val fromNodeId: NID.node_id -> object
    val fromLiteral: L.literal -> object
end

type triple

val new: S.subject -> P.predicate -> O.object -> triple

val layout: triple -> Layout.t

end
