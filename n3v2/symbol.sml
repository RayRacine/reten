functor SymbolFn (structure U: URI structure Q:QNAME) :> SYMBOL = 
struct

structure U = U
structure Q = Q

datatype symbol = Uri of U.uri 
		| QName of Q.qname

fun fromUri u = Uri u

fun fromQName q = QName q

fun toString s = case s of
		     Uri u => U.toString u
		   | QName q => Q.toString q

fun layout s = Layout.str (toString s)

end

structure Symbol = SymbolFn (structure U = Uri structure Q = QName)
