structure Tokens = Tokens

type pos = int
type svalue = Tokens.svalue
type ('a,'b) token = ('a,'b) Tokens.token
type lexresult = (svalue,pos) token

val pos = ref 0
val eof = fn () => Tokens.EOF (!pos, !pos)

structure S = String


fun stripQuotes s =
    case s of
        "" => s
       | "\"\"" => s
       | s => S.extract (s,1, SOME (S.size s - 2))

(* N3 spec allows escaped quotes? for string.*)

(* 
{langcode}   => (Tokens.LANG_CODE (yytext, !pos, !pos));
langcode=[a-zA-Z0-9]+(-[a-zA-Z0-9]+)?;
{leadchars}{fillchars}* => (Tokens.BARENAME (yytext, !pos, !pos));
*)

%%
%header (functor N3LexFun(structure Tokens: N3_TOKENS));
%s C;
comment=#.*;
leadchars=[a-zA-Z_];
fillchars=[a-zA-Z0-9_];
explicituri=[<][^>,]*[>];
numeric_literal=[-+]?[0-9]+(\.[0-9]+)?(e[-+]?[0-9]+)?;
qname=(([a-zA-Z_][a-zA-Z0-9_]*)?:)([a-zA-Z_][a-z$@A-Z0-9_]*)?;
langcode=[a-z]+([-][a-z0-9]+)*;
string=\"([^"]*)\"; 
variable=[?][a-zA-Z_][a-zA-Z0-9_]*;
ws=[\ \t]; 

%%
\n  => (pos := (!pos) + 1; lex());
{ws}+ => (lex());
"," => ((* print "COMMA\n"; *) Tokens.COMMA (!pos, !pos));
";" => ((* print "SEMI\n"; *) Tokens.SEMI (!pos, !pos));
"(" => (Tokens.LPAREN (!pos, !pos));
")" => (Tokens.RPAREN (!pos, !pos));
"[" => ((*print "LBRAC\n";*) Tokens.LBRAC (!pos, !pos));
"]" => ((*print "RBRAC\n";*) Tokens.RBRAC (!pos, !pos));
"{" => ((*print "{\n";*)  Tokens.LCURL (!pos, !pos));
"}" => ((*print "}\n";*)  Tokens.RCURL (!pos, !pos));
"!" => (Tokens.BANG (!pos, !pos));
"." => ((*print "PERIOD\n";*) Tokens.PERIOD (!pos, !pos));
"=>" => ((*print "Implies\n";*) Tokens.RIGHT_ARROW (!pos, !pos));
"<=" => ((*print "Implies\n";*) Tokens.LEFT_ARROW (!pos, !pos));
"=" => ((*print "Equal\n";*) Tokens.QNAME ("owl:equivalentTo", !pos, !pos));

"^^"        => (Tokens.TYPE_OP (!pos, !pos));
"^"         => (Tokens.HAT (!pos, !pos));
"a"         => (Tokens.AT_A (!pos, !pos));
"@has"      => (Tokens.AT_HAS (!pos, !pos));
"@keywords" => (Tokens.KEYWORDS (!pos, !pos));
"@prefix"   => (Tokens.AT_PREFIX (!pos, !pos));
"@forSome"  => (Tokens.AT_FORSOME (!pos, !pos));
"@forAll"   => (Tokens.AT_FORALL (!pos, !pos));
"@"         => (Tokens.LANG_OP (!pos, !pos));

{numeric_literal} => (let val n = Real.fromString yytext
		      in 
		        case n of
		            SOME n => Tokens.NUMERIC_LITERAL (n, !pos, !pos)
		          | NONE => Tokens.NUMERIC_LITERAL (0.0, !pos, !pos)
                     end);
{explicituri} => ((*print ("URI: " ^ yytext ^ "\n");*) Tokens.EXPLICIT_URI (yytext, !pos, !pos));
{qname}     => ((*print ("QNAME |" ^ yytext ^ "|\n");*) Tokens.QNAME (yytext, !pos, !pos));
{string}    => ((*print ("Literal: " ^ yytext ^ "\n");*) Tokens.LITERAL (stripQuotes yytext, !pos, !pos));
{variable}  => ((*print ("Variable: " ^ yytext ^ "\n");*) Tokens.VARIABLE (yytext, !pos, !pos));
{langcode}  => ((*print ("LangCode: " ^ yytext ^ "\n");*) Tokens.LANGCODE  (yytext, !pos, !pos));

{comment} => ((*print ("Comment: " ^ yytext ^ "\n");*) pos := (!pos) + 1;lex()); 
