structure Scratch =
struct

val u = Uri.new "http://bravais.org"

val nid = NodeId.generate ()

val plit = Literal.plain "Ray Racine" NONE

val tlit = Literal.typed "123" (Uri.new "xsd:string")

structure T = NTriple

val triple1 = T.new (T.S.fromNodeId (NodeId.generate())) (T.P.fromURI u) (T.O.fromLiteral plit)
val triple2 = T.new (T.S.fromNodeId (NodeId.generate())) (T.P.fromURI u) (T.O.fromLiteral tlit)

fun test() = List.app (fn trip =>  Layout.outputln (T.layout trip, TextIO.stdOut))
		      [triple1,triple2]

end
