signature NTRIPLE =
sig

    structure L: LITERAL
    structure U: URI
    structure NID: NODE_ID
		   
    structure S: sig
	type t
	val fromUri: U.uri -> subject
	val fromNodeId: NID.node_id -> subject
    end
		 
    structure P: sig
	type t
	val fromUri: U.uri -> predicate
    end
		 
		 
    structure O: sig
	type t
	val fromUri: U.uri -> object
	val fromNodeId: NID.node_id -> object
	val fromLiteral: L.literal -> object
    end
		 
    type t
	 
    val new: S .t -> P.t -> O.t -> t
				   
    val isAnon: t -> bool
		     
    val layout: t -> Layout.t
		     
end
