structure N3NTriples :> N3_NTRIPLE where type N3A.document = N3Ast.document 
                                     and type NT.triple = NTriple.triple =

struct

structure N3A = N3Ast
structure NT = NTriple
structure NID = NodeId

structure U = Uri
structure Q = QName
structure NID = NodeId
structure L = Literal

datatype flatnode = UriRef of U.uri
		  | Anon of NID.node_id
		  | Literal of L.literal

fun symbol s = case s of
		   N3A.S.Uri u => UriRef u
		 | N3A.S.QName q => UriRef (U.fromString (Q.toString q))
				
fun node node = case node of 
		    N3A.Symbol s => (symbol s, [])
		  | N3A.Numeric n => (Literal (L.fromReal n),[])
		  | N3A.Literal (l,lang) => (Literal (L.plain l NONE), []) (* fix me NONE *)
		  | N3A.Anonymous props => (UriRef (Uri.fromString "Anon"),[])
		  | N3A.PathList plist => (Literal (L.plain "PathList" NONE), [])
		  | N3A.Formula _ => (Literal (L.plain "Formulat" NONE), [])
		  | N3A.Variable _ => (Literal (L.plain "Variable" NONE), [])

fun subjFromFlat n' = case n' of
			  UriRef u => NT.S.fromUri u
			| Anon nid => NT.S.fromNodeId nid
			| _ => raise Fail "NTriple predicate constrained to Anon or UriRef only."

fun predFromFlat (n: flatnode) = case n of
				     UriRef u => NT.P.fromUri u
				   | _ => raise Fail "NTriple predicate constrained to UriRef only."
				     
fun objFromFlat n' = case n' of
			 UriRef u => NT.O.fromUri u
		       | Anon nid => NT.O.fromNodeId nid
		       | Literal l => NT.O.fromLiteral l
			      
fun canonicalize s = s

fun symbolToExplicitUri symbol = canonicalize symbol

			 
fun segments (n, [], accum) = (n, [], accum)
  | segments (n, s::segs, accum) = 
      let val nid = NID.generate()
      in
	  case s of
	      N3A.Bang n' => let val subj = subjFromFlat n
				 val pred = let val (n,_) = node n' in predFromFlat n end
				 val obj = NT.O.fromNodeId nid
				 val triple = NT.new subj pred obj
			     in
				 segments (Anon nid, segs, triple::accum)
			     end
			     
	    | N3A.Hat n' => let val subj = NT.S.fromNodeId nid
				val pred = let val (n,_) = node n' in predFromFlat n end
				val obj = objFromFlat n
				val triple = NT.new subj pred obj
			    in
				segments (Anon nid, segs, triple::accum)
			    end
      end
				   
fun path (N3A.Path (n,segs)) = 
      let val (fnode,trips) = node n
	  val (node,_,trips) = segments (fnode, segs, trips)
      in
	  (node,trips)
      end
      
(* val subj = NT.S.fromNodeId (NID.generate())
val pred = NT.P.fromUri (U.fromString "http:///bravais.org/test#")
val obj  = NT.O.fromNodeId (NID.generate())
	   
val dummyTriple = NT.new subj pred obj *)

fun ntSubject fnode = case fnode of
			  UriRef u => NT.S.fromUri u
			| Anon nid => NT.S.fromNodeId nid
			| Literal l => raise Fail ("Literal value is invalid as subject of NTriple: " ^ (L.toString l))
		  
fun declaration _ = []

fun existential _ = []

fun universal _ = []
				
fun verb v = case v of 
		 N3A.VPath p => path p
	       | N3A.AKW => (UriRef RDFSyntax.rdfType, [])
	       (* need to mirror reverse triple *)
	       | N3A.IsOfKW p => path p 
	       | N3A.HasKW p => path p
	       | N3A.EQUAL => (UriRef RDFSyntax.owlSameAs, [])
	       (* need to mirror reverse triple *)
	       | N3A.LEFT_DARROW => (UriRef RDFSyntax.logImplies, []) 
	       | N3A.RIGHT_DARROW => (UriRef RDFSyntax.logImplies, [])
		 
fun property (N3A.Property (pred, paths)) = let val (vpred, vtrips) = verb pred
						fun flattenPath [] objects triples = (objects, triples)
						  | flattenPath (p::ps) objects triples = let val (node,trips) = path p      
											  in
											      flattenPath ps (node::objects) (trips @ triples)
											  end					  
						val (objects, triples) = flattenPath paths [] vtrips
					    in
						((predFromFlat vpred, map objFromFlat objects), triples)
					    end 

fun statement s = case s of 
		      N3A.Declaration d => declaration d
		    | N3A.Universal u => universal u
		    | N3A.Existential e => existential e
		    | N3A.SimpleStatement (subjectPath, propertylist) => let val (subjectnode, pathtriples) = path subjectPath
									     val subj: NT.S.subject = ntSubject subjectnode
									     fun flattenProps props accum: NT.triple list = 
										 case props of 
										     [] => accum
										   | (p::ps) => let val (pos, trips) = property p
												    val flatpos: NT.triple list  = let val (p,os) = pos
																   in
																       map (fn ano => NT.new subj p ano) os
																   end
												in 
												    flattenProps ps (flatpos @ accum)
												end
									 in
									     flattenProps propertylist []
									 end	
									 
fun document stmts =
    let fun fDoc (N3A.Statements [], accum) = List.rev accum
	  | fDoc (N3A.Statements (s::ss), accum) = fDoc (N3A.Statements ss, statement s @ accum)
    in
	fDoc (stmts,[])
    end     
end
