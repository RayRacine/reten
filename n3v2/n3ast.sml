
functor N3AstFn (structure Q: QNAME structure AU: URI): N3_AST 
        where type U.uri = AU.uri =
struct

  structure LO = Layout

  structure Q = Q
  structure U = AU
  
  type variable = string    
  type numeric = real
  type langcode = string
  type barename = string
		  				 		 
  structure S = 
  struct
    datatype symbol = Uri of U.uri
		    | QName of Q.qname

    val loSymbol = LO.str "Symbol "
    val loQName = LO.str "QName "
    val loUri = LO.str "Uri "

    fun layout sym = LO.seq [loSymbol, 
			     LO.paren (case sym of
					   Uri u => LO.seq [loUri, LO.str (U.toString u)]
					 | QName qn => LO.seq [loQName, LO.str (Q.toString qn)])]
			     
  end

  datatype dtlang = Language of langcode
		  | DatumType of S.symbol

  type literal = string * dtlang option

  datatype node = Symbol of S.symbol
		| Formula of statement list
		| Variable of variable
		| Numeric of  numeric
		| Literal of literal
		| Anonymous of property list 
		| PathList of path list   
			      
       and path_segment = Bang of node
			| Hat of node

       and path = Path of node * path_segment list
			  
       and  verb = VPath of path
		 | AKW 
		 | IsOfKW of path
		 | HasKW of path
		 | EQUAL
		 | LEFT_DARROW
		 | RIGHT_DARROW
		   
       and declaration = Prefix of Q.qname * U.uri
                       | Keywords of barename list 
				     
       and statement = Declaration of declaration
		     | Universal of S.symbol list 
		     | Existential of S.symbol list 
		     | SimpleStatement of path * property list
					  
       and property = Property of (verb * path list) 
				  
  type subject = path
		
  datatype document = Statements of statement list 

  val loBang = LO.str "Bang, "
  val loHat = LO.str "Hat, "
  val loSpace = LO.str " "
  val atA = LO.str "@"
  val isKW = LO.str " is "
  val ofKW = LO.str " of "
  val hasKW = LO.str " has "
  val leftB = LO.str "["
  val rightB = LO.str "]"
  val equalKW = LO.str "="
  val leftDArrow = LO.str "<="
  val rightDArrow = LO.str "=>"
  val forAll = LO.str "@forall "
  val forSome = LO.str "@forsome "
  val loSymbol = LO.str "Symbol "
  val loLiteral = LO.str "Literal "
  val loLParen = LO.str "( "
  val loRParen = LO.str " )"
  val loLBrak = LO.str "{"
  val loRBrak = LO.str "}"
  val loPath = LO.str "Path"
  val loVerb = LO.str "Verb "
  val loProperty = LO.str "Property "
  val loUniversal = LO.str "Universal "
  val loExistential = LO.str "Existential "
  val loDeclaration = LO.str "Declaration -"
  val loSimpleStatement = LO.str "SimpleStatement "
  val loPathList = LO.str "PathList "
  val loNode = LO.str "Node "
  val loNumeric = LO.str "Numeric "
  val loVariable = LO.str "Variable "
  val loFormula = LO.str "Formula "
  val loObjects = LO.str "Objects "
  val loSubject = LO.str "Subject "
  val loPrefix = LO.str "Prefix "
  val loKeyword = LO.str "Keyword "
  val loColon = LO.str ":"

  fun layoutSymbol sym = S.layout sym

  fun layoutLiteral (lit,_) = LO.seq [loLiteral, LO.str lit] (* fix me dtlang RPR *)
					  
  fun layoutPath (Path (node,pathsegs)) = LO.mayAlign [loPath, LO.seq [layoutNode node, LO.list (List.map layoutSegment pathsegs)]]
				   
  and layoutVerb verb = case verb of
			    VPath path => LO.seq [loVerb, LO.paren (layoutPath path)]
			  | AKW => LO.seq [loVerb, atA]
			  | IsOfKW path => LO.seq [loVerb, isKW, layoutPath path, ofKW]
			  | HasKW path => LO.seq [loVerb, hasKW, layoutPath path]
			  | EQUAL => LO.seq [loVerb, equalKW]
			  | LEFT_DARROW => LO.seq [loVerb, leftDArrow]
			  | RIGHT_DARROW => rightDArrow
					    
  and layoutProperty (Property (verb,paths)) = LO.seq [loProperty, LO.paren (LO.align [layoutVerb verb,
										       LO.seq [loObjects,
											       LO.list (List.map layoutPath paths)]])]
					       
  and layoutAnonymous props = LO.seq [leftB, LO.seq (List.map layoutProperty props), rightB]
			      
  and layoutNode n = case n of
			 Symbol s => LO.seq [loNode, loLParen,S.layout s, loRParen]
		       | Formula statements => LO.seq [loNode, loFormula, loLBrak, LO.mayAlign (map layoutStatement statements),loRBrak] 
		       | Variable v => LO.seq [loNode, LO.seq [loVariable, LO.str v]]
		       | Numeric num => LO.seq [loNode, LO.seq [loNumeric, LO.str (Real.toString num)]]
		       | Literal lit => LO.seq [loNode, loLParen, layoutLiteral lit,loRParen]
		       | Anonymous props => LO.seq [loNode, LO.tuple [layoutAnonymous props]]
		       | PathList paths => LO.seq [loNode,
						   LO.group [LO.align [loPathList, LO.list (List.map layoutPath paths)]]]
					   
  and layoutSegment seg = case seg of
			      Bang n => LO.seq [loBang, layoutNode n]
			    | Hat n => LO.seq [loHat, layoutNode n]
      
  and layoutSimpleStatement (path, props) =  LO.seq [loSimpleStatement,
						     LO.list (LO.seq [loSubject, LO.paren (layoutPath path)] :: (List.map layoutProperty props))]

  and layoutSymbols syms = LO.seq (List.map S.layout syms)

  and layoutDeclaration d = LO.mayAlign [loDeclaration,
					 case d of
					     Prefix (qname,uri) => LO.seq [loPrefix, LO.list [Q.layout qname, U.layout uri]]
					   | Keywords barenames => LO.seq [loKeyword, LO.list (List.map LO.str barenames)]]

  and layoutStatement stmt = case stmt of
				       SimpleStatement s => layoutSimpleStatement s
				     | Declaration d => layoutDeclaration d
				     | Existential e => LO.seq [loExistential, LO.list (List.map S.layout e)]
				     | Universal u => LO.seq [loUniversal, LO.list (List.map S.layout u)]

  fun layoutDocument (Statements stmnts) = LO.align (List.map layoutStatement stmnts)

				    
end

structure N3Ast = N3AstFn (structure Q = QName structure AU = Uri)
