structure QName :> QNAME = 
struct

type qname = string

val delim = #":"
val delimStr = String.str delim

fun fromPrefixValue p v =  if String.isSuffix delimStr p
			   then p ^ v
			   else String.concat [p,delimStr,v]

fun fromString s = s

(* optimize later *)
fun components sym = let val toks = String.tokens (fn c => c = delim) sym
		     in
			 if List.null toks
			 then {prefix="", name=""} (* should not happen with proper from checks *)
			 else if List.length toks = 2
		              then {prefix = List.nth (toks,0),
				    name = List.nth (toks,1)}
		               else {prefix = ":", name = (List.hd toks)}
		 end

fun comparePrefix sym compVal = let val {prefix,name} = components sym
				in
				    String.compare (prefix, compVal) = EQUAL
				end

fun isLocal sym = comparePrefix sym ""

fun isAnon sym = comparePrefix sym  "_"	 

fun equal (qn1,qn2) = String.compare (qn1,qn2) = EQUAL

fun toString sym = sym

fun layout sym = Layout.str (toString sym)

end
