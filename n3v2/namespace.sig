signature NAMESPACE = 
sig
    
    structure U: URI
		 
    type namespace
    type prefix = string
		  
    val new: U.uri -> prefix -> namespace
    val prefix: namespace -> prefix
    val uri: namespace -> U.uri
    val values: namespace -> U.uri * prefix
			     
end 
