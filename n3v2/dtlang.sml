functor DtLangFn (S: SYMBOL): DT_LANG =
struct

type langcode = string

datatype dtlang = Language of langcode
		| DatumType of S.symbol


fun toString dtl = case dtl of
		       Language s => s
		     | DatumType s => S.toString s

end

structure DtLang = DtLangFn (Symbol)


