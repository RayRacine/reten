structure NTripleRDF :> NTRIPLE_RDF =
struct

structure NT = NTriple
structure RDF = RDFSyntax

val rdfType = NT.P.fromUri RDF.rdfType

end
