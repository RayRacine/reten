functor N3LrValsFun(structure Token : TOKEN)
 : sig structure ParserData : PARSER_DATA
       structure Tokens : N3_TOKENS
   end
 = 
struct
structure ParserData=
struct
structure Header = 
struct

structure N3A = N3Ast
structure Q = QName
structure U = Uri


end
structure LrTable = Token.LrTable
structure Token = Token
local open LrTable in 
val table=let val actionRows =
"\
\\001\000\000\000\
\\001\000\001\000\000\000\000\000\
\\001\000\002\000\038\000\000\000\
\\001\000\004\000\025\000\007\000\024\000\008\000\054\000\009\000\023\000\
\\011\000\022\000\018\000\021\000\025\000\020\000\026\000\019\000\
\\027\000\018\000\000\000\
\\001\000\004\000\025\000\007\000\024\000\009\000\023\000\010\000\051\000\
\\011\000\022\000\013\000\037\000\014\000\036\000\015\000\035\000\
\\016\000\034\000\017\000\033\000\018\000\021\000\020\000\032\000\
\\025\000\020\000\026\000\019\000\027\000\018\000\000\000\
\\001\000\004\000\025\000\007\000\024\000\009\000\023\000\011\000\022\000\
\\013\000\037\000\014\000\036\000\015\000\035\000\016\000\034\000\
\\017\000\033\000\018\000\021\000\020\000\032\000\025\000\020\000\
\\026\000\019\000\027\000\018\000\000\000\
\\001\000\004\000\025\000\007\000\024\000\009\000\023\000\011\000\022\000\
\\018\000\021\000\025\000\020\000\026\000\019\000\027\000\018\000\000\000\
\\001\000\004\000\025\000\007\000\024\000\009\000\023\000\011\000\022\000\
\\018\000\021\000\025\000\020\000\026\000\019\000\027\000\018\000\
\\031\000\017\000\032\000\016\000\033\000\015\000\034\000\014\000\000\000\
\\001\000\004\000\025\000\018\000\021\000\000\000\
\\001\000\004\000\062\000\000\000\
\\001\000\008\000\071\000\000\000\
\\001\000\010\000\070\000\000\000\
\\001\000\012\000\069\000\000\000\
\\001\000\018\000\039\000\000\000\
\\001\000\021\000\079\000\000\000\
\\001\000\030\000\065\000\000\000\
\\088\000\000\000\
\\089\000\004\000\025\000\007\000\024\000\009\000\023\000\011\000\022\000\
\\018\000\021\000\025\000\020\000\026\000\019\000\027\000\018\000\
\\031\000\017\000\032\000\016\000\033\000\015\000\034\000\014\000\000\000\
\\090\000\000\000\
\\091\000\000\000\
\\092\000\002\000\068\000\000\000\
\\093\000\000\000\
\\094\000\000\000\
\\095\000\000\000\
\\096\000\000\000\
\\097\000\000\000\
\\098\000\000\000\
\\099\000\000\000\
\\100\000\000\000\
\\101\000\004\000\025\000\018\000\021\000\000\000\
\\102\000\000\000\
\\103\000\004\000\025\000\018\000\021\000\000\000\
\\104\000\000\000\
\\105\000\000\000\
\\106\000\004\000\025\000\007\000\024\000\009\000\023\000\011\000\022\000\
\\013\000\037\000\014\000\036\000\015\000\035\000\016\000\034\000\
\\017\000\033\000\018\000\021\000\020\000\032\000\025\000\020\000\
\\026\000\019\000\027\000\018\000\000\000\
\\107\000\000\000\
\\108\000\019\000\078\000\000\000\
\\109\000\000\000\
\\110\000\019\000\078\000\022\000\077\000\000\000\
\\111\000\000\000\
\\112\000\000\000\
\\113\000\022\000\077\000\000\000\
\\114\000\000\000\
\\115\000\000\000\
\\116\000\000\000\
\\117\000\000\000\
\\118\000\000\000\
\\119\000\000\000\
\\120\000\000\000\
\\121\000\000\000\
\\122\000\000\000\
\\123\000\000\000\
\\124\000\023\000\028\000\024\000\027\000\000\000\
\\125\000\000\000\
\\126\000\000\000\
\\127\000\023\000\028\000\024\000\027\000\000\000\
\\128\000\023\000\028\000\024\000\027\000\000\000\
\\129\000\000\000\
\\130\000\000\000\
\\131\000\000\000\
\\132\000\000\000\
\\133\000\000\000\
\\134\000\000\000\
\\135\000\000\000\
\\136\000\000\000\
\\137\000\000\000\
\\138\000\004\000\025\000\007\000\024\000\009\000\023\000\011\000\022\000\
\\018\000\021\000\025\000\020\000\026\000\019\000\027\000\018\000\000\000\
\\139\000\000\000\
\\140\000\000\000\
\\141\000\000\000\
\\142\000\000\000\
\\143\000\028\000\046\000\029\000\045\000\000\000\
\\144\000\000\000\
\\145\000\000\000\
\\146\000\022\000\064\000\000\000\
\\147\000\000\000\
\\148\000\000\000\
\\149\000\022\000\064\000\000\000\
\"
val actionRowNumbers =
"\007\000\059\000\057\000\052\000\
\\049\000\034\000\026\000\024\000\
\\025\000\023\000\002\000\016\000\
\\013\000\000\000\029\000\031\000\
\\071\000\060\000\058\000\069\000\
\\007\000\004\000\003\000\068\000\
\\051\000\006\000\006\000\042\000\
\\006\000\033\000\006\000\048\000\
\\047\000\046\000\006\000\045\000\
\\017\000\009\000\028\000\030\000\
\\074\000\032\000\070\000\015\000\
\\008\000\020\000\019\000\012\000\
\\011\000\063\000\010\000\066\000\
\\061\000\055\000\056\000\050\000\
\\038\000\014\000\043\000\018\000\
\\027\000\075\000\008\000\072\000\
\\073\000\021\000\007\000\065\000\
\\064\000\062\000\067\000\053\000\
\\054\000\036\000\037\000\006\000\
\\005\000\044\000\077\000\022\000\
\\035\000\041\000\039\000\076\000\
\\040\000\001\000"
val gotoT =
"\
\\001\000\085\000\002\000\011\000\006\000\010\000\007\000\009\000\
\\008\000\008\000\009\000\007\000\010\000\006\000\015\000\005\000\
\\017\000\004\000\019\000\003\000\022\000\002\000\024\000\001\000\000\000\
\\000\000\
\\000\000\
\\018\000\024\000\000\000\
\\000\000\
\\011\000\029\000\014\000\028\000\017\000\027\000\019\000\003\000\
\\022\000\002\000\024\000\001\000\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\023\000\038\000\000\000\
\\024\000\040\000\025\000\039\000\000\000\
\\024\000\040\000\025\000\041\000\000\000\
\\021\000\042\000\000\000\
\\000\000\
\\000\000\
\\000\000\
\\003\000\047\000\004\000\046\000\006\000\045\000\007\000\009\000\
\\008\000\008\000\009\000\007\000\010\000\006\000\015\000\005\000\
\\017\000\004\000\019\000\003\000\022\000\002\000\024\000\001\000\000\000\
\\011\000\048\000\014\000\028\000\017\000\027\000\019\000\003\000\
\\022\000\002\000\024\000\001\000\000\000\
\\017\000\051\000\019\000\003\000\020\000\050\000\022\000\002\000\
\\024\000\001\000\000\000\
\\000\000\
\\000\000\
\\019\000\053\000\022\000\002\000\024\000\001\000\000\000\
\\019\000\054\000\022\000\002\000\024\000\001\000\000\000\
\\000\000\
\\016\000\056\000\017\000\055\000\019\000\003\000\022\000\002\000\
\\024\000\001\000\000\000\
\\000\000\
\\017\000\057\000\019\000\003\000\022\000\002\000\024\000\001\000\000\000\
\\000\000\
\\000\000\
\\000\000\
\\017\000\058\000\019\000\003\000\022\000\002\000\024\000\001\000\000\000\
\\000\000\
\\002\000\059\000\006\000\010\000\007\000\009\000\008\000\008\000\
\\009\000\007\000\010\000\006\000\015\000\005\000\017\000\004\000\
\\019\000\003\000\022\000\002\000\024\000\001\000\000\000\
\\000\000\
\\000\000\
\\000\000\
\\026\000\061\000\000\000\
\\000\000\
\\000\000\
\\000\000\
\\024\000\064\000\000\000\
\\005\000\065\000\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\017\000\051\000\019\000\003\000\020\000\070\000\022\000\002\000\
\\024\000\001\000\000\000\
\\000\000\
\\018\000\071\000\000\000\
\\018\000\072\000\000\000\
\\000\000\
\\012\000\074\000\013\000\073\000\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\024\000\078\000\000\000\
\\000\000\
\\000\000\
\\000\000\
\\004\000\079\000\006\000\045\000\007\000\009\000\008\000\008\000\
\\009\000\007\000\010\000\006\000\015\000\005\000\017\000\004\000\
\\019\000\003\000\022\000\002\000\024\000\001\000\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\012\000\080\000\000\000\
\\000\000\
\\016\000\081\000\017\000\055\000\019\000\003\000\022\000\002\000\
\\024\000\001\000\000\000\
\\011\000\082\000\014\000\028\000\017\000\027\000\019\000\003\000\
\\022\000\002\000\024\000\001\000\000\000\
\\000\000\
\\026\000\083\000\000\000\
\\000\000\
\\000\000\
\\013\000\084\000\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\"
val numstates = 86
val numrules = 62
val s = ref "" and index = ref 0
val string_to_int = fn () => 
let val i = !index
in index := i+2; Char.ord(String.sub(!s,i)) + Char.ord(String.sub(!s,i+1)) * 256
end
val string_to_list = fn s' =>
    let val len = String.size s'
        fun f () =
           if !index < len then string_to_int() :: f()
           else nil
   in index := 0; s := s'; f ()
   end
val string_to_pairlist = fn (conv_key,conv_entry) =>
     let fun f () =
         case string_to_int()
         of 0 => EMPTY
          | n => PAIR(conv_key (n-1),conv_entry (string_to_int()),f())
     in f
     end
val string_to_pairlist_default = fn (conv_key,conv_entry) =>
    let val conv_row = string_to_pairlist(conv_key,conv_entry)
    in fn () =>
       let val default = conv_entry(string_to_int())
           val row = conv_row()
       in (row,default)
       end
   end
val string_to_table = fn (convert_row,s') =>
    let val len = String.size s'
        fun f ()=
           if !index < len then convert_row() :: f()
           else nil
     in (s := s'; index := 0; f ())
     end
local
  val memo = Array.array(numstates+numrules,ERROR)
  val _ =let fun g i=(Array.update(memo,i,REDUCE(i-numstates)); g(i+1))
       fun f i =
            if i=numstates then g i
            else (Array.update(memo,i,SHIFT (STATE i)); f (i+1))
          in f 0 handle Subscript => ()
          end
in
val entry_to_action = fn 0 => ACCEPT | 1 => ERROR | j => Array.sub(memo,(j-2))
end
val gotoT=Array.fromList(string_to_table(string_to_pairlist(NT,STATE),gotoT))
val actionRows=string_to_table(string_to_pairlist_default(T,entry_to_action),actionRows)
val actionRowNumbers = string_to_list actionRowNumbers
val actionT = let val actionRowLookUp=
let val a=Array.fromList(actionRows) in fn i=>Array.sub(a,i) end
in Array.fromList(map actionRowLookUp actionRowNumbers)
end
in LrTable.mkLrTable {actions=actionT,gotos=gotoT,numRules=numrules,
numStates=numstates,initialState=STATE 0}
end
end
local open Header in
type pos = int
type arg = unit
structure MlyValue = 
struct
datatype svalue = VOID | ntVOID of unit | LANGCODE of  (string)
 | LITERAL of  (string) | VARIABLE of  (string)
 | NUMERIC_LITERAL of  (real) | QNAME of  (string)
 | EXPLICIT_URI of  (string) | SYMBOL_CSL_TAIL of  (N3A.S.symbol list)
 | SYMBOL_CSL of  (N3A.S.symbol list) | SYMBOL of  (N3A.S.symbol)
 | BARENAME_CSL of  (string list) | STRING_LITERAL of  (N3A.node)
 | DTLANG of  (N3A.dtlang) | PATH_LIST of  (N3A.path list)
 | NODE of  (N3A.node) | PATH_TAIL of  (N3A.path_segment list)
 | PATH of  (N3A.path) | OBJECT of  (N3A.path)
 | SUBJECT of  (N3A.path) | VERB of  (N3A.verb)
 | OBJECT_TAIL of  (N3A.path list)
 | PROPERTY_LIST_TAIL of  (N3A.property list)
 | PROPERTY_LIST of  (N3A.property list)
 | SIMPLE_STATEMENT of  (N3A.statement)
 | UNIVERSAL of  (N3A.statement) | EXISTENTIAL of  (N3A.statement)
 | DECLARATION of  (N3A.declaration) | STATEMENT of  (N3A.statement)
 | STATEMENT_TAIL of  (N3A.statement list)
 | STATEMENT_LIST of  (N3A.statement list)
 | FORMULA_CONTENT of  (N3A.statement list)
 | STATEMENTS_OPTIONAL of  (N3A.statement list)
 | N3DOCUMENT of  (N3A.document)
end
type svalue = MlyValue.svalue
type result = N3A.document
end
structure EC=
struct
open LrTable
val is_keyword =
fn (T 4) => true | (T 5) => true | (T 6) => true | (T 7) => true | (T 
8) => true | (T 9) => true | (T 10) => true | (T 11) => true | (T 12)
 => true | (T 13) => true | (T 19) => true | (T 20) => true | (T 14)
 => true | (T 15) => true | (T 16) => true | (T 21) => true | (T 22)
 => true | (T 23) => true | (T 1) => true | (T 28) => true | (T 27)
 => true | (T 31) => true | (T 30) => true | _ => false
val preferred_change = 
nil
val noShift = 
fn (T 0) => true | _ => false
val showTerminal =
fn (T 0) => "EOF"
  | (T 1) => "PERIOD"
  | (T 2) => "BARENAME"
  | (T 3) => "EXPLICIT_URI"
  | (T 4) => "PREFIX"
  | (T 5) => "KEYWORDS"
  | (T 6) => "LPAREN"
  | (T 7) => "RPAREN"
  | (T 8) => "LBRAC"
  | (T 9) => "RBRAC"
  | (T 10) => "LCURL"
  | (T 11) => "RCURL"
  | (T 12) => "AT_A"
  | (T 13) => "AT_HAS"
  | (T 14) => "EQUAL"
  | (T 15) => "LEFT_ARROW"
  | (T 16) => "RIGHT_ARROW"
  | (T 17) => "QNAME"
  | (T 18) => "SEMI"
  | (T 19) => "AT_IS"
  | (T 20) => "AT_OF"
  | (T 21) => "COMMA"
  | (T 22) => "HAT"
  | (T 23) => "BANG"
  | (T 24) => "NUMERIC_LITERAL"
  | (T 25) => "VARIABLE"
  | (T 26) => "LITERAL"
  | (T 27) => "TYPE_OP"
  | (T 28) => "LANG_OP"
  | (T 29) => "LANGCODE"
  | (T 30) => "AT_FORALL"
  | (T 31) => "AT_FORSOME"
  | (T 32) => "AT_KEYWORDS"
  | (T 33) => "AT_PREFIX"
  | _ => "bogus-term"
local open Header in
val errtermvalue=
fn _ => MlyValue.VOID
end
val terms = (T 0) :: (T 1) :: (T 2) :: (T 4) :: (T 5) :: (T 6) :: (T 7
) :: (T 8) :: (T 9) :: (T 10) :: (T 11) :: (T 12) :: (T 13) :: (T 14)
 :: (T 15) :: (T 16) :: (T 18) :: (T 19) :: (T 20) :: (T 21) :: (T 22)
 :: (T 23) :: (T 27) :: (T 28) :: (T 30) :: (T 31) :: (T 32) :: (T 33)
 :: nil
end
structure Actions =
struct 
type int = Int.int
exception mlyAction of int
local open Header in
val actions = 
fn (i392:int,defaultPos,stack,
    (()):arg) =>
case (i392,stack)
of (0,(_,(MlyValue.STATEMENTS_OPTIONAL STATEMENTS_OPTIONAL,
STATEMENTS_OPTIONAL1left,STATEMENTS_OPTIONAL1right))::rest671) => let 
val result=MlyValue.N3DOCUMENT((N3A.Statements STATEMENTS_OPTIONAL))
 in (LrTable.NT 0,(result,STATEMENTS_OPTIONAL1left,
STATEMENTS_OPTIONAL1right),rest671) end
| (1,(_,(_,_,PERIOD1right))::(_,(MlyValue.STATEMENT STATEMENT,
STATEMENT1left,_))::rest671) => let val result=
MlyValue.STATEMENTS_OPTIONAL(([STATEMENT]))
 in (LrTable.NT 1,(result,STATEMENT1left,PERIOD1right),rest671) end
| (2,(_,(MlyValue.STATEMENTS_OPTIONAL STATEMENTS_OPTIONAL,_,
STATEMENTS_OPTIONAL1right))::_::(_,(MlyValue.STATEMENT STATEMENT,
STATEMENT1left,_))::rest671) => let val result=
MlyValue.STATEMENTS_OPTIONAL((STATEMENT :: STATEMENTS_OPTIONAL))
 in (LrTable.NT 1,(result,STATEMENT1left,STATEMENTS_OPTIONAL1right),
rest671) end
| (3,(_,(MlyValue.STATEMENT_LIST STATEMENT_LIST,STATEMENT_LIST1left,
STATEMENT_LIST1right))::rest671) => let val result=
MlyValue.FORMULA_CONTENT((STATEMENT_LIST))
 in (LrTable.NT 2,(result,STATEMENT_LIST1left,STATEMENT_LIST1right),
rest671) end
| (4,(_,(MlyValue.STATEMENT STATEMENT,STATEMENT1left,STATEMENT1right))
::rest671) => let val result=MlyValue.STATEMENT_LIST(([STATEMENT]))
 in (LrTable.NT 3,(result,STATEMENT1left,STATEMENT1right),rest671) end
| (5,(_,(MlyValue.STATEMENT_TAIL STATEMENT_TAIL,_,STATEMENT_TAIL1right
))::(_,(MlyValue.STATEMENT STATEMENT,STATEMENT1left,_))::rest671) => 
let val result=MlyValue.STATEMENT_LIST((STATEMENT :: STATEMENT_TAIL))
 in (LrTable.NT 3,(result,STATEMENT1left,STATEMENT_TAIL1right),rest671
) end
| (6,(_,(MlyValue.STATEMENT_LIST STATEMENT_LIST,_,STATEMENT_LIST1right
))::(_,(_,PERIOD1left,_))::rest671) => let val result=
MlyValue.STATEMENT_TAIL((STATEMENT_LIST))
 in (LrTable.NT 4,(result,PERIOD1left,STATEMENT_LIST1right),rest671)
 end
| (7,(_,(MlyValue.DECLARATION DECLARATION,DECLARATION1left,
DECLARATION1right))::rest671) => let val result=MlyValue.STATEMENT((
N3A.Declaration DECLARATION))
 in (LrTable.NT 5,(result,DECLARATION1left,DECLARATION1right),rest671)
 end
| (8,(_,(MlyValue.UNIVERSAL UNIVERSAL,UNIVERSAL1left,UNIVERSAL1right))
::rest671) => let val result=MlyValue.STATEMENT((UNIVERSAL))
 in (LrTable.NT 5,(result,UNIVERSAL1left,UNIVERSAL1right),rest671) end
| (9,(_,(MlyValue.EXISTENTIAL EXISTENTIAL,EXISTENTIAL1left,
EXISTENTIAL1right))::rest671) => let val result=MlyValue.STATEMENT((
EXISTENTIAL))
 in (LrTable.NT 5,(result,EXISTENTIAL1left,EXISTENTIAL1right),rest671)
 end
| (10,(_,(MlyValue.SIMPLE_STATEMENT SIMPLE_STATEMENT,
SIMPLE_STATEMENT1left,SIMPLE_STATEMENT1right))::rest671) => let val 
result=MlyValue.STATEMENT((SIMPLE_STATEMENT))
 in (LrTable.NT 5,(result,SIMPLE_STATEMENT1left,SIMPLE_STATEMENT1right
),rest671) end
| (11,(_,(MlyValue.EXPLICIT_URI EXPLICIT_URI,_,EXPLICIT_URI1right))::(
_,(MlyValue.QNAME QNAME,_,_))::(_,(_,AT_PREFIX1left,_))::rest671) => 
let val result=MlyValue.DECLARATION((
N3A.Prefix (Q.fromString QNAME, U.fromString EXPLICIT_URI)))
 in (LrTable.NT 6,(result,AT_PREFIX1left,EXPLICIT_URI1right),rest671)
 end
| (12,(_,(MlyValue.BARENAME_CSL BARENAME_CSL,_,BARENAME_CSL1right))::(
_,(_,AT_KEYWORDS1left,_))::rest671) => let val result=
MlyValue.DECLARATION((N3A.Keywords BARENAME_CSL))
 in (LrTable.NT 6,(result,AT_KEYWORDS1left,BARENAME_CSL1right),rest671
) end
| (13,(_,(_,AT_FORSOME1left,AT_FORSOME1right))::rest671) => let val 
result=MlyValue.EXISTENTIAL((N3A.Existential []))
 in (LrTable.NT 7,(result,AT_FORSOME1left,AT_FORSOME1right),rest671)
 end
| (14,(_,(MlyValue.SYMBOL_CSL SYMBOL_CSL,_,SYMBOL_CSL1right))::(_,(_,
AT_FORSOME1left,_))::rest671) => let val result=MlyValue.EXISTENTIAL((
N3A.Existential SYMBOL_CSL))
 in (LrTable.NT 7,(result,AT_FORSOME1left,SYMBOL_CSL1right),rest671)
 end
| (15,(_,(_,AT_FORALL1left,AT_FORALL1right))::rest671) => let val 
result=MlyValue.UNIVERSAL((N3A.Universal []))
 in (LrTable.NT 8,(result,AT_FORALL1left,AT_FORALL1right),rest671) end
| (16,(_,(MlyValue.SYMBOL_CSL SYMBOL_CSL,_,SYMBOL_CSL1right))::(_,(_,
AT_FORALL1left,_))::rest671) => let val result=MlyValue.UNIVERSAL((
N3A.Universal SYMBOL_CSL))
 in (LrTable.NT 8,(result,AT_FORALL1left,SYMBOL_CSL1right),rest671)
 end
| (17,(_,(MlyValue.PROPERTY_LIST PROPERTY_LIST,_,PROPERTY_LIST1right))
::(_,(MlyValue.SUBJECT SUBJECT,SUBJECT1left,_))::rest671) => let val 
result=MlyValue.SIMPLE_STATEMENT((
N3A.SimpleStatement (SUBJECT, PROPERTY_LIST)))
 in (LrTable.NT 9,(result,SUBJECT1left,PROPERTY_LIST1right),rest671)
 end
| (18,(_,(MlyValue.SUBJECT SUBJECT,SUBJECT1left,SUBJECT1right))::
rest671) => let val result=MlyValue.SIMPLE_STATEMENT((
N3A.SimpleStatement (SUBJECT, [])))
 in (LrTable.NT 9,(result,SUBJECT1left,SUBJECT1right),rest671) end
| (19,(_,(MlyValue.PROPERTY_LIST_TAIL PROPERTY_LIST_TAIL,_,
PROPERTY_LIST_TAIL1right))::(_,(MlyValue.OBJECT_TAIL OBJECT_TAIL,_,_))
::(_,(MlyValue.OBJECT OBJECT,_,_))::(_,(MlyValue.VERB VERB,VERB1left,_
))::rest671) => let val result=MlyValue.PROPERTY_LIST((
(N3A.Property (VERB,OBJECT::OBJECT_TAIL)) :: PROPERTY_LIST_TAIL))
 in (LrTable.NT 10,(result,VERB1left,PROPERTY_LIST_TAIL1right),rest671
) end
| (20,(_,(MlyValue.OBJECT_TAIL OBJECT_TAIL,_,OBJECT_TAIL1right))::(_,(
MlyValue.OBJECT OBJECT,_,_))::(_,(MlyValue.VERB VERB,VERB1left,_))::
rest671) => let val result=MlyValue.PROPERTY_LIST((
[N3A.Property (VERB,OBJECT::OBJECT_TAIL)]))
 in (LrTable.NT 10,(result,VERB1left,OBJECT_TAIL1right),rest671) end
| (21,(_,(MlyValue.PROPERTY_LIST_TAIL PROPERTY_LIST_TAIL,_,
PROPERTY_LIST_TAIL1right))::(_,(MlyValue.OBJECT OBJECT,_,_))::(_,(
MlyValue.VERB VERB,VERB1left,_))::rest671) => let val result=
MlyValue.PROPERTY_LIST((
(N3A.Property (VERB,[OBJECT])) :: PROPERTY_LIST_TAIL))
 in (LrTable.NT 10,(result,VERB1left,PROPERTY_LIST_TAIL1right),rest671
) end
| (22,(_,(MlyValue.OBJECT OBJECT,_,OBJECT1right))::(_,(MlyValue.VERB 
VERB,VERB1left,_))::rest671) => let val result=MlyValue.PROPERTY_LIST(
([N3A.Property (VERB,[OBJECT])]))
 in (LrTable.NT 10,(result,VERB1left,OBJECT1right),rest671) end
| (23,(_,(MlyValue.PROPERTY_LIST PROPERTY_LIST,_,PROPERTY_LIST1right))
::(_,(_,SEMI1left,_))::rest671) => let val result=
MlyValue.PROPERTY_LIST_TAIL((PROPERTY_LIST))
 in (LrTable.NT 11,(result,SEMI1left,PROPERTY_LIST1right),rest671) end
| (24,(_,(MlyValue.OBJECT_TAIL OBJECT_TAIL,_,OBJECT_TAIL1right))::(_,(
MlyValue.OBJECT OBJECT,_,_))::(_,(_,COMMA1left,_))::rest671) => let 
val result=MlyValue.OBJECT_TAIL((OBJECT :: OBJECT_TAIL))
 in (LrTable.NT 12,(result,COMMA1left,OBJECT_TAIL1right),rest671) end
| (25,(_,(MlyValue.OBJECT OBJECT,_,OBJECT1right))::(_,(_,COMMA1left,_)
)::rest671) => let val result=MlyValue.OBJECT_TAIL(([OBJECT]))
 in (LrTable.NT 12,(result,COMMA1left,OBJECT1right),rest671) end
| (26,(_,(MlyValue.PATH PATH,PATH1left,PATH1right))::rest671) => let 
val result=MlyValue.VERB((N3A.VPath PATH))
 in (LrTable.NT 13,(result,PATH1left,PATH1right),rest671) end
| (27,(_,(MlyValue.PATH PATH,_,PATH1right))::(_,(_,AT_HAS1left,_))::
rest671) => let val result=MlyValue.VERB((N3A.HasKW PATH))
 in (LrTable.NT 13,(result,AT_HAS1left,PATH1right),rest671) end
| (28,(_,(_,_,AT_OF1right))::(_,(MlyValue.PATH PATH,_,_))::(_,(_,
AT_IS1left,_))::rest671) => let val result=MlyValue.VERB((
N3A.IsOfKW PATH))
 in (LrTable.NT 13,(result,AT_IS1left,AT_OF1right),rest671) end
| (29,(_,(_,AT_A1left,AT_A1right))::rest671) => let val result=
MlyValue.VERB((N3A.AKW))
 in (LrTable.NT 13,(result,AT_A1left,AT_A1right),rest671) end
| (30,(_,(_,EQUAL1left,EQUAL1right))::rest671) => let val result=
MlyValue.VERB((N3A.EQUAL))
 in (LrTable.NT 13,(result,EQUAL1left,EQUAL1right),rest671) end
| (31,(_,(_,LEFT_ARROW1left,LEFT_ARROW1right))::rest671) => let val 
result=MlyValue.VERB((N3A.LEFT_DARROW))
 in (LrTable.NT 13,(result,LEFT_ARROW1left,LEFT_ARROW1right),rest671)
 end
| (32,(_,(_,RIGHT_ARROW1left,RIGHT_ARROW1right))::rest671) => let val 
result=MlyValue.VERB((N3A.RIGHT_DARROW))
 in (LrTable.NT 13,(result,RIGHT_ARROW1left,RIGHT_ARROW1right),rest671
) end
| (33,(_,(MlyValue.PATH PATH,PATH1left,PATH1right))::rest671) => let 
val result=MlyValue.SUBJECT((PATH))
 in (LrTable.NT 14,(result,PATH1left,PATH1right),rest671) end
| (34,(_,(MlyValue.PATH PATH,PATH1left,PATH1right))::rest671) => let 
val result=MlyValue.OBJECT((PATH))
 in (LrTable.NT 15,(result,PATH1left,PATH1right),rest671) end
| (35,(_,(MlyValue.PATH_TAIL PATH_TAIL,_,PATH_TAIL1right))::(_,(
MlyValue.NODE NODE,NODE1left,_))::rest671) => let val result=
MlyValue.PATH((N3A.Path (NODE, PATH_TAIL)))
 in (LrTable.NT 16,(result,NODE1left,PATH_TAIL1right),rest671) end
| (36,(_,(MlyValue.NODE NODE,NODE1left,NODE1right))::rest671) => let 
val result=MlyValue.PATH((N3A.Path (NODE, [])))
 in (LrTable.NT 16,(result,NODE1left,NODE1right),rest671) end
| (37,(_,(MlyValue.PATH_TAIL PATH_TAIL,_,PATH_TAIL1right))::(_,(
MlyValue.NODE NODE,_,_))::(_,(_,BANG1left,_))::rest671) => let val 
result=MlyValue.PATH_TAIL((N3A.Bang NODE :: PATH_TAIL))
 in (LrTable.NT 17,(result,BANG1left,PATH_TAIL1right),rest671) end
| (38,(_,(MlyValue.PATH_TAIL PATH_TAIL,_,PATH_TAIL1right))::(_,(
MlyValue.NODE NODE,_,_))::(_,(_,HAT1left,_))::rest671) => let val 
result=MlyValue.PATH_TAIL((N3A.Hat NODE :: PATH_TAIL))
 in (LrTable.NT 17,(result,HAT1left,PATH_TAIL1right),rest671) end
| (39,(_,(MlyValue.NODE NODE,_,NODE1right))::(_,(_,BANG1left,_))::
rest671) => let val result=MlyValue.PATH_TAIL(([N3A.Bang NODE]))
 in (LrTable.NT 17,(result,BANG1left,NODE1right),rest671) end
| (40,(_,(MlyValue.NODE NODE,_,NODE1right))::(_,(_,HAT1left,_))::
rest671) => let val result=MlyValue.PATH_TAIL(([N3A.Hat NODE]))
 in (LrTable.NT 17,(result,HAT1left,NODE1right),rest671) end
| (41,(_,(MlyValue.STRING_LITERAL STRING_LITERAL,STRING_LITERAL1left,
STRING_LITERAL1right))::rest671) => let val result=MlyValue.NODE((
STRING_LITERAL))
 in (LrTable.NT 18,(result,STRING_LITERAL1left,STRING_LITERAL1right),
rest671) end
| (42,(_,(MlyValue.NUMERIC_LITERAL NUMERIC_LITERAL,
NUMERIC_LITERAL1left,NUMERIC_LITERAL1right))::rest671) => let val 
result=MlyValue.NODE((N3A.Numeric NUMERIC_LITERAL))
 in (LrTable.NT 18,(result,NUMERIC_LITERAL1left,NUMERIC_LITERAL1right)
,rest671) end
| (43,(_,(MlyValue.SYMBOL SYMBOL,SYMBOL1left,SYMBOL1right))::rest671)
 => let val result=MlyValue.NODE((N3A.Symbol SYMBOL))
 in (LrTable.NT 18,(result,SYMBOL1left,SYMBOL1right),rest671) end
| (44,(_,(MlyValue.VARIABLE VARIABLE,VARIABLE1left,VARIABLE1right))::
rest671) => let val result=MlyValue.NODE((N3A.Variable VARIABLE))
 in (LrTable.NT 18,(result,VARIABLE1left,VARIABLE1right),rest671) end
| (45,(_,(_,_,RPAREN1right))::(_,(_,LPAREN1left,_))::rest671) => let 
val result=MlyValue.NODE((N3A.PathList []))
 in (LrTable.NT 18,(result,LPAREN1left,RPAREN1right),rest671) end
| (46,(_,(_,_,RPAREN1right))::(_,(MlyValue.PATH_LIST PATH_LIST,_,_))::
(_,(_,LPAREN1left,_))::rest671) => let val result=MlyValue.NODE((
N3A.PathList PATH_LIST))
 in (LrTable.NT 18,(result,LPAREN1left,RPAREN1right),rest671) end
| (47,(_,(_,_,RBRAC1right))::(_,(_,LBRAC1left,_))::rest671) => let 
val result=MlyValue.NODE((N3A.Anonymous []))
 in (LrTable.NT 18,(result,LBRAC1left,RBRAC1right),rest671) end
| (48,(_,(_,_,RBRAC1right))::(_,(MlyValue.PROPERTY_LIST PROPERTY_LIST,
_,_))::(_,(_,LBRAC1left,_))::rest671) => let val result=MlyValue.NODE(
(N3A.Anonymous PROPERTY_LIST))
 in (LrTable.NT 18,(result,LBRAC1left,RBRAC1right),rest671) end
| (49,(_,(_,_,RCURL1right))::(_,(MlyValue.FORMULA_CONTENT 
FORMULA_CONTENT,_,_))::(_,(_,LCURL1left,_))::rest671) => let val 
result=MlyValue.NODE((N3A.Formula FORMULA_CONTENT))
 in (LrTable.NT 18,(result,LCURL1left,RCURL1right),rest671) end
| (50,(_,(MlyValue.PATH PATH,PATH1left,PATH1right))::rest671) => let 
val result=MlyValue.PATH_LIST(([PATH]))
 in (LrTable.NT 19,(result,PATH1left,PATH1right),rest671) end
| (51,(_,(MlyValue.PATH_LIST PATH_LIST,_,PATH_LIST1right))::(_,(
MlyValue.PATH PATH,PATH1left,_))::rest671) => let val result=
MlyValue.PATH_LIST((PATH :: PATH_LIST))
 in (LrTable.NT 19,(result,PATH1left,PATH_LIST1right),rest671) end
| (52,(_,(MlyValue.EXPLICIT_URI EXPLICIT_URI,EXPLICIT_URI1left,
EXPLICIT_URI1right))::rest671) => let val result=MlyValue.SYMBOL((
N3A.S.Uri (U.fromString EXPLICIT_URI)))
 in (LrTable.NT 23,(result,EXPLICIT_URI1left,EXPLICIT_URI1right),
rest671) end
| (53,(_,(MlyValue.QNAME QNAME,QNAME1left,QNAME1right))::rest671) => 
let val result=MlyValue.SYMBOL((N3A.S.QName (Q.fromString QNAME)))
 in (LrTable.NT 23,(result,QNAME1left,QNAME1right),rest671) end
| (54,(_,(MlyValue.DTLANG DTLANG,_,DTLANG1right))::(_,(
MlyValue.LITERAL LITERAL,LITERAL1left,_))::rest671) => let val result=
MlyValue.STRING_LITERAL((N3A.Literal (LITERAL, SOME DTLANG)))
 in (LrTable.NT 21,(result,LITERAL1left,DTLANG1right),rest671) end
| (55,(_,(MlyValue.LITERAL LITERAL,LITERAL1left,LITERAL1right))::
rest671) => let val result=MlyValue.STRING_LITERAL((
N3A.Literal (LITERAL,NONE)))
 in (LrTable.NT 21,(result,LITERAL1left,LITERAL1right),rest671) end
| (56,(_,(MlyValue.LANGCODE LANGCODE,_,LANGCODE1right))::(_,(_,
LANG_OP1left,_))::rest671) => let val result=MlyValue.DTLANG((
N3A.Language LANGCODE))
 in (LrTable.NT 20,(result,LANG_OP1left,LANGCODE1right),rest671) end
| (57,(_,(MlyValue.SYMBOL SYMBOL,_,SYMBOL1right))::(_,(_,TYPE_OP1left,
_))::rest671) => let val result=MlyValue.DTLANG((N3A.DatumType SYMBOL)
)
 in (LrTable.NT 20,(result,TYPE_OP1left,SYMBOL1right),rest671) end
| (58,(_,(MlyValue.SYMBOL SYMBOL,SYMBOL1left,SYMBOL1right))::rest671)
 => let val result=MlyValue.SYMBOL_CSL(([SYMBOL]))
 in (LrTable.NT 24,(result,SYMBOL1left,SYMBOL1right),rest671) end
| (59,(_,(MlyValue.SYMBOL_CSL_TAIL SYMBOL_CSL_TAIL,_,
SYMBOL_CSL_TAIL1right))::(_,(MlyValue.SYMBOL SYMBOL,SYMBOL1left,_))::
rest671) => let val result=MlyValue.SYMBOL_CSL((
SYMBOL :: SYMBOL_CSL_TAIL))
 in (LrTable.NT 24,(result,SYMBOL1left,SYMBOL_CSL_TAIL1right),rest671)
 end
| (60,(_,(MlyValue.SYMBOL_CSL_TAIL SYMBOL_CSL_TAIL,_,
SYMBOL_CSL_TAIL1right))::(_,(MlyValue.SYMBOL SYMBOL,_,_))::(_,(_,
COMMA1left,_))::rest671) => let val result=MlyValue.SYMBOL_CSL_TAIL((
SYMBOL :: SYMBOL_CSL_TAIL))
 in (LrTable.NT 25,(result,COMMA1left,SYMBOL_CSL_TAIL1right),rest671)
 end
| (61,(_,(MlyValue.SYMBOL SYMBOL,_,SYMBOL1right))::(_,(_,COMMA1left,_)
)::rest671) => let val result=MlyValue.SYMBOL_CSL_TAIL(([SYMBOL]))
 in (LrTable.NT 25,(result,COMMA1left,SYMBOL1right),rest671) end
| _ => raise (mlyAction i392)
end
val void = MlyValue.VOID
val extract = fn a => (fn MlyValue.N3DOCUMENT x => x
| _ => let exception ParseInternal
	in raise ParseInternal end) a 
end
end
structure Tokens : N3_TOKENS =
struct
type svalue = ParserData.svalue
type ('a,'b) token = ('a,'b) Token.token
fun EOF (p1,p2) = Token.TOKEN (ParserData.LrTable.T 0,(
ParserData.MlyValue.VOID,p1,p2))
fun PERIOD (p1,p2) = Token.TOKEN (ParserData.LrTable.T 1,(
ParserData.MlyValue.VOID,p1,p2))
fun BARENAME (p1,p2) = Token.TOKEN (ParserData.LrTable.T 2,(
ParserData.MlyValue.VOID,p1,p2))
fun EXPLICIT_URI (i,p1,p2) = Token.TOKEN (ParserData.LrTable.T 3,(
ParserData.MlyValue.EXPLICIT_URI i,p1,p2))
fun PREFIX (p1,p2) = Token.TOKEN (ParserData.LrTable.T 4,(
ParserData.MlyValue.VOID,p1,p2))
fun KEYWORDS (p1,p2) = Token.TOKEN (ParserData.LrTable.T 5,(
ParserData.MlyValue.VOID,p1,p2))
fun LPAREN (p1,p2) = Token.TOKEN (ParserData.LrTable.T 6,(
ParserData.MlyValue.VOID,p1,p2))
fun RPAREN (p1,p2) = Token.TOKEN (ParserData.LrTable.T 7,(
ParserData.MlyValue.VOID,p1,p2))
fun LBRAC (p1,p2) = Token.TOKEN (ParserData.LrTable.T 8,(
ParserData.MlyValue.VOID,p1,p2))
fun RBRAC (p1,p2) = Token.TOKEN (ParserData.LrTable.T 9,(
ParserData.MlyValue.VOID,p1,p2))
fun LCURL (p1,p2) = Token.TOKEN (ParserData.LrTable.T 10,(
ParserData.MlyValue.VOID,p1,p2))
fun RCURL (p1,p2) = Token.TOKEN (ParserData.LrTable.T 11,(
ParserData.MlyValue.VOID,p1,p2))
fun AT_A (p1,p2) = Token.TOKEN (ParserData.LrTable.T 12,(
ParserData.MlyValue.VOID,p1,p2))
fun AT_HAS (p1,p2) = Token.TOKEN (ParserData.LrTable.T 13,(
ParserData.MlyValue.VOID,p1,p2))
fun EQUAL (p1,p2) = Token.TOKEN (ParserData.LrTable.T 14,(
ParserData.MlyValue.VOID,p1,p2))
fun LEFT_ARROW (p1,p2) = Token.TOKEN (ParserData.LrTable.T 15,(
ParserData.MlyValue.VOID,p1,p2))
fun RIGHT_ARROW (p1,p2) = Token.TOKEN (ParserData.LrTable.T 16,(
ParserData.MlyValue.VOID,p1,p2))
fun QNAME (i,p1,p2) = Token.TOKEN (ParserData.LrTable.T 17,(
ParserData.MlyValue.QNAME i,p1,p2))
fun SEMI (p1,p2) = Token.TOKEN (ParserData.LrTable.T 18,(
ParserData.MlyValue.VOID,p1,p2))
fun AT_IS (p1,p2) = Token.TOKEN (ParserData.LrTable.T 19,(
ParserData.MlyValue.VOID,p1,p2))
fun AT_OF (p1,p2) = Token.TOKEN (ParserData.LrTable.T 20,(
ParserData.MlyValue.VOID,p1,p2))
fun COMMA (p1,p2) = Token.TOKEN (ParserData.LrTable.T 21,(
ParserData.MlyValue.VOID,p1,p2))
fun HAT (p1,p2) = Token.TOKEN (ParserData.LrTable.T 22,(
ParserData.MlyValue.VOID,p1,p2))
fun BANG (p1,p2) = Token.TOKEN (ParserData.LrTable.T 23,(
ParserData.MlyValue.VOID,p1,p2))
fun NUMERIC_LITERAL (i,p1,p2) = Token.TOKEN (ParserData.LrTable.T 24,(
ParserData.MlyValue.NUMERIC_LITERAL i,p1,p2))
fun VARIABLE (i,p1,p2) = Token.TOKEN (ParserData.LrTable.T 25,(
ParserData.MlyValue.VARIABLE i,p1,p2))
fun LITERAL (i,p1,p2) = Token.TOKEN (ParserData.LrTable.T 26,(
ParserData.MlyValue.LITERAL i,p1,p2))
fun TYPE_OP (p1,p2) = Token.TOKEN (ParserData.LrTable.T 27,(
ParserData.MlyValue.VOID,p1,p2))
fun LANG_OP (p1,p2) = Token.TOKEN (ParserData.LrTable.T 28,(
ParserData.MlyValue.VOID,p1,p2))
fun LANGCODE (i,p1,p2) = Token.TOKEN (ParserData.LrTable.T 29,(
ParserData.MlyValue.LANGCODE i,p1,p2))
fun AT_FORALL (p1,p2) = Token.TOKEN (ParserData.LrTable.T 30,(
ParserData.MlyValue.VOID,p1,p2))
fun AT_FORSOME (p1,p2) = Token.TOKEN (ParserData.LrTable.T 31,(
ParserData.MlyValue.VOID,p1,p2))
fun AT_KEYWORDS (p1,p2) = Token.TOKEN (ParserData.LrTable.T 32,(
ParserData.MlyValue.VOID,p1,p2))
fun AT_PREFIX (p1,p2) = Token.TOKEN (ParserData.LrTable.T 33,(
ParserData.MlyValue.VOID,p1,p2))
end
end
