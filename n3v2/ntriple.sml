functor NTripleFn (structure L: LITERAL
		   structure U: URI
		   structure NID: NODE_ID) :> NTRIPLE 
	where type U.uri = U.uri and
	      type NID.node_id = NID.node_id and
              type L.literal = L.literal =
struct

  structure L = L
  structure U = U
  structure NID = NID
		  
  structure Subject =
  struct
    
    datatype subject = UriRef of U .uri
		     | Anon of NID.node_id
			       
    fun fromUri uri = UriRef uri
		      
    fun fromNodeId nid = Anon nid
			 
    fun layout subj = case subj of
			  UriRef u => U.layout u
			| Anon nid => NID.layout nid
				      
  end
  
  structure Predicate =
  struct		   
    
    datatype predicate = UriRef of U.uri
				   
    fun fromUri uri = UriRef uri
		      
    fun layout (UriRef uri) = U.layout uri
			      
  end
  
  structure Object = 
  struct
    
    datatype object = UriRef of U.uri
		    | Anon of NID.node_id
		    | Literal of L.literal
				 
    fun fromUri uri = UriRef uri
		      
    fun fromNodeId nid = Anon nid
			 
    fun fromLiteral lit = Literal lit
			  
    fun layout obj = case obj of
			 UriRef u => U.layout u
		       | Anon nid => NID.layout nid
		       | Literal l => L.layout l
				      
  end
  
  structure S = Subject
  structure P = Predicate
  structure O = Object
		
  datatype triple = Triple of {subject: S.subject,
			       predicate: P.predicate,
			       object: O.object}
			      
  fun new subj pred obj = Triple {subject = subj,
				  predicate = pred,
				  object = obj}
				 
  local
      val space = Layout.str " "
      val eos = Layout.str "."
  in
    
    fun layout (Triple {subject,predicate,object}) = 
	Layout.seq [S.layout subject,
		    space,
		    P.layout predicate,
		    space,
		    O.layout object,
		    space,
		    eos]
  end
			  
end


structure NTriple = NTripleFn (structure L = Literal
                               structure U = Uri
			       structure NID = NodeId)
