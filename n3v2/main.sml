structure Main =
struct

  fun announce fname = (print "Parsing ";
			print fname;
			print "\n\n   ---------------  \n")

fun main (_,argv) =
    let val infile = List.hd (argv)
	val ast = N3Parser.n3ParseFile infile
    in
	Layout.outputln (N3Ast.layoutDocument ast, TextIO.stdOut);
	print "-------------------------\n";
        let val trips = Flatten.document ast
	in
	    List.app (fn t => Layout.outputln (Flatten.layout t, TextIO.stdOut)) trips;
	    print "\n"
	end
    end
end


val _ = Main.main(CommandLine.name(), CommandLine.arguments())
