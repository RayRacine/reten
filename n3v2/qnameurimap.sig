signature QNAME_URI_MAP =
sig

type qumap

structure Q: QNAME
structure U: URI

val mapping: Q.qname * U.uri -> ()

val qnameToExplicitUri: Q.qname -> U.uri


end
