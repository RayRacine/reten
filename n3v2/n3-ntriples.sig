signature N3_NTRIPLE =
sig

    structure NT: NTRIPLE
    structure N3A: N3_AST
		   
    type flatnode
	 
    val ntSubject: flatnode -> NT.S.subject
    val symbol: N3A.S.symbol -> flatnode 
    val node: N3A.node -> flatnode * NT.triple list
    val segments: flatnode * N3A.path_segment list * NT.triple list -> flatnode * N3A.path_segment list * NT.triple list
    val path: N3A.path -> flatnode * NT.triple list
    val verb: N3A.verb -> flatnode * NT.triple list
    val property: N3A.property -> (NT.P.predicate * NT.O.object list) * NT.triple list
    val statement: N3A.statement -> NT.triple list				    
    val document: N3A.document -> NT.triple list 
end
