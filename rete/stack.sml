signature STACK =
sig
    type elem
    type stack

    val empty: stack
    val push: elem * stack -> stack
    val pop:  stack -> elem * stack
    val top: stack -> elem
    val isEmpty: stack -> bool
end

structure ListStack =
  struct  
  
  exception EmptyStack

  val empty = []
  fun push(e,s) = e::s
  fun pop [] = raise EmptyStack
    | pop (e::s) = (e,s)
  fun top [] = raise EmptyStack
    | top (e::s) = e
  fun isEmpty [] = true
    | isEmpty _ = false
  end

functor ImpStackFn(structure S: STACK) =
struct

val stack = ref S.empty
	    
fun push e = stack := S.push(e,!stack)
fun pop () = let val (e,s) = S.pop (!stack)
	     in
		 stack := s;
		 e
	     end
end 

(*
 structure IntListStack: STACK =
 struct
 type elem = int
 type stack = int list
 open ListStack
 end
 
 structure IntStack1 = ImpStackFn(structure S = IntListStack)
		       
 structure IntStack2 = ImpStackFn(structure S = 
				  struct
				  type elem = int
				  type stack = int list
				  open ListStack
				  end)
		       
 structure StringStack1 = ImpStackFn(structure S = 
				     struct
				     type elem = string
				     type stack = string list
				     open ListStack
				    end) *)

