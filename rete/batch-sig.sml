signature BATCH =
sig

    type rete

    val load: rete -> unit
    val save: rete -> unit

end
