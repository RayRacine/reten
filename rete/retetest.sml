structure ReteTest =

struct
  
structure C = Condition
structure R = Rete
structure R3 = ReteN3
structure RL = Rule
structure WM = WorkingMemory
structure W = ReteNode.Wme
structure S = Symbol
	      
structure JustOne =
struct
  
  val w1 = W.mkWme (S.sym "ball", S.sym "color", S.sym "red")
  val c1 = C.parseCondition ("?x", "color", "red")
  val p1 = ([c1], RL.Assert[C.parseCondition ("?x","shape","round")])
	   
	     		   
  fun dump rete = let in 
		      print "Rete AM\n";
		      R.writeAlphaMemory (rete,TextIO.stdOut);
		      print "\n";
		      print "Rete BM\n";
		      R.showBetaMemory rete
		  end
		  
  fun test() = let val rete = R.mkReteNetwork NONE
	       in 
		   R.addProduction rete p1;
		   print "Adding wme\n";
		   R.addWME rete w1;
		   dump rete
	       end
	      
end
  
structure JustTwo = 
struct
  
val w1 = W.mkWme (S.sym "ball", S.sym "color", S.sym "red")
val c1 = C.parseCondition ("?x", "color", "red")
val c2 = C.parseCondition ("?x", "shape", "round")
val p1 = ([c1], RL.Assert[C.parseCondition ("?x","shape","round")])
	 
val p2 = ([c1,c2], RL.Assert[C.parseCondition ("?x", "playtoy", "babies")])
	 		
fun test() = let val reten3 as (R3.ReteN3 {wm,rete}) = R3.mkReteN3 NONE
	     in 
		 R3.output (reten3,TextIO.stdOut);
		 print "Add production p1.\n";
		 R3.addProduction reten3 p1;
		 R3.output (reten3,TextIO.stdOut);
		 print "Add production p2.\n";
		 R3.addProduction reten3 p2;
		 print "Productions added to rete.\n";
		 R3.output (reten3,TextIO.stdOut);
		 print "Adding wme\n";
		 R3.addWME reten3 w1;
		 R3.output (reten3,TextIO.stdOut);
		 print "Removing wme\n";
		 ReteNode.removeWme w1;
		 R3.output (reten3,TextIO.stdOut)
	     end	     

end
  
end

structure BigRete =
struct

structure C = Condition
structure R = Rete
structure RL = Rule
structure W = ReteNode.Wme
structure S = Symbol

val sz = SOME 150000

fun addProd _ 0 = ()
  | addProd rete n = let val pred = "in" ^ (Int.toString n)
			 val c1 = C.parseCondition ("?x", pred, "?y")
			 val c2 = C.parseCondition ("?y", pred, "?z")
			 val c3 = RL.Assert [C.parseCondition ("?x", pred, "?z")]
		     in
			 R.addProduction rete ([c1,c2],c3); 
			 addProd rete (n - 1)
		     end

fun addWME _ 0 = ()
  | addWME rete n = let val w = W.mkWme (S.sym "one", S.sym ("in" ^ (Int.toString n)), S.sym "one")
		    in
			R.addWME rete w;
			addWME rete (n-1)
		    end

fun time thunk = let val t0 = Time.now()
		 in
		     thunk();
		     print ("Done: " ^ (Time.toString (Time.- (Time.now(),t0))));
		     print "\n"
		 end

fun test() = let val rete = R.mkReteNetwork sz
		 val t0 = Time.now()
	     in	
		 time (fn () => addProd rete 100000);
		 time (fn () => addWME rete 100000)
	     end
end

val _ = ReteTest.JustTwo.test()

(* val  _ =  BigRete.test(); *)


