functor ReteLrValsFun(structure Token : TOKEN)
 : sig structure ParserData : PARSER_DATA
       structure Tokens : Rete_TOKENS
   end
 = 
struct
structure ParserData=
struct
structure Header = 
struct
structure PT = ParseTypes
structure RT = ReteTypes
structure BR = BaseRete


end
structure LrTable = Token.LrTable
structure Token = Token
local open LrTable in 
val table=let val actionRows =
"\
\\001\000\001\000\000\000\008\000\000\000\000\000\
\\001\000\002\000\023\000\006\000\022\000\000\000\
\\001\000\003\000\017\000\009\000\016\000\000\000\
\\001\000\004\000\010\000\000\000\
\\001\000\004\000\013\000\000\000\
\\001\000\005\000\031\000\000\000\
\\001\000\005\000\032\000\000\000\
\\001\000\006\000\014\000\000\000\
\\001\000\006\000\018\000\000\000\
\\001\000\006\000\026\000\000\000\
\\001\000\006\000\028\000\000\000\
\\001\000\006\000\029\000\000\000\
\\001\000\007\000\009\000\000\000\
\\001\000\010\000\008\000\011\000\007\000\012\000\006\000\000\000\
\\034\000\000\000\
\\035\000\000\000\
\\036\000\000\000\
\\037\000\000\000\
\\038\000\000\000\
\\039\000\004\000\013\000\000\000\
\\040\000\004\000\013\000\000\000\
\\041\000\004\000\013\000\000\000\
\\042\000\000\000\
\\043\000\000\000\
\\044\000\000\000\
\\045\000\000\000\
\\046\000\000\000\
\\047\000\000\000\
\"
val actionRowNumbers =
"\013\000\012\000\015\000\014\000\
\\003\000\004\000\007\000\002\000\
\\008\000\019\000\022\000\001\000\
\\016\000\018\000\004\000\004\000\
\\009\000\023\000\025\000\001\000\
\\026\000\010\000\021\000\020\000\
\\011\000\001\000\027\000\005\000\
\\006\000\017\000\024\000\000\000"
val gotoT =
"\
\\001\000\031\000\002\000\003\000\005\000\002\000\009\000\001\000\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\003\000\010\000\004\000\009\000\000\000\
\\000\000\
\\008\000\013\000\000\000\
\\000\000\
\\003\000\017\000\000\000\
\\000\000\
\\006\000\019\000\007\000\018\000\000\000\
\\000\000\
\\000\000\
\\003\000\010\000\004\000\022\000\000\000\
\\003\000\010\000\004\000\023\000\000\000\
\\000\000\
\\000\000\
\\000\000\
\\006\000\025\000\007\000\018\000\000\000\
\\000\000\
\\000\000\
\\003\000\017\000\000\000\
\\003\000\017\000\000\000\
\\000\000\
\\006\000\028\000\007\000\018\000\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\"
val numstates = 32
val numrules = 14
val s = ref "" and index = ref 0
val string_to_int = fn () => 
let val i = !index
in index := i+2; Char.ord(String.sub(!s,i)) + Char.ord(String.sub(!s,i+1)) * 256
end
val string_to_list = fn s' =>
    let val len = String.size s'
        fun f () =
           if !index < len then string_to_int() :: f()
           else nil
   in index := 0; s := s'; f ()
   end
val string_to_pairlist = fn (conv_key,conv_entry) =>
     let fun f () =
         case string_to_int()
         of 0 => EMPTY
          | n => PAIR(conv_key (n-1),conv_entry (string_to_int()),f())
     in f
     end
val string_to_pairlist_default = fn (conv_key,conv_entry) =>
    let val conv_row = string_to_pairlist(conv_key,conv_entry)
    in fn () =>
       let val default = conv_entry(string_to_int())
           val row = conv_row()
       in (row,default)
       end
   end
val string_to_table = fn (convert_row,s') =>
    let val len = String.size s'
        fun f ()=
           if !index < len then convert_row() :: f()
           else nil
     in (s := s'; index := 0; f ())
     end
local
  val memo = Array.array(numstates+numrules,ERROR)
  val _ =let fun g i=(Array.update(memo,i,REDUCE(i-numstates)); g(i+1))
       fun f i =
            if i=numstates then g i
            else (Array.update(memo,i,SHIFT (STATE i)); f (i+1))
          in f 0 handle Subscript => ()
          end
in
val entry_to_action = fn 0 => ACCEPT | 1 => ERROR | j => Array.sub(memo,(j-2))
end
val gotoT=Array.fromList(string_to_table(string_to_pairlist(NT,STATE),gotoT))
val actionRows=string_to_table(string_to_pairlist_default(T,entry_to_action),actionRows)
val actionRowNumbers = string_to_list actionRowNumbers
val actionT = let val actionRowLookUp=
let val a=Array.fromList(actionRows) in fn i=>Array.sub(a,i) end
in Array.fromList(map actionRowLookUp actionRowNumbers)
end
in LrTable.mkLrTable {actions=actionT,gotos=gotoT,numRules=numrules,
numStates=numstates,initialState=STATE 0}
end
end
local open Header in
type pos = int
type arg = unit
structure MlyValue = 
struct
datatype svalue = VOID | ntVOID of unit | SYMBOL of  (string)
 | PROD_LHS of  (RT.lhs) | PROD_RHS of  (RT.rhs)
 | VARIABLE of  (RT.atom) | ATOM of  (RT.atom)
 | ASSERTION of  (PT.rete_statement)
 | CONDITIONS of  (RT.condition list) | CONDITION of  (RT.condition)
 | PRODUCTION of  (PT.rete_statement)
 | STATEMENT of  (PT.rete_statement)
end
type svalue = MlyValue.svalue
type result = PT.rete_statement
end
structure EC=
struct
open LrTable
val is_keyword =
fn (T 2) => true | (T 6) => true | (T 1) => true | (T 3) => true | (T 
4) => true | (T 8) => true | (T 9) => true | (T 10) => true | (T 11)
 => true | _ => false
val preferred_change = 
nil
val noShift = 
fn (T 0) => true | (T 7) => true | _ => false
val showTerminal =
fn (T 0) => "EOF"
  | (T 1) => "VAR_MARKER"
  | (T 2) => "PRINT"
  | (T 3) => "LPAREN"
  | (T 4) => "RPAREN"
  | (T 5) => "SYMBOL"
  | (T 6) => "PROD_OP"
  | (T 7) => "SEMI"
  | (T 8) => "ASSERT"
  | (T 9) => "USE"
  | (T 10) => "RULE"
  | (T 11) => "FACT"
  | _ => "bogus-term"
local open Header in
val errtermvalue=
fn _ => MlyValue.VOID
end
val terms = (T 0) :: (T 1) :: (T 2) :: (T 3) :: (T 4) :: (T 6) :: (T 7
) :: (T 8) :: (T 9) :: (T 10) :: (T 11) :: nil
end
structure Actions =
struct 
type int = Int.int
exception mlyAction of int
local open Header in
val actions = 
fn (i392:int,defaultPos,stack,
    (()):arg) =>
case (i392,stack)
of (0,(_,(MlyValue.PRODUCTION PRODUCTION,PRODUCTION1left,
PRODUCTION1right))::rest671) => let val result=MlyValue.STATEMENT((
PRODUCTION))
 in (LrTable.NT 0,(result,PRODUCTION1left,PRODUCTION1right),rest671)
 end
| (1,(_,(MlyValue.ASSERTION ASSERTION,ASSERTION1left,ASSERTION1right))
::rest671) => let val result=MlyValue.STATEMENT((ASSERTION))
 in (LrTable.NT 0,(result,ASSERTION1left,ASSERTION1right),rest671) end
| (2,(_,(MlyValue.SYMBOL SYMBOL,_,SYMBOL1right))::(_,(_,USE1left,_))::
rest671) => let val result=MlyValue.STATEMENT((PT.UseStmt SYMBOL))
 in (LrTable.NT 0,(result,USE1left,SYMBOL1right),rest671) end
| (3,(_,(_,_,RPAREN1right))::(_,(MlyValue.SYMBOL SYMBOL3,_,_))::(_,(
MlyValue.SYMBOL SYMBOL2,_,_))::(_,(MlyValue.SYMBOL SYMBOL1,_,_))::_::(
_,(_,FACT1left,_))::rest671) => let val result=MlyValue.ASSERTION((
PT.AssertStmt (Wme.mkWme (SYMBOL1,SYMBOL2,SYMBOL3))))
 in (LrTable.NT 4,(result,FACT1left,RPAREN1right),rest671) end
| (4,(_,(MlyValue.PROD_RHS PROD_RHS,_,PROD_RHS1right))::_::(_,(
MlyValue.PROD_LHS PROD_LHS,PROD_LHS1left,_))::rest671) => let val 
result=MlyValue.PRODUCTION((PT.ProductionStmt (PROD_LHS, PROD_RHS)))
 in (LrTable.NT 1,(result,PROD_LHS1left,PROD_RHS1right),rest671) end
| (5,(_,(MlyValue.CONDITIONS CONDITIONS,_,CONDITIONS1right))::(_,(_,
RULE1left,_))::rest671) => let val result=MlyValue.PROD_LHS((
CONDITIONS))
 in (LrTable.NT 8,(result,RULE1left,CONDITIONS1right),rest671) end
| (6,(_,(MlyValue.CONDITIONS CONDITIONS,_,CONDITIONS1right))::(_,(_,
PRINT1left,_))::rest671) => let val result=MlyValue.PROD_RHS((
RT.Print CONDITIONS))
 in (LrTable.NT 7,(result,PRINT1left,CONDITIONS1right),rest671) end
| (7,(_,(MlyValue.CONDITIONS CONDITIONS,_,CONDITIONS1right))::(_,(_,
ASSERT1left,_))::rest671) => let val result=MlyValue.PROD_RHS((
RT.Assert CONDITIONS))
 in (LrTable.NT 7,(result,ASSERT1left,CONDITIONS1right),rest671) end
| (8,(_,(MlyValue.CONDITION CONDITION,CONDITION1left,CONDITION1right))
::rest671) => let val result=MlyValue.CONDITIONS(([CONDITION]))
 in (LrTable.NT 3,(result,CONDITION1left,CONDITION1right),rest671) end
| (9,(_,(MlyValue.CONDITION CONDITION,_,CONDITION1right))::(_,(
MlyValue.CONDITIONS CONDITIONS,CONDITIONS1left,_))::rest671) => let 
val result=MlyValue.CONDITIONS((CONDITION :: CONDITIONS))
 in (LrTable.NT 3,(result,CONDITIONS1left,CONDITION1right),rest671)
 end
| (10,(_,(_,_,RPAREN1right))::(_,(MlyValue.ATOM ATOM3,_,_))::(_,(
MlyValue.ATOM ATOM2,_,_))::(_,(MlyValue.ATOM ATOM1,_,_))::(_,(_,
LPAREN1left,_))::rest671) => let val result=MlyValue.CONDITION((
(ATOM1,ATOM2,ATOM3)))
 in (LrTable.NT 2,(result,LPAREN1left,RPAREN1right),rest671) end
| (11,(_,(MlyValue.VARIABLE VARIABLE,VARIABLE1left,VARIABLE1right))::
rest671) => let val result=MlyValue.ATOM((VARIABLE))
 in (LrTable.NT 5,(result,VARIABLE1left,VARIABLE1right),rest671) end
| (12,(_,(MlyValue.SYMBOL SYMBOL,SYMBOL1left,SYMBOL1right))::rest671)
 => let val result=MlyValue.ATOM((RT.Constant SYMBOL))
 in (LrTable.NT 5,(result,SYMBOL1left,SYMBOL1right),rest671) end
| (13,(_,(MlyValue.SYMBOL SYMBOL,_,SYMBOL1right))::(_,(_,
VAR_MARKER1left,_))::rest671) => let val result=MlyValue.VARIABLE((
RT.Variable SYMBOL))
 in (LrTable.NT 6,(result,VAR_MARKER1left,SYMBOL1right),rest671) end
| _ => raise (mlyAction i392)
end
val void = MlyValue.VOID
val extract = fn a => (fn MlyValue.STATEMENT x => x
| _ => let exception ParseInternal
	in raise ParseInternal end) a 
end
end
structure Tokens : Rete_TOKENS =
struct
type svalue = ParserData.svalue
type ('a,'b) token = ('a,'b) Token.token
fun EOF (p1,p2) = Token.TOKEN (ParserData.LrTable.T 0,(
ParserData.MlyValue.VOID,p1,p2))
fun VAR_MARKER (p1,p2) = Token.TOKEN (ParserData.LrTable.T 1,(
ParserData.MlyValue.VOID,p1,p2))
fun PRINT (p1,p2) = Token.TOKEN (ParserData.LrTable.T 2,(
ParserData.MlyValue.VOID,p1,p2))
fun LPAREN (p1,p2) = Token.TOKEN (ParserData.LrTable.T 3,(
ParserData.MlyValue.VOID,p1,p2))
fun RPAREN (p1,p2) = Token.TOKEN (ParserData.LrTable.T 4,(
ParserData.MlyValue.VOID,p1,p2))
fun SYMBOL (i,p1,p2) = Token.TOKEN (ParserData.LrTable.T 5,(
ParserData.MlyValue.SYMBOL i,p1,p2))
fun PROD_OP (p1,p2) = Token.TOKEN (ParserData.LrTable.T 6,(
ParserData.MlyValue.VOID,p1,p2))
fun SEMI (p1,p2) = Token.TOKEN (ParserData.LrTable.T 7,(
ParserData.MlyValue.VOID,p1,p2))
fun ASSERT (p1,p2) = Token.TOKEN (ParserData.LrTable.T 8,(
ParserData.MlyValue.VOID,p1,p2))
fun USE (p1,p2) = Token.TOKEN (ParserData.LrTable.T 9,(
ParserData.MlyValue.VOID,p1,p2))
fun RULE (p1,p2) = Token.TOKEN (ParserData.LrTable.T 10,(
ParserData.MlyValue.VOID,p1,p2))
fun FACT (p1,p2) = Token.TOKEN (ParserData.LrTable.T 11,(
ParserData.MlyValue.VOID,p1,p2))
end
end
