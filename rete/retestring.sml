structure ReteString =
struct

structure RT = ReteTypes 
structure B = Binding

fun wmeToString (RT.Wme w) =
    " Subject=" ^ (#subject w) ^
    " Predicate="  ^ (#predicate w) ^
    " Object=" ^ (#object w)
  | wmeToString RT.NilWme = " NilWme"

end
