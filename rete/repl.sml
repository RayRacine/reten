structure Repl =
struct

structure BR = BaseRete 
structure PT = ParseTypes
structure RD = ReteDump
structure R  = Rete

structure ReteLrVals = ReteLrValsFun(structure Token = LrParser.Token)
		     
structure ReteLex = ReteLexFun(structure Tokens = ReteLrVals.Tokens)
		    
structure ReteParser = Join(structure LrParser = LrParser
			    structure ParserData = ReteLrVals.ParserData
			    structure Lex = ReteLex)
		     
val promptLine = "rete> "

val rete = R.mkReteNetwork()
 
fun dumpResult result = 
    case result of 
	PT.ProductionStmt pstmt => let val (lhs,rhs) = pstmt 
				   in 
				       RD.dumpConditions lhs
				   end
      | PT.AssertStmt astmt => RD.dumpWME (astmt,0)
      | PT.UseStmt fileName => print ("Opening " ^ fileName ^"\n")
      | PT.ParseErrorStmt s => print ("Parse error\n")

fun handleResult result =
    case result of
	PT.UseStmt fileName => parseFile fileName
      | PT.ProductionStmt production => R.addProduction rete production
      | PT.AssertStmt wme => R.addWME rete wme
      | PT.ParseErrorStmt _ => ()

and parseFile fileName = 
    let val file = TextIO.openIn (fileName ^ ".rete")
	fun get _ = TextIO.input file
	fun printError (s,i:int,_) = if s = "EOF" then print ("|" ^ s ^ "|\n")
				     else TextIO.output(TextIO.stdOut, "Parse error, line " ^ (Int.toString i) ^ ", " ^ s ^ "\n")
	val dummyEOF = ReteLrVals.Tokens.EOF(0,0)
	val lexer = ReteParser.makeLexer (fn _ => let val line = TextIO.inputLine file
						  in
						      case line of 
							  SOME l => l
							| NONE => ""
						  end) (*LrParser.Stream.streamify (ReteLex.makeLexer get) *)
	fun invokeParser lexer = ReteParser.parse(30,lexer, printError, ())
	fun loop lexer =
	    let val (result,lexer) = invokeParser lexer
		val (nextToken,lexer) = ReteParser.Stream.get lexer
	    in 
		dumpResult result;
		handleResult result;
		if ReteParser.sameToken(nextToken, dummyEOF) then ()
		else loop lexer
	    end
    in
	loop lexer;
	TextIO.closeIn file
    end
	
fun repl() = 
    let fun prompt() = (TextIO.output(TextIO.stdOut, promptLine); TextIO.flushOut TextIO.stdOut)
	val lexer = ReteParser.makeLexer (fn _ => let val line = TextIO.inputLine TextIO.stdIn
						  in
						      case line of 
							  SOME l => l
							| NONE => ""
						  end)
	val dummyEOF = ReteLrVals.Tokens.EOF(0,0)
	val dummySEMI = ReteLrVals.Tokens.SEMI(0,0)
	fun invokeParser lexstream =
	    let fun print_error (s,i:int,_) =
		    TextIO.output(TextIO.stdOut,
				  "Error, line " ^ (Int.toString i) ^ ", " ^ s ^ "\n")
	    in ReteParser.parse(0, lexstream,print_error, ())
	    end
	fun loop lexer =
	    let val _ = prompt()
		val (result,lexer) = invokeParser lexer handle Exception => (PT.ParseErrorStmt "Parse error.", lexer)
		val (nextToken,lexer) = ReteParser.Stream.get lexer
	    in 
		dumpResult result;
		handleResult result;
		if ReteParser.sameToken(nextToken, dummyEOF) then ()
		else loop lexer
	    end
    in 
	loop lexer 
    end

end

(* uncomment for MLTon *)
(* val _ =  Repl.repl() *)

(*	fun extractEOF tok = let val (ReteParser.Token.TOKEN (_,(v,_,_))) = tok in v end;
	fun extractLoc tok = let val (ReteParser.Token.TOKEN (_,(_,x1,y1))) = tok in (x1,y1) end;
	fun extractTerm tok = let val (ReteParser.Token.TOKEN (x ,_)) = tok in x end;
	val dummyEOF = extractTerm (ReteLrVals.Tokens.EOF(0,0)) *)
