structure Load =
struct

structure R = Rete
structure RT = ReteTypes

val u = RT.Variable "u"
val g = RT.Variable "g"
val g1 = RT.Variable "g1"
val g2 = RT.Variable "g2"
val p = RT.Variable "p"

fun symbolGen prefix n =
    RT.Constant (prefix ^ (Int.toString n))

val in_id = symbolGen "in"
val has_id = symbolGen "has"
val sg_id = symbolGen "sg"

fun genProduction n =
    let val p1 = ([(u, in_id n, g), (g, has_id n, p)], 
		  RT.Assert [(u,  has_id n, p)])
	val p2 = ([(g2, has_id n, p), (g1, sg_id n, g2)],
		  RT.Assert [(g1, has_id n,p)])
	val p3 = ([(u, in_id n, g1),(g1, sg_id n, g2)],
		  RT.Assert [(u, in_id n, g2)])
    in
	[p1,p2,p3]
    end
	
val rete = R.mkReteNetwork()

fun load n = if n = 0 then ()
	     else (List.app (fn p => R.addProduction rete p) (genProduction n); load (n-1))

fun timedLoad n = let val t0 = Time.now()
		      fun diff t1 = Time.toMicroseconds(t1) - Time.toMicroseconds(t0)
		  in
		      Stats.clearStats();
		      load n;
		      print ("In " ^ IntInf.toString(diff (Time.now())) ^ ".\n");
		      Stats.showStats()
		  end

end

(* val _ = Load.timedLoad 33333 *)
