functor  BindingFN (S: SYMBOL): BINDING =
struct
  
  structure S = S
  structure L = Layout		

  datatype field = SUBJECT | PREDICATE | OBJECT 
					 
  type variable_binding = {field: field, distance: int}
			  
  datatype rhs_binding = 
	   VariableBinding of variable_binding |
	   ConstantBinding of S.symbol
			      
  type wme_binding = (rhs_binding * rhs_binding * rhs_binding)
		     
  fun mkWmeBinding rhs3 = rhs3
			  
  fun fieldToString field = 
      case field of
	  SUBJECT => "SUBJECT" 
	| PREDICATE => "PREDICATE"
	| OBJECT => "OBJECT"
		    
  fun rhsBindingToString rhs = 
      case rhs of 
	  VariableBinding {field,distance} => "Variable: " ^ (fieldToString field) ^ " Distance: " ^ (Int.toString distance)
	| ConstantBinding cb => "Constant: " ^ (S.toString cb)
				
  fun field {field,distance} = field
					    
  fun distance {distance,field} = distance
				
  fun wmeBindings b = b
		  
  fun layoutField field = L.str (fieldToString field)
			  
  fun layoutWmeBinding (b1,b2,b3) = 
      let val lorhs = L.str o rhsBindingToString
      in
	  L.align [lorhs b1, lorhs b2, lorhs b3]
      end
	  
end
  
structure Binding = BindingFN (Symbol)
		    
