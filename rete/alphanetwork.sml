functor AlphaNetworkFn  (structure C: CONDITION) = 
struct
		
    exception NonExistentAlphaMemory
	      
    structure C = C
    structure RN = ReteNode
		   
    structure AMKey = 
        struct		
	  
	  type hash_key = string * string * string
			  
	  val varSym = "-"			     
	  val sep = "_"
	  fun hashVal (s1,s2,s3) =  (HashString.hashString s1) + (HashString.hashString s2) +
				    (HashString.hashString s3)
	  fun sameKey ((s11,s12,s13),(s21,s22,s23)) = String.compare (s11, s21) = EQUAL andalso
						      String.compare (s12, s22) = EQUAL andalso
						      String.compare (s13, s23) = EQUAL

	  fun toString (s1,s2,s3) = s1 ^ s2 ^ s3
	end
	
    type amkey = AMKey.hash_key
		     
    structure AMMap = HashTableFn(AMKey)
		      
    type alpha_network = RN.alpha_node AMMap.hash_table
			 
    val varSym = AMKey.varSym
		 
    fun lookup (alphanw,amkey) = AMMap.lookup alphanw amkey
				 
    fun find (alphanw,amkey) = AMMap.find alphanw amkey
			       
    fun insert alphanw data = AMMap.insert alphanw data
			      
    fun listAlphaNodes alphanw = AMMap.listItems alphanw
				 
    fun mkAlphaNetwork sz = AMMap.mkTable(sz, NonExistentAlphaMemory)
			    	
    fun conditionToAMKey cond = C.keyValue (AMKey.sep,cond)

    (* Exhaustive hash table technique for the AM for a condition *)
    fun buildShareAlphaMemory alphaNetwork condition = 
	let val key = conditionToAMKey condition 
	in
	    (*	log Log.DEBUG3 ("Build/Share AM using key: " ^  key); *)
	    lookup (alphaNetwork,key) 
	       handle NonExistentAlphaMemory =>
		      let val am = RN.mkAlphaMemory()
		      in 
			  insert alphaNetwork (key, am);
			  (* GOD FIX ME HERE the initialize below is TEMP commented out *) 
			  (* initializeAlphaMemoryWithWorkingMemory am condition; *)
			  am
		      end	
	end 
	    

end

(* FIX ME FUNCTOR DOES NOT WORK *)
structure AlphaNetwork = AlphaNetworkFn(structure C = Condition) 
