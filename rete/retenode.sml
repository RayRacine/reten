HELLO !!  Check to see if I am reusing link values in the Tokens/Items.  Something is causing them to be missing.g

functor ReteNodeFn () : RETE_NODE =
struct

  exception BadNodeUsage of string
  exception UnexpectedException of string
  exception UnKnownProductionException			
  exception NonExistentAlphaMemory
	    
  structure JNT: JOIN_NODE_TEST = JoinNodeTest
  structure C: CONDITION = Condition
  structure B: BINDING = Binding
  structure S: SYMBOL = Symbol
  structure R: RULE = Rule

  structure ST = Stats
  structure AQ = AssertionQ
		  
  type production_action = Rule.production_action
			
  structure WGuid = GuidFn()
  structure TGuid = GuidFn()
  structure RNGuid = GuidFn()

  structure LO = Layout

  datatype wme = NilWme |
	   Wme of {guid: WGuid.guid,
		   subject: S.symbol, 
		   predicate: S.symbol, 
		   object: S.symbol,
		   (* alpha memories containing this wme *)
		   alphaMemItems: item_in_alpha_memory list ref, 
		   (* tokens where the wme is this wme *)
		   tokens: token BaseDList.dlist, 
		   negativeJoinResults: negative_join_result list ref}
		  
  and token =  
      NilToken |
      Token of {guid: TGuid.guid,
		(* points to the higher token, for items 1...i-1 *)
		parent: token,
		(* this is the ith item in the token chain *)
		wme: wme,
		(* points to the memory this token is in *)
		node: rete_node,
		(* the ones with parent=this token *)
		children: token BaseDList.dlist, 
		variant: token_variant option,
		(* A Token may participate in 3 double linked lists, we need snipability *)
		in_memory_prev: token BaseDList.dnode ref,
		in_memory_next: token BaseDList.dnode ref,
		in_wme_prev: token BaseDList.dnode ref,
		in_wme_next: token BaseDList.dnode ref,
		in_parent_prev: token BaseDList.dnode ref,
		in_parent_next: token BaseDList.dnode ref}
	       
  and rete_node = 
      NilReteNode 
    | BetaNode of {guid: RNGuid.guid, 
		   parent: rete_node, 
		   children: rete_node BaseDList.dlist, 
		   in_children_prev: rete_node BaseDList.dnode ref,
		   in_children_next: rete_node BaseDList.dnode ref,
		   items: token BaseDList.dlist,  
		   allChildren: rete_node BaseDList.dlist}  
    | JoinNode of {guid: RNGuid.guid, 
		   parent: rete_node, 
		   children: rete_node BaseDList.dlist,  
		   in_children_prev: rete_node BaseDList.dnode ref,
		   in_children_next: rete_node BaseDList.dnode ref,
		   alphaMemory: alpha_node, 
		   in_alpha_prev: rete_node BaseDList.dnode ref,
		   in_alpha_next: rete_node BaseDList.dnode ref,
		   tests: JNT.join_node_test list ref,
		   nearestAncestor: rete_node} 
    | ProductionNode of {guid: RNGuid.guid, 
			 parent: rete_node, 
			 in_children_prev: rete_node BaseDList.dnode ref,
			 in_children_next: rete_node BaseDList.dnode ref,
			 action: production_action, 
			 bindings : B.wme_binding list}
    | NegativeNode of {guid: RNGuid.guid,
		       parent: rete_node,
		       items: token BaseDList.dlist,  
		       children: rete_node BaseDList.dlist, 
		       in_children_prev: rete_node BaseDList.dnode ref,
		       in_children_next: rete_node BaseDList.dnode ref,
		       amem: alpha_node, 
		       in_alpha_prev: rete_node BaseDList.dnode ref,
		       in_alpha_next: rete_node BaseDList.dnode ref,
		       tests: JNT.join_node_test list, 
		       nearestAncestor: rete_node}
    | NCCNode of {parent: rete_node,
		  children: rete_node BaseDList.dlist, 
		  in_children_prev: rete_node BaseDList.dnode ref,
		  in_children_next: rete_node BaseDList.dnode ref,
		  items: token BaseDList.dlist, 
		  partner: rete_node}
    | NCCPartnerNode of {parent: rete_node,
			 nccnode: rete_node, 
			 children: rete_node BaseDList.dlist, 
			 in_children_prev: rete_node BaseDList.dnode ref,
			 in_children_next: rete_node BaseDList.dnode ref,
			 conjunctions: int, 
			 resultBuffer: token list ref}
			
  and alpha_node = AlphaNode of {guid: RNGuid.guid, 
				 items: item_in_alpha_memory BaseDList.dlist, 
				 successors: rete_node BaseDList.dlist, 
				 refcount: int ref}
				
  and negative_join_result = NegativeJoinResult of {owner: token, 
						    wme: wme}
						   
  and item_in_alpha_memory = ItemInAlphaMemory of {wme: wme, 
						   amem: alpha_node,
						   in_alpha_prev: item_in_alpha_memory BaseDList.dnode ref,
						   in_alpha_next: item_in_alpha_memory BaseDList.dnode ref}
						  
  withtype tokens = token list
  and token_variant = {owner: token ref,
		       joinResults: negative_join_result list ref,
		       nccResults: token BaseDList.dlist,
		       (* may participate in a negation or ncc result dll *)
		       in_result_prev: token BaseDList.dnode ref,
		       in_result_next: token BaseDList.dnode ref}
		      
  structure TokenLists =
  struct
    fun tokenInMemoryLinks token =
	case token of
	    Token {in_memory_prev,in_memory_next,...} => (in_memory_prev,in_memory_next)
	  | NilToken => (print "===========> OOPS"; 
			 raise BadNodeUsage "Linking a NilToken")
			
    fun tokenInParentLinks token = 
	case token of
	    Token {in_parent_prev,in_parent_next,...} => (in_parent_prev,in_parent_next)
	  | NilToken => raise BadNodeUsage "Linking a NilToken"
			      
    fun tokenInWmeLinks token =
	case token of
	    Token {in_wme_prev,in_wme_next,...} => (in_wme_prev,in_wme_next)
	  | NilToken => raise BadNodeUsage "Linking a NilToken"
				 
    fun tokenInResultLinks token =
	case token of
	    Token {variant,...} => (case variant of
					NONE => raise BadNodeUsage "Token has not variant aspect."
				      | SOME {in_result_prev, in_result_next,...} => (in_result_prev,in_result_next))
	  | NilToken => raise BadNodeUsage "Linking a NilToken"
			      
			      
    structure TokenInMemory = DList(structure L =
				    struct
				      type t = token
				      datatype dnode = datatype BaseDList.dnode
			              val links = tokenInMemoryLinks
				    end)

    structure TokenInParent = DList(structure L =
				    struct
				      type t = token
				      datatype dnode = datatype BaseDList.dnode
				      val links = tokenInParentLinks
				  end)		       

    structure TokenInWme = DList(structure L =
				 struct
				   type t = token
				   datatype dnode = datatype BaseDList.dnode
				   val links = tokenInWmeLinks
				 end)

    structure TokenInResult = DList(structure L =
				    struct
				      type t = token
				      datatype dnode = datatype BaseDList.dnode
				      val links = tokenInResultLinks
				    end)
							 
  end
    
  structure ReteNodeLists =
  struct
    
    fun retenodeInAlphaLinks rnode =
	case rnode of
	    JoinNode {in_alpha_prev,in_alpha_next,...} => (in_alpha_prev,in_alpha_next)
	  | NegativeNode {in_alpha_prev,in_alpha_next,...} => (in_alpha_prev,in_alpha_next)
	  | _ => raise BadNodeUsage "Invalid Rete node in alpha list"
		       
		       
    fun retenodeInChildrenLinks rnode =
	case rnode of
	    BetaNode {in_children_prev,in_children_next,...} => (in_children_prev,in_children_next)
	  | JoinNode {in_children_prev,in_children_next,...} => (in_children_prev,in_children_next)
	  | ProductionNode {in_children_prev,in_children_next,...} => (in_children_prev,in_children_next)
	  | NegativeNode {in_children_prev,in_children_next,...} => (in_children_prev,in_children_next)
	  | NCCNode {in_children_prev,in_children_next,...} => (in_children_prev,in_children_next)
	  | NCCPartnerNode {in_children_prev,in_children_next,...} => (in_children_prev,in_children_next)
								      
    fun retenodeInAllChildren rnode =
	case rnode of
	    (*	  BetaNode {in_allchildren_prev,in_allchildren_next,...} => (in_allchildren_prev,in_allchildren_next) *)
	    _ => raise BadNodeUsage "Invalid rete node in AllChildren list."		       
									 
    structure ReteNodeInAlpha = DList( structure L =
				       struct				      
					 type t = rete_node
					 datatype dnode = datatype BaseDList.dnode
					 val links = retenodeInAlphaLinks
				       end )
				
    structure ReteNodeInChildren = DList ( structure L =
					   struct
					     type t = rete_node
					     datatype dnode = datatype BaseDList.dnode
					     val links = retenodeInChildrenLinks
					   end )
				   
    structure ReteNodeInAllChildren = DList ( structure L = 
					      struct
						type t = rete_node
						datatype dnode = datatype BaseDList.dnode
						val links = retenodeInAllChildren
					      end )
  end

  (* Hey I think one of these is redundent.  One structure per link pair!! *)
  structure ItemInAlphaLists = 
  struct
    structure InWme = DList ( structure L = 
			      struct			    
				type t = item_in_alpha_memory
				datatype dnode = datatype BaseDList.dnode
				fun links ( ItemInAlphaMemory { in_alpha_prev, in_alpha_next, ... } ) = ( in_alpha_prev, in_alpha_next )
			      end )
    structure InAlpha = DList ( structure L =
				struct	    
				  type t = item_in_alpha_memory
				  datatype dnode = datatype BaseDList.dnode
			          fun links ( ItemInAlphaMemory { in_alpha_prev, in_alpha_next, ... } ) = ( in_alpha_prev, in_alpha_next )
				end )
  end

    
  structure RNL = ReteNodeLists
  structure TL = TokenLists
  structure IIAL = ItemInAlphaLists
		   
  type 'a dlist = 'a BaseDList.dlist
		  
  type rhs_binding = Binding.rhs_binding
		     
  val log = Log.log (LoggerManager.ctorLogger ("ReteNode", Log.INFO))
	    
  val top = ref NilReteNode

  structure Wme =
  struct    

    structure S = S
    structure B = B
    
    type token = token
    type wme = wme

    fun addTokenToWme (wme,tok) =
	case wme of
	    Wme {tokens,...} =>  TokenLists.TokenInWme.cons (tok,tokens)
	  | NilWme => raise BadNodeUsage "Adding tokento NilWme."
			    
    fun wmeField ( Wme {subject, predicate, object, ... }, fid ) =
	case fid of
	    B.SUBJECT => subject
	  | B.PREDICATE => predicate
	  | B.OBJECT => object 
			
    fun values ( Wme { subject, predicate, object, ... } ) = { subject = subject,
							      predicate = predicate,
							      object = object }
							    
    fun mkWme (subj, pred, obj) = 
	      (Stats.cntWme (Stats.currentStats (), Stats.CREATE);
	       Wme {guid = WGuid.next(),
		    subject=subj, 
		    predicate=pred, 
		    object=obj,
		    alphaMemItems= ref [], 
		    tokens= BaseDList.make(), 
		    negativeJoinResults=ref []})
	
    fun wmeEq (Wme {guid=g1,...}, Wme {guid=g2,...}) = WGuid.eq(g1,g2)

    fun addItemToWME (item, Wme {alphaMemItems, ...}) =
	alphaMemItems := item::(!alphaMemItems)
	
    fun wmeAsStrings ( Wme { subject, predicate, object, ... } ) = ( S.toString subject, 
								    S.toString predicate, 
								    S.toString object )
      | wmeAsStrings NilWme = let val n = "Nil"
			      in
				  ( n, n, n )
			      end
			     
    fun wmeEqv ( Wme { subject = s1, predicate = p1, object = o1, ... }, 
		 Wme { subject = s2, predicate = p2, object = o2, ... } ) =
	S.symEq ( s1, s2 ) andalso S.symEq ( p1, p2 ) andalso S.symEq ( o1, o2 )
						      
    structure HK: HASH_KEY =
    struct
      type hash_key = wme

      fun hashVal w = let val (subj,pred,obj) = wmeAsStrings w
			  val hk = HashString.hashString
		      in
			  hk subj + hk pred + hk obj
		      end

(*      fun wmeKey (Wme wme) = 
	    let val sep = WMKey.sep 
	    in 
		String.concat[#subject wme, sep, #predicate wme, sep, #object wme]
	    end *)
			  
      fun sameKey (k1,k2) = wmeEqv(k1,k2)
    end 
      
    fun layout w = let val (subject, predicate, object) = wmeAsStrings w
		      in
			  LO.seq [LO.str "WME ", LO.record [("Subject",LO.str subject),
							 ("Predicate",LO.str predicate),
							 ("Object",LO.str object)]]
		      end
			  
    fun layoutWMEs wmes = LO.align (List.map layout wmes)

    fun toString wme = let val (s,p,obj) = wmeAsStrings wme
		       in
			   s ^ p ^ obj
		       end

  end (* structure Wme *)

  val assertQ: wme AssertionQ.assertion_queue = AQ.mkAssertionQ ()
						
  fun setTop rnode = top := rnode
  fun getTop () = !top
		  
  structure Token: sig
      val mkToken: rete_node * token * wme -> token
      val addTokenToChildren: token * token -> unit
      val layoutToken: token -> Layout.t
      val layoutItems: token BaseDList.dlist -> Layout.t 
  end = struct

  fun addTokenToChildren (parentToken,tok) = 
      case parentToken of	  
	  Token {children,...} => TL.TokenInParent.cons (tok,children)
	| NilToken => (print "Hmmmm...."; raise BadNodeUsage "Niltoken on addTokenToChildren.")
  
  fun mkTokenVariant (node, parent, wme, variant) =
      let val tok = Token {guid=TGuid.next(),
			   parent=parent,
			   wme=wme,
			   node=node,
			   children= BaseDList.make(),
			   variant= variant,
			   in_memory_prev=ref BaseDList.NilDNode,
			   in_memory_next=ref BaseDList.NilDNode,
			   in_wme_prev=ref BaseDList.NilDNode,
			   in_wme_next=ref BaseDList.NilDNode,
			   in_parent_prev=ref BaseDList.NilDNode,
			   in_parent_next=ref BaseDList.NilDNode}
      in
	  (case parent of 
	       NilToken => ()
	     | Token _ =>  (log Log.DEBUG3 "Parent was not a NilToken.  Adding new token to parent token.";
			    addTokenToChildren (parent,tok)));
	  (case wme of 
	      NilWme => ()
	    | Wme _ => (log Log.DEBUG3 "New tokens WME was not a NilWme.  Adding new token to owning WME.";
			Wme.addTokenToWme(wme,tok)));
	      (* see pg 165.  Check wme <> nil *)
	  tok
      end					   

  fun mkToken (node,parent,wme) = mkTokenVariant (node,parent,wme,NONE)

  fun layoutToken token =
      let fun layout tok =
	      case tok of
		  Token {parent,wme,...} => (Wme.layout wme) :: (layout parent)
		| NilToken => [LO.str "-NilToken-"]
	  val loToks = LO.align(layout token)
      in
	  LO.align [LO.str "Token", LO.indent(loToks,2)]
      end
	  
  fun layoutItems tokens = 
      (
       print "LO Items\n";
       TokenLists.TokenInParent.foreach (tokens, fn t => LO.outputln (layoutToken t, TextIO.stdOut)); LO.str "ray"
(*       LO.align (TokenLists.TokenInParent.foldr(fn (t,accum) => (print "tok"; layoutToken t :: accum),
						[], 
						tokens)) *)
      )
      
  end
  structure T = Token

(*
 structure LayoutReteNodeGuid: sig
     val layoutGuid: rete_node -> Layout.t
 end = struct
 
	 
 end
 structure LORNG = LayoutReteNodeGuid
 *)

  structure ReteNode: sig
      val addChild: rete_node * rete_node -> unit
      val removeChild: rete_node * rete_node -> unit
      val getParent: rete_node -> rete_node				 
      val removeFromParent: rete_node -> unit
      val getGuid: rete_node -> RNGuid.guid option
      val getItems: rete_node -> token BaseDList.dlist
      val getChildren: rete_node -> rete_node BaseDList.dlist
      val getChildrenAsList: rete_node -> rete_node list
      val typeAsString: rete_node -> string
      val unexpectedNode: rete_node -> string -> unit

      val layout: rete_node -> Layout.t
      val layoutGuid: rete_node -> Layout.t
  end = struct

  fun typeAsString rnode =
      case rnode of
	  BetaNode _ => "BetaNode"
	| JoinNode _ => "JoinNode"
	| NCCNode _ => "NCCNode"
	| NegativeNode _ => "NegativeNode"
	| NCCPartnerNode _ => "NCCPartnerNode"
	| ProductionNode _ => "ProductionNode"
	| NilReteNode => "NilReteNode"

  fun unexpectedNode node s = (print (s ^ "::" ^ (typeAsString node) ^ ".\n"); raise BadNodeUsage s)	

  fun getChildren rnode = 
      case rnode of
	  BetaNode {children,...} => children
	| JoinNode {children,...} => children
	| NCCNode {children,...} => children
	| NegativeNode {children,...} => children
	| NCCPartnerNode {children,...} => children
	| ProductionNode _ => raise BadNodeUsage "Production Nodes do not have children."
	| NilReteNode => raise BadNodeUsage "NilRete nodes do not have children."
  
  fun addChild(rnode, kid) = 
      let val kids = getChildren rnode
      in
	  RNL.ReteNodeInChildren.cons (kid,kids)
      end

  fun removeChild (rnode,kid) = 
      let val kids = getChildren rnode
      in
	  RNL.ReteNodeInChildren.snip (kid,kids)
      end
	  
  fun getParent rnode = 
      case rnode of
	  BetaNode bnode => #parent bnode
	| JoinNode jnode => #parent jnode
	| ProductionNode pnode => #parent pnode
	| _ => raise BadNodeUsage "Unhandled node in getNodeParent"

  fun removeFromParent node =
      let val pnode = getParent node
      in
	  removeChild (pnode,node)
      end

  fun getItems rnode =
      case rnode of
	  BetaNode {items,...} => items
	| NegativeNode {items,...} => items
	| NCCNode {items,...} => items
	| node => unexpectedNode node "Accessing items for an invalid node type."

  fun getGuid node = 
      case node of 
	  BetaNode {guid,...} => SOME guid
	| JoinNode {guid,...} => SOME guid
	| ProductionNode {guid,...} => SOME guid
	| _ => NONE

  fun getChildrenAsList rnode = 
      case rnode of
	  ProductionNode _ => []
	| _ => ReteNodeLists.ReteNodeInChildren.toList (getChildren rnode)
	       
  fun layoutGuid node =
      let val sguid = case getGuid node of
			  SOME id => RNGuid.toString id
			| NONE => "--" 
      in
	  LO.str (typeAsString node ^ "::" ^ sguid)
      end
	  
  fun layoutAlphaNodeHeader (AlphaNode {guid,items,successors,refcount}) =
      LO.seq [LO.str "AlphaNode", LO.str "Guid::", RNGuid.layout guid]
      
  fun layoutBetaNode (BetaNode {guid,parent,children,items,...}) =
      LO.align [LO.str "BetaNode",
		LO.indent (LO.align [LO.seq [LO.str "Guid::", LO.str (RNGuid.toString guid)],
				     LO.seq [LO.str "Parent=", layoutGuid parent],
				     LO.seq [LO.str "Items=", LO.str (Int.toString (TokenLists.TokenInParent.size items))],
				     LO.indent(T.layoutItems items,2),
				     LO.str "Children",
				     LO.indent(LO.align (List.map layoutGuid (ReteNodeLists.ReteNodeInChildren.toList children)), 2)],2)]
      
  fun layoutJoinNode (JoinNode {guid,parent,children,alphaMemory,tests,...}) =
      LO.align [LO.str "JoinNode",
		LO.indent (LO.align [LO.seq [LO.str "Guid::", LO.str (RNGuid.toString guid)],
				     LO.seq [LO.str "Parent=", layoutGuid parent],
				     layoutAlphaNodeHeader alphaMemory,
				     JoinNodeTest.layoutJoinNodeTests (!tests),
				     LO.seq[LO.str "Children",
					    LO.indent (LO.align (List.map layoutGuid (ReteNodeLists.ReteNodeInChildren.toList children)),2)]],2)]
      
  fun layoutProductionNode (ProductionNode {guid,parent,action,bindings,...}) =
      LO.align [LO.str "ProductionNode",
		LO.indent (LO.align [LO.seq [LO.str "Guid::", LO.str (RNGuid.toString guid)],
				     LO.seq [LO.str "Parent=", layoutGuid parent],
				     LO.seq [LO.str "Action=", Rule.layoutProductionAction action],
				     LO.align [LO.str "Bindings", LO.indent (LO.align (List.map Binding.layoutWmeBinding bindings),2)]],2)]
      
  fun layout node =
      let val nada = LO.str ("Layout not defined for node: " ^ (typeAsString node))
      in
	  case node of
	      BetaNode _ => layoutBetaNode node
	    | JoinNode _ => layoutJoinNode node
	    | ProductionNode _ => layoutProductionNode node
	    | NilReteNode => LO.str "NilReteNode"
	    | NegativeNode _ => nada
	    | NCCNode _ => nada
	    | NCCPartnerNode _ => nada
      end
      
  end (* structure ReteNode *)
  structure RN = ReteNode
		 
  structure AlphaMemory: sig
      val mkAlphaMemory: unit -> alpha_node
      val equal: alpha_node * alpha_node -> bool
      val addSuccessor: alpha_node -> rete_node -> unit
      val removeTopSuccessor: alpha_node -> unit
      val incRefCount: alpha_node -> unit
      val layout: alpha_node -> Layout.t
  end = struct
  fun mkAlphaMemory() = ( Stats.cntAlpha (Stats.currentStats (), Stats.CREATE);
			  AlphaNode {guid = RNGuid.next(), 
				     items = BaseDList.make(),
				     successors = BaseDList.make(),
				     refcount = ref 0} )
			  
  fun equal (AlphaNode {guid=g1,...}, AlphaNode {guid=g2,...}) = RNGuid.eq(g1,g2)
      
  fun addSuccessor (AlphaNode {successors,...}) succnode = RNL.ReteNodeInAlpha.cons(succnode,successors)
							   
  fun removeTopSuccessor (AlphaNode {successors,...}) = let val _ = RNL.ReteNodeInAlpha.pop successors
							in
							    ()
							end
							    
  fun incRefCount (AlphaNode {refcount,...}) = refcount := !refcount + 1
					       
  fun layout (AlphaNode {guid,items,successors,refcount}) =
      LO.align [LO.str "AlphaNode",
	       LO.indent(LO.align [LO.seq [LO.str "Guid::", 
					RNGuid.layout guid],
				 LO.seq [LO.str "Ref Count: ", 
					LO.str (Int.toString (!refcount))],
				 LO.align [LO.str "Successors",
					  LO.indent (LO.align (List.map RN.layoutGuid (ReteNodeLists.ReteNodeInAlpha.toList successors)),2)],
				 LO.align [LO.str "Items",
					  LO.indent (LO.align (List.map (fn (ItemInAlphaMemory {wme,...}) => Wme.layout wme) 
								      (IIAL.InAlpha.toList items)),2)]],2)]
      
  end 
  structure AM = AlphaMemory

  val mkAlphaMemory = AM.mkAlphaMemory
  
(*   fun setReteNodeChildren rnode kids =
      case rnode of
	  BetaNode node => (#children node) := kids
	| JoinNode node => (#children node) := kids
	| _ => raise BadNodeUsage "Unhandled node in getNodeChildren" *)
		     
	  			      
  fun betaNodeAddToken (BetaNode {items,...}) item = 
      (
       print "Adding item--->\n";       
       TL.TokenInMemory.cons (item,items);
       Token.layoutItems items
      )
						     
  fun conditionMatch (cond, Wme {subject,predicate,object,...}) =
      C.matches(cond,(subject,predicate,object))
      
  fun mkProductionNode(pnode, action, bindings) = 
      (Stats.cntProduction (ST.currentStats(), Stats.CREATE);
       (* log Log.DEBUG2 ("Creating ProductionNode::" ^ (Int.toString (Guid.peek()))); *)
       ProductionNode {guid=RNGuid.next(),
		       parent=pnode, 
		       action = action, 
		       bindings = bindings,
		       in_children_prev = ref BaseDList.NilDNode,
		       in_children_next = ref BaseDList.NilDNode})
      
  fun mkBetaNode pnode = 
      ( Stats.cntBeta (Stats.currentStats (), Stats.CREATE);
	BetaNode {guid = RNGuid.next(),
		  parent = pnode, 
		  children= BaseDList.make(), 
		  in_children_prev = ref BaseDList.NilDNode,
		  in_children_next = ref BaseDList.NilDNode,
		  items = BaseDList.make() ,
		  allChildren = BaseDList.make()})
      
  fun mkJoinNode(pnode, tests, alphaMem) = 
      ( Stats.cntJoin (Stats.currentStats (), Stats.CREATE);
	JoinNode {guid = RNGuid.next(),
		  parent = pnode,
		  children = BaseDList.make(),
		  in_children_prev = ref BaseDList.NilDNode,
		  in_children_next = ref BaseDList.NilDNode,
		  tests = ref tests,
		  alphaMemory = alphaMem,
		  in_alpha_prev = ref BaseDList.NilDNode,
		  in_alpha_next = ref BaseDList.NilDNode,
		  nearestAncestor = NilReteNode}) (* FIXME RPR  ancestor *)
      		      

  fun tokenNthFrom (NilToken,_) = raise UnexpectedException "Test node specified non-existent anscestor."
    | tokenNthFrom (Token tok,0) = Token tok
    | tokenNthFrom (Token tok,nth) = tokenNthFrom (#parent tok,nth - 1)					
				 
  fun mkItemInAlphaMemory (wme,alpha) = 
      ItemInAlphaMemory {wme = wme,
			 amem = alpha,
			 in_alpha_prev = ref BaseDList.NilDNode,
			 in_alpha_next = ref BaseDList.NilDNode}
      

  fun getVariantRecord (Token t) = 
      case #variant t of
	  NONE => raise BadNodeUsage "Token has not variant aspect (2)."
	| SOME v => v
				   
  fun tokenEq (Token {guid=g1,...}, Token {guid=g2,...}) = TGuid.eq(g1,g2)
    | tokenEq (NilToken, NilToken) = true

  fun getTokenJoinResults token = #joinResults (getVariantRecord  token)

(*  fun addTokenToNCC (NCCNode ncc) token = 
      let val {items,...} = ncc
      in
	  items := token :: (!items)
      end *)

  fun addTokenToNCCResults(token,resulttoken) = 
      let val nccresults = #nccResults (getVariantRecord token)
      in
	  TL.TokenInResult.cons(resulttoken, nccresults)
      end 

  fun setTokenVariantOwner token ownertoken =
      let val owner = #owner (getVariantRecord token)
      in
	  owner := ownertoken
      end

  fun getTokenWme (Token {wme,...}) = wme

  fun getPartnerResultBuffer nccnode = 
      let val NCCNode {partner,...} = nccnode
	  val NCCPartnerNode {resultBuffer,...} = partner
      in
	  resultBuffer
      end

  fun addToPartnerResultBuffer partner token =
      let val NCCPartnerNode {resultBuffer,...} = partner
      in
	  resultBuffer := token :: !resultBuffer
      end

  fun clearPartnerResultBuffer nccnode =
      (getPartnerResultBuffer nccnode) := []

  fun getNCCNodeItems nccnode = 
      let val NCCNode {items,...} = nccnode
      in
	  items
      end

  fun addToTokensNCCResult token result =
      let val {nccResults, ...} = getVariantRecord token
      in
	  TL.TokenInResult.cons(result,nccResults)
      end   

  fun mkNegativeJoinResult (owner,wme) = NegativeJoinResult {owner = owner,
							     wme = wme}

  fun negativeJoinResultEq (NegativeJoinResult {owner=t1,wme=w1},
			    NegativeJoinResult {owner=t2,wme=w2}) =
      tokenEq (t1,t2) andalso Wme.wmeEq (w1,w2)

  fun addJoinResultToToken (Token tok) jr =
      case #variant tok of
	  SOME v => let val jrs = #joinResults v
			    in
				jrs := jr :: !jrs
			    end
	| NONE => raise BadNodeUsage "Specified token does not have a variant aspect."			  
			      
  fun addJoinResultToWME (Wme wme) jr =
      let val njrs = #negativeJoinResults wme
      in
	  njrs := jr :: !njrs
      end

  fun removeFromList [] e pred =[]
    | removeFromList (l::ls) e pred= if pred(l,e) 
				 then ls
				 else l :: (removeFromList ls e pred)

  fun memoryNodeItemsChildren node = 
      case node of 
	  BetaNode {items,children,...} => SOME (items,children)
	| NegativeNode {items,children,...} => SOME (items,children)
	| NCCNode {items,children,...} => SOME (items,children)
	| _ => NONE
      


  fun showTop () =
      let val q = Queue.mkQueue()
	  fun qnodes nodes = List.app (fn n => Queue.enqueue (q,n)) nodes
          (* Consider adding node coloring later to dump a node at most once. *)
	  fun bfw () = let val curr = Queue.dequeue q
		       in
			   (* Layout.outputln(Layout.indent(layoutReteNode curr,2), TextIO.stdOut); *)
			   qnodes (RN.getChildrenAsList curr);
			   if Queue.isEmpty q
			   then ()
			   else bfw()
		       end
      in
	  Queue.enqueue (q,getTop());
	  bfw()
      end

  (******************** JOIN Node *********************)
	    
  fun performJoinTests (tests, token, wme) = 
      let fun performTests [] = true
	    | performTests (test::tests) = 
	      let val f1 = Wme.wmeField (wme, JNT.field1 test)
		  val wme = getTokenWme (tokenNthFrom (token, JNT.distance test))
		  val f2 = Wme.wmeField (wme, JNT.field2 test)
	      in 
		  if S.symEq (f1, f2)
		  then performTests tests
		  else false
	      end
      in
	  performTests tests
      end
	  
  fun buildShareJoinNode betanode alphamem ntests =
      (* OK I am already raising an error on a non-BetaNode.
       * Just add a getBetaNodeChildren in ReteNode. *)
      let val children = case betanode of
			     BetaNode {children,...} => children
			   | _ => raise BadNodeUsage "BetaNode children only."
	 (* fun findJoinNode _ = NONE*) (* (log Log.WARN "JN share always fails"; NONE) *)
	  fun jnodeSelector rnode = 
	      case rnode of
		  JoinNode child => ( (* print "Cmp JNs\n";
				       RD.dumpJoinNodeTests ((!(#tests child)),0);
				       RD.dumpJoinNodeTests (ntests,0); *)
		  if !(#tests child) = ntests 
		  then print "EQ\n"
		  else print "NEQ\n";
		  AlphaMemory.equal (#alphaMemory child, alphamem) andalso !(#tests child) = ntests )
		| _ => false 
      in
	  (*	log Log.DEBUG2 "Build share JoinNode. Dumping BetaNode."; *)
	  (*				 Layout.outputln(ReteNode.layoutReteNode parentNode, TextIO.stdOut); *)
	  case ReteNodeLists.ReteNodeInChildren.findFirstSelected (children, jnodeSelector)  of
	      SOME node => (print "JNode shared\n"; node)
	    | NONE => let val jnode = mkJoinNode(betanode, ntests, alphamem)
			  val _ = print "JNode NOT shared.\n"
		      in
			  RN.addChild (betanode, jnode);
			  AM.addSuccessor alphamem jnode;
			  (* Layout.outputln(ReteNode.layoutReteNode parentNode, TextIO.stdOut);
			   * Layout.outputln(ReteNode.layoutReteNode jnode, TextIO.stdOut);*)
			  jnode						 
		      end
      end
	  
	  
  (************ Activation **************)
  (************ Activation **************)
  (************ Activation **************)

  fun matchOneWME tok binding =
      let val (b1,b2,b3) = B.wmeBindings binding
	  fun extract binding = 
	      case binding of 
		  B.ConstantBinding c => c
		| B.VariableBinding vb => 
		  let val Token {wme,...} = tokenNthFrom(tok, B.distance vb)
		  in
		      Wme.wmeField (wme, B.field vb)
		  end
      in
	  Wme.mkWme(extract b1, extract b2, extract b3)
      end
	  
  fun extractRHSMatches _ [] = []
    | extractRHSMatches tok ((b::bindings):B.wme_binding list) = (matchOneWME tok b) :: (extractRHSMatches tok bindings)
								 
  fun productionNodeLeftActivation3 pnode tok wme =
      case pnode of
	ProductionNode {guid,parent,action,bindings,...} =>
	let val tok = T.mkToken(pnode, tok, wme)
	in	
 	    log Log.DEBUG3 "==>Production Activation<=="; 
	    case action of
		Rule.PRINT => let val matches = extractRHSMatches tok bindings
			      in
				  List.app (fn (Wme w) => (print (S.toString (#subject w));
							   print " ";
							   print (S.toString (#predicate w));
							   print  " ";
							   print (S.toString (#object w));
							   print " .\n"))
					   matches;
					   log Log.DEBUG3 "RHS Print"
			      (* print "RHS Print \n";
			       RD.dumpWmes matches 0 *)
			      end
	      | Rule.ASSERT => let val matches = extractRHSMatches tok bindings
			       in
				   log Log.INFO "RHS Assertion in production.";
				   List.app (fn wme => LO.outputln(LO.indent(Wme.layout wme,2), TextIO.stdOut)) matches; 
				   app (fn wme => AQ.enqueue (assertQ, wme)) matches;
				   log Log.DEBUG3 "RHS Assertions added to Q."
			       end
	      | Rule.RETRACT => log Log.WARN "RHS - Retract is not implemented."
	end
      | _ => log Log.ERROR "ProductionNode LA3 invoked with a non production node."
	     
  and productionNodeLeftActivation2 pnode tok = (print "PN LA: "; Layout.output (T.layoutToken tok,TextIO.stdOut))
						
  and leftActivation2Arity (node: rete_node) tok  = 
      case node of 
	  JoinNode jnode => joinNodeLeftActivation node tok
	| ProductionNode pnode => (print "PNLA2\n"; productionNodeLeftActivation2 pnode tok)
	|  _  => print "Print unhandled node in left activation.\n"
	       
  and leftActivation3Arity (node : rete_node) tok awme = 
      (log Log.DEBUG3 ("LA3- with WME " ^ (Wme.toString awme));
       case node of
	   BetaNode bnode => (log Log.DEBUG3 "BetaNode was LA3.";
			      betaLeftActivation node tok awme)
	 | ProductionNode _ => (log Log.DEBUG3 "ProdNode was LA3.";
				productionNodeLeftActivation3 node tok awme;
				log Log.DEBUG3 "ProdNode LA3 invocation complete.")
	 |  _ => log Log.ERROR "Unexpected and unhandled LA3.")
      
  and betaLeftActivation (BetaNode bnode) token wme =
      let val ntoken = T.mkToken(BetaNode bnode, token, wme)
      in
	  print ("BNLA-" ^ (Wme.toString wme) ^ "\n");
	  betaNodeAddToken (BetaNode bnode) ntoken;
	  print ">>>>>>>>>>>>>>>\n";
	  LO.outputln (RN.layout (BetaNode bnode), TextIO.stdOut);
	  print "<<<<<<<<<<<<<<<\n";
	  RNL.ReteNodeInChildren.foreach (#children bnode,fn child => leftActivation2Arity child ntoken)
      end
	  
  and joinNodeLeftActivation (JoinNode jnode) token =
      let fun parentBecameNonEmpty (JoinNode {parent,...}) = 
	      case parent of 
		  BetaNode {items,...} => TL.TokenInParent.hasOnlyOneElement items
		| _ => raise BadNodeUsage "parentBacameNonEmpty"
      in	
	  (* FIX ME UNLINK CRAP IS NOT WORKING *)
	  (*	if parentBecameNonEmpty (JoinNode jnode)
		then (relinkToAlphaMemory (JoinNode jnode);
		      let val AlphaNode an = #alphaMemory jnode
		      in
			  if BaseDList.isEmpty (#items an)
			  then removeReteNodeFromParent (JoinNode jnode)
			  else ()
		      end)
		else ();  *)
	  let val AlphaNode {items,...} = #alphaMemory jnode 
	      fun activateIIA (ItemInAlphaMemory {wme,amem,...}) =
		  if performJoinTests (!(#tests jnode), token, wme) 
		  then RNL.ReteNodeInChildren.foreach 
			   (#children jnode,fn child => leftActivation3Arity child token wme)
		  else ()
	  in	    
	      (* print "JoinNode Left Activation\n";
	       LO.outputln (RN.layout (JoinNode jnode), TextIO.stdOut); *)
	      IIAL.InAlpha.foreach(items,activateIIA) 
	  end
      end 
	  
  and joinNodeRightActivation (jnode, Wme awme, alphaBecameNonEmpty) =
      let fun relinkToBetaMemory jnode = RN.addChild(RN.getParent jnode,jnode)
	  val JoinNode {parent,alphaMemory,tests,children,...} = jnode
      in		
	  log Log.DEBUG3 "JNRA with WME: ";
	  (*	LO.outputln (LO.indent(Wme.layout (Wme awme),2), TextIO.stdOut); *)
	  (*	showTop(); *)
	  (*	if alphaMemoryBecameNonEmpty jnode
		then (relinkToBetaMemory jnode;
		      case parent of
			  BetaNode bnode => if BaseDList.isEmpty(#items bnode) 
					    then let val AlphaNode {successors,...} = alphaMemory
						 in
						     RNL.ReteNodeInAlpha.snip(jnode,successors)
						 end
					    else ())
		else (); *)
	  case parent of
	      BetaNode bnode => let val items = (#items bnode)
				    fun testTokens item = let val tresult = performJoinTests (!tests, item, Wme awme)
							  in 
							      (*							       log Log.DEBUG3("Test result: " ^ (if tresult then "True" else "False")); *)
							      if tresult then
								  RNL.ReteNodeInChildren.foreach (children,fn child => leftActivation3Arity child item (Wme awme))
							      else ()
							  end
				in
				    log Log.DEBUG3 "JNRA-Processing items.";
				    (*				     showTop();
				     RD.dumpTokens (items,1); *)
				    log Log.DEBUG3 "JNRA-Tests.";
				    (*				     RD.dumpJoinNodeTests(!tests,1); *)
				    TL.TokenInMemory.foreach (items, testTokens);
				    log Log.DEBUG3 "Test Tokens completed?"
				end
	    | _ => log Log.WARN "Bad match in joinNodeRightActivation."
      end
	  
  and productionNodeRightActivation pnode wme = log Log.ERROR "Never Happen?? Prod node RA."
  (* NOTE: Instead of passing this extra arg all time
   * The pdf suggests an alternate of keeping a left unlink flag 
   * on the JoinNode. *)
  and rightActivation (node : rete_node, wme, alphaBecameNonEmpty) = 
      case node of
	  JoinNode jnode => (log Log.DEBUG3 "JNRA with JN"; joinNodeRightActivation (node, wme, alphaBecameNonEmpty))
	| ProductionNode pnode => (log Log.DEBUG3 "JNRA with PN"; productionNodeRightActivation node wme)
	| _ => log Log.ERROR "Right activation invoked with an unexpected NODE type."
	       
  and relinkToAlphaMemory(JoinNode jnode) =
      let fun isRightUnlinked _ = (print "FIX ME RUL\n"; false)
	  fun nearestLinkedAncestor (JoinNode jnode) = 
	      let val ancestor = (#nearestAncestor jnode) 
	      in
		  case ancestor of
		      NilReteNode => ancestor
		    | _ => if isRightUnlinked ancestor 
			   then nearestLinkedAncestor ancestor
			   else ancestor
	      end
	  val ancestor = nearestLinkedAncestor (JoinNode jnode)
	  val successors = let val AlphaNode an = #alphaMemory jnode in #successors an end
	  fun spliceIn elem  pre [] = List.revAppend(pre,[elem]) (* deq here? *)
	    | spliceIn elem pre (e::es) = if (print "FIX ME relinkAlpha\n"; true) (* ancestor = e  *)
					  then List.revAppend(pre,elem::e::es)
					  else spliceIn elem (e::pre) es
      in
	  RNL.ReteNodeInAlpha.cons (JoinNode jnode,successors);
	  ()
      end
	  
  (* Add the wme to the node's items list.
   * The items field is a list of an alpha node's wmes.
   * Then right activate all the successor nodes with the just added wme.
   * The successors field is a list of rete nodes for this alpha node. *)	
  and alphaMemoryActivation (amem as AlphaNode anode) awme =
      let val item = mkItemInAlphaMemory (awme,amem)
	  val alphaBecameNonEmpty = let val AlphaNode {items,...} = amem
				    in 
					BaseDList.isEmpty items
				    end
	  fun addItemToAlphaMemory item = IIAL.InAlpha.cons (item,#items anode)
      in
	log Log.DEBUG3 "xxx AlphaMemory activation.";
	addItemToAlphaMemory item;
	(*	showTop(); *)
	log Log.DEBUG2  "xxxx ----> addItemToWME";
	Wme.addItemToWME(item,awme);
	(*	showTop(); *)
	log Log.DEBUG2 "Performing alpha right activiation.";
	RNL.ReteNodeInAlpha.foreach (#successors anode, fn child => rightActivation (child,awme,alphaBecameNonEmpty));
	(*	showTop(); *)
	log Log.DEBUG2 "Completed alpha right activation."
      end
	  
  (********* Beta Network ***********)
	  
  fun mkDummyToken beta = T.mkToken (beta,NilToken,NilWme)
			  
  fun mkTopBetaNode() = let val b as BetaNode {items,...} = mkBetaNode NilReteNode
			in
			  TL.TokenInMemory.cons (mkDummyToken b, items);
			  b
			end
			    
  fun updateNewNodeWithMatchesFromAbove (newnode: rete_node) =
      let val parent = RN.getParent newnode
      in
	  case parent of
	      BetaNode {items,...} => 
	         TL.TokenInParent.foreach(items, fn tok => leftActivation2Arity newnode tok)
	    | JoinNode {children,alphaMemory,...} => 
	          let val tmp = BaseDList.make()
		      val AlphaNode {items,...} = alphaMemory
		      fun activate (ItemInAlphaMemory {wme,...}) = rightActivation (parent,wme,false)
		  in
		      RNL.ReteNodeInChildren.cons(newnode,tmp);
 		      BaseDList.swap(children,tmp); 
		      IIAL.InAlpha.foreach(items,activate);
		      BaseDList.swap(children,tmp) 
		  end
	    | NegativeNode {items,...} => TL.TokenInParent.foreach(items, fn tok => leftActivation2Arity newnode tok)
	    | NCCNode {items,...} => print "Fix me update from above 2." (* TL.TokenInParent.foreach(items, fn tok => leftActivation2Arity newnode tok) *)
      end
	  
  fun buildShareBetaMemoryNode retenode = 
      case retenode of 
	  JoinNode {children,...} => 
	      let fun selectBetaNode rnode = 
		      case rnode of
			  BetaNode _ => true
			| _ => false
	      in
		  case RNL.ReteNodeInChildren.findFirstSelected(children,selectBetaNode) of
		      SOME node => ( Stats.cntJoin (Stats.currentStats (), Stats.SHARE); node )
		    | NONE => let val bnode = mkBetaNode retenode
			      in
				  RN.addChild(retenode, bnode);
				  updateNewNodeWithMatchesFromAbove bnode; (* see pg 38 Rete/UL *)
				  bnode
			      end
	      end
	| _ => raise (BadNodeUsage ("buildShareBetaMemory called with " ^ RN.typeAsString retenode))
		     
  (* I am not doing a tok.wme <> nil check.  But I don't see why I need to
   * Q. When does a token NOT have a WME???? I think it always does.
   * This maybe related to the 2 vs 3 arity activation stuff.
   * Sometimes the 3 arity activation is being called out with a nil value.
   * I have been treating that as a 2 arity activation. hmmmm 
   * I must be missing something.  I expect it will rear up eventually. *)
  fun deleteTokenAndDescendents (tok as Token {children,node,wme,parent,variant,...}) =
      let	fun removeNonPartnerNodes () = case node of
						   NCCPartnerNode _ => ()
						 | ProductionNode _ => () (* I added this.  Check paper.  But a ProductionNode CANNOT have items??  Well yes, thee things it inserted!!*)
						 | _ => TL.TokenInMemory.snip(tok, RN.getItems node)
							
							
		(* pg 158 requires tok.wme <> nil I must be missing something if the token is in a WME list just remove it *)
		fun removeFromWme () = 	let val Wme {tokens,...} = wme 
				in   
					    TL.TokenInWme.snip (tok,tokens)
					end
					    
	fun removeFromParent() = 
	    let val Token {children,...} = parent
	    in
		TL.TokenInParent.snip (tok, children)
	    end
	    
	fun removeFromAlphaMem() = 
	    case memoryNodeItemsChildren node of
		SOME (items,children) => 
		   if BaseDList.isEmpty items
		   then let fun removeJoinNodeChildFromAlpha kid =
				case kid of
				    (JoinNode {alphaMemory,...}) => 
				        let val AlphaNode {successors,...} = alphaMemory
					in
					    RNL.ReteNodeInAlpha.snip(kid,successors)
					end
				  | _ => ()
			in
			    RNL.ReteNodeInChildren.foreach(children,removeJoinNodeChildFromAlpha)
			end
		   else ()					
	      | NONE => ()
			
      in	
	  while not (BaseDList.isEmpty children) do
	      deleteTokenAndDescendents (TL.TokenInParent.first children);
	  
	  removeNonPartnerNodes();
	  removeFromWme();
	  removeFromParent();
	  
	  case node of
	      BetaNode _ => removeFromAlphaMem() (* only if memory node  GO BACK AND FIX THIS AGAIN RPR *)
	    | NegativeNode {items,amem,...} => if BaseDList.isEmpty items
					       then let val AlphaNode {successors,...} = amem
						    in
							RNL.ReteNodeInAlpha.snip(node,successors)
						    end
					       else ()
	    | NCCNode _ => (case variant of
				NONE => ()
			      | SOME {nccResults,...} => let fun remove (Token {parent,wme,...}) = 
								 let val Wme {tokens,...} = wme
								     val Token {children,...} = parent
								 in
								     TL.TokenInWme.snip(tok,tokens);
								     TL.TokenInMemory.snip(tok,children)
								 end
							 in
							     TL.TokenInResult.foreach (nccResults,remove)
							 end)
	    | NCCPartnerNode {nccnode,...}  => (case variant of
						    SOME {owner,...} => 
						    let val Token {variant,...} = !owner							
						    in
							case variant of 
							    SOME {nccResults,...} => (TL.TokenInResult.snip(tok, nccResults);
										      if BaseDList.isEmpty nccResults
										      then let val NCCNode {children,...} = nccnode
											       fun activate kid = leftActivation2Arity kid (!owner)
											   in
											       RNL.ReteNodeInChildren.foreach(children,activate)
											   end
										      else ())
							  | NONE => ()
						    end
						  | NONE => ())
	    | ProductionNode _ => () (* FixME I added this.  Do we want to implement the capability of tracking what a ProductionNode asserted?? *)
	    | node => RN.unexpectedNode node "Unhandled node type in deleteTokensAndDescendents"
		      
      end
	  
  fun deleteDescendentsOfToken (Token {children,...}) = 
      let fun delete kids = if BaseDList.isEmpty kids 
			    then ()
			    else let val kid = TL.TokenInParent.first kids
				 in
				     deleteTokenAndDescendents kid;
				     delete kids
				 end
      in
	  delete children
      end
	  
  fun findNearestAncestorWithSameAmem(NilReteNode, _ ) = (print "Nearest ancestor with same AM not found!!!\n"; NilReteNode)
    | findNearestAncestorWithSameAmem(node, alpha) =
      case node of
	  JoinNode {alphaMemory,...} => if AM.equal (alphaMemory,alpha)
					then node
					else findNearestAncestorWithSameAmem(RN.getParent node, alpha)
	| NegativeNode {amem,...} => if AM.equal (amem,alpha)
				     then node
				     else findNearestAncestorWithSameAmem(RN.getParent node, alpha)
	| NCCNode {partner,...} => let val NCCPartnerNode {parent,...} = partner
				   in
				       findNearestAncestorWithSameAmem(parent,alpha)
				   end
	| _ => findNearestAncestorWithSameAmem(RN.getParent node,alpha)	     

  structure NegativeNode: sig
      
      val mkNegativeNode: rete_node -> alpha_node -> JNT.join_node_test list -> rete_node -> rete_node
									    
  end = struct
  
  fun mkNegativeNode parent amem tests nearest =
      NegativeNode {guid = RNGuid.next(),
		    parent = parent,
		    items = BaseDList.make(),
		    children = BaseDList.make(),
		    in_children_prev = ref BaseDList.NilDNode,
		    in_children_next = ref BaseDList.NilDNode,
		    amem = amem,
		    in_alpha_prev = ref BaseDList.NilDNode,
		    in_alpha_next = ref BaseDList.NilDNode,
		    tests = tests,
		    nearestAncestor = nearest}
      
  fun negativeNodeLeftActivation node token awme =
      let val NegativeNode {items,amem,tests,children,...} = node 
	  val newtoken = T.mkToken(node, token, awme) (* will fail, need to ctor a variant *)
	  val newtokenNegJRs = #joinResults (getVariantRecord newtoken)
	  fun testWme (ItemInAlphaMemory {wme,...}) = 
	      if performJoinTests (tests, token, wme) 
	      then let val jr = mkNegativeJoinResult(newtoken,awme)
		       val wmeNegJRs = let val Wme w = awme in #negativeJoinResults w end
		   in
		       newtokenNegJRs := jr :: !newtokenNegJRs;
 		       wmeNegJRs := jr :: !wmeNegJRs
		   end
	      else ()	   
	  fun testAlphaItems (AlphaNode {items,...}) = 
	      IIAL.InAlpha.foreach(items,testWme)
	  fun checkRelink items = if BaseDList.isEmpty items
				  then relinkToAlphaMemory node
				  else ()
	  fun checkNegationSatisfied () = if List.null (!newtokenNegJRs)
					  then RNL.ReteNodeInChildren.foreach (children,fn kid => leftActivation2Arity kid newtoken)
					  else ()
      in
	  checkRelink items;
	  TL.TokenInMemory.cons(newtoken,items);
	  testAlphaItems amem;
	  checkNegationSatisfied ()
      end
	  
  fun negativeNodeRightActivation node wme =
      let val NegativeNode {items,tests,...} = node 
	  fun testToken tok = if performJoinTests (tests, tok, wme)
			      then (if List.null (! (getTokenJoinResults tok))
				    then deleteDescendentsOfToken tok
				    else ();
				    let val jr = mkNegativeJoinResult(tok,wme)
				    in
					addJoinResultToToken tok jr;
					addJoinResultToWME wme jr
    				    end)
			      else ()
      in
	  TL.TokenInMemory.foreach(items,fn item => testToken item)
      end
	  
  fun testsEq ([],[]) = true
    | testsEq ([],_) = false
    | testsEq (_,[]) = false 
    | testsEq (t1::ts1, t2::ts2) = JNT.equal (t1,t2)
				   
  fun buildShareNegativeNode parent am newtests =
      let fun select node = case node of
				NegativeNode {amem, tests,...} => 
				AM.equal (amem, am) andalso testsEq (tests,newtests)
	  fun findSharableNode kids = RNL.ReteNodeInChildren.findFirstSelected (kids,select)
	  val sharedNode = findSharableNode (RN.getChildren parent)
      in
	  case sharedNode of
	      SOME negnode => negnode
	    | NONE => let val nearest = findNearestAncestorWithSameAmem (parent,am)
			  val negnode = mkNegativeNode parent am newtests nearest
			  val _ = AM.addSuccessor am negnode
			  val _ = AM.incRefCount am
			  val _ = updateNewNodeWithMatchesFromAbove negnode
			  val NegativeNode {items,...} = negnode
			  (* Right-Unlink if node has no tokens. *)
			  val _ = if BaseDList.isEmpty items
				  then AM.removeTopSuccessor am
				  else ()
		      in
			  negnode
		      end
      end	    
  end
  
  structure NN = NegativeNode
		 
  fun nccNodeLeftActivation nccnode token wme =
      let val newtoken = T.mkToken (nccnode, token, wme)
	  val NCCNode {items,...} = nccnode
	  fun doResult result = 
	      (addTokenToNCCResults(newtoken,result);
	       setTokenVariantOwner result newtoken)
      in
	  TL.TokenInMemory.cons(newtoken, items);
	  
	  (* differs from pg 169. I am for-each first then clear result. *)
	  (* paper has for-reach remove result, ... *)
	  List.app doResult (!(getPartnerResultBuffer nccnode));
	  clearPartnerResultBuffer nccnode;
	  
	  let val nccresults = #nccResults (getVariantRecord newtoken)
	      val kids = RN.getChildren nccnode 
	  in
	      if BaseDList.isEmpty nccresults 
	      then RNL.ReteNodeInChildren.foreach(kids,fn kid => leftActivation2Arity kid newtoken)
	      else ()
	  end
      end 
	  
  (* Needs final audit *)
  fun nccPartnerNodeLeftActivation (NCCNode partnernode, t: token, w: wme) =
      let val {partner,items,...} = partnernode
	  val NCCPartnerNode {nccnode, conjunctions,resultBuffer,...} = partner
	  val newresult = T.mkToken (partner,t,w)
	  fun walk (t,w) n =  if n = 0 
			      then (t,w)
			      else let val Token {parent, wme, ...} = t
				   in
				       walk (parent,wme) (n - 1)
				   end
	  val (t,w) = walk (t,w) conjunctions
	  fun addToPartnerBuffer () = resultBuffer := newresult :: (!resultBuffer)
      in
	  case TL.TokenInParent.findFirstSelected(items, fn item => case item of 
									NilToken => false
								      | Token {parent, wme, ...} => (tokenEq(t,parent) andalso Wme.wmeEq(w,wme)))
	   of
	      SOME owner => (addToTokensNCCResult owner newresult;
			     setTokenVariantOwner newresult owner;
			     deleteDescendentsOfToken owner)
	    | NONE => addToPartnerResultBuffer partner newresult
      end
	  
  fun removeWme w  =
      let val Wme {alphaMemItems, tokens, negativeJoinResults,...} = w
	  fun removeItemFromItsAlphaNodeItems (item as ItemInAlphaMemory {amem,...}) =
	      let val AlphaNode {items, successors,...} = amem		
	      in
		  IIAL.InAlpha.snip (item,items);
		  if BaseDList.isEmpty items (* just became empty *)
		  then RNL.ReteNodeInAlpha.foreach (successors, RN.removeFromParent)
		  else ()
	      end
	  fun removeJRFromOwner jr = let val NegativeJoinResult {owner,...}  = jr
					 val Token {variant,...} = owner
				     in
					 case variant of 
					     NONE => raise BadNodeUsage "Expected variant in removeWME."
					   | SOME v => let val {joinResults,...} = v
							   val newjrs = removeFromList (!joinResults) jr negativeJoinResultEq
						       in
							   joinResults := newjrs;
							   if List.null newjrs
							   then let val Token {node,...} = owner
								    val kids = RN.getChildren node
								in
								    RNL.ReteNodeInChildren.foreach(kids,fn kid => leftActivation2Arity kid owner)
								end
							   else ()
						       end
				     end
      in
	  List.app removeItemFromItsAlphaNodeItems (!alphaMemItems);	
	  while not (BaseDList.isEmpty tokens) do 
	      deleteTokenAndDescendents (TL.TokenInWme.first tokens);
	  List.app removeJRFromOwner (!negativeJoinResults)
      end
	  
  fun isDummyTopNode node =
      case node of 
	  NilReteNode => true
	| _ => false
	       
	       
  (* for export *)
  val getChildrenAsList =  RN.getChildrenAsList
  val addChild = RN.addChild
  val layoutAlphaNode = AM.layout		 
  val layout = RN.layout
end

structure ReteNode = ReteNodeFn ()
		     
