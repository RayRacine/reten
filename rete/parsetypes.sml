structure ParseTypes : sig
    
    type production 
    type wme 
	       
    datatype rete_statement = 
	     ProductionStmt of production |
	     AssertStmt of wme |
	     UseStmt of string |
	     ParseErrorStmt of string
			       
end =
struct

type production = Production.production 
type wme = ReteTypes.wme

datatype rete_statement = 
	 ProductionStmt of production | 
	 AssertStmt of wme | 
	 UseStmt of string |
	 ParseErrorStmt of string
						    
end

	      

