signature JOIN_NODE_TEST =
sig
    
    structure B: BINDING
    structure C1: CONDITION where type B.field = B.field

    type join_node_test
	 
    val mkJoinTestsForCondition: C1.condition * C1.condition list -> join_node_test list

    val distance: join_node_test -> int

    val fields: join_node_test -> B.field * B.field
    val field1: join_node_test -> B.field
    val field2: join_node_test -> B.field

    val equal: join_node_test * join_node_test -> bool
								   
    val layoutJoinNodeTest: join_node_test -> Layout.t
    val layoutJoinNodeTests: join_node_test list -> Layout.t

end
