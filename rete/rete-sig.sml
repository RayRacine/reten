signature RETE = 
sig 

    structure W: WME
    structure P: PRODUCTION
    structure Q: ASSERTION_QUEUE

    type rete
	 
    val mkReteNetwork: int option -> rete
    val addWME: rete -> W.wme -> unit 
    val removeWME: rete -> W.wme -> unit
    val addProduction: rete -> P.production -> unit
    val removeProduction: rete -> P.production -> unit
    
    val nextAssertion: rete -> W.wme option

    val showBetaMemory: rete -> unit
    val showAlphaMemory: rete -> unit
				 
end
