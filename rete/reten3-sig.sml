signature RETEN3 = 
sig
    structure W: WME
    structure P: PRODUCTION
    structure WM: WORKING_MEMORY

    type reten3

    val mkReteN3: int option -> reten3
    val addWME: reten3 -> W.wme -> unit 
    val removeWME: reten3 -> W.wme -> unit
    val addProduction: reten3 -> P.production -> unit
    val removeProduction: reten3 -> P.production -> unit

    val workingMemory: reten3 -> W.wme list
end
