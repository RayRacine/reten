functor ConditionFn (structure S1: SYMBOL 
		     structure BD: BINDING where type S.symbol = S1.symbol) 
	: CONDITION =
struct 

  structure S1 = S1
  structure B = BD

  structure L = Layout

  datatype pattern = Variable of S1.symbol 
		   | Constant of S1.symbol

  type condition = pattern * pattern * pattern
		
  val varValue = "-"

  fun subjectPattern (c,_,_) = c
  fun predicatePattern (_,c,_) = c
  fun objectPattern (_,_,c) = c

  fun mkCondition c = c

  fun patternToString pattern = 
      case pattern of
	  (Variable v) => "?" ^ (S1.toString v)
	| (Constant c) => S1.toString c
	
  fun layoutPattern pattern = 
      L.str (patternToString pattern)
		     
  fun toString (c1,c2,c3) = 
      "(" ^ patternToString c1 ^ ", " ^ patternToString c2 ^ ", " ^ patternToString c3 ^ ")"

  fun layoutCondition c =
      L.str (toString c)
      
  fun keyValue (sep, (c1, c2 ,c3)) =
      let fun segment c = case c of 
			      Variable _ => varValue
			    | Constant s => S1.toString s
      in 
	 (* String.concat[segment c1, sep, segment c2, sep, segment c3] *)
	  (segment c1, segment c2, segment c3)
      end	  
      
  fun compare (Variable _, _) = true
    | compare (Constant c, v) = S1.symEq(c,v)

  fun matches((c1,c2,c3),(v1,v2,v3)) =
      compare (c1,v1) andalso 
      compare (c2,v2) andalso
      compare (c3,v3)      		   

(************************************************************************** 
 * Lift a string to an Pattern.
 * If the string starts with a '?' it's a C.Variable otherwise a C.Constant 
 **************************************************************************)	   
  fun liftStringToPattern s = if String.sub (s,0) = #"?"
			      then Variable (S1.fromString s)
			      else Constant (S1.fromString s)
				
  fun parseCondition (subj, pred, obj) = (liftStringToPattern subj,
					  liftStringToPattern pred,
					  liftStringToPattern obj)
					 
  val conditionAccessors = let fun f1 (x: condition) : pattern = #1 x
			       fun f2 (x: condition) : pattern = #2 x
			       fun f3 (x: condition) : pattern = #3 x
			   in
			       [f1, f2, f3]
			   end     				       
			 
fun variableInCondition v (c1, c2, c3) =					  
    let fun varEqual v1 pattern = case pattern of
				      Variable v2 => S1.symEq(v1,v2)
				    | _ => false
    in
	if varEqual v c1 then
	    SOME B.SUBJECT
	else if varEqual v c2 then
	    SOME B.PREDICATE
	else if varEqual v c3 then
	    SOME B.OBJECT
	else NONE
    end
    
fun variableInFirstAvailableCondition (var,conds) =
    let fun findVar _ [] cnt = NONE
	  | findVar (Constant _) _ _ = NONE
	  | findVar (Variable v) (c::conds) cnt =
	    case (variableInCondition v c) of
		SOME cond => SOME (cond, cnt)
	      | NONE => findVar (Variable v) conds (cnt + 1)
    in
	findVar var conds 0
    end
          
end

structure Condition = ConditionFn(structure S1 = Symbol 
				  structure BD = Binding)
