signature BINDING =
sig
    		
    structure S: SYMBOL
		
    type wme_binding 				

    datatype field = SUBJECT | PREDICATE | OBJECT 

    type variable_binding

    datatype rhs_binding = 
	     VariableBinding of variable_binding |
	     ConstantBinding of S.symbol

    val distance: variable_binding -> int
    val field: variable_binding -> field
    val wmeBindings: wme_binding -> rhs_binding * rhs_binding * rhs_binding
    val mkWmeBinding: rhs_binding * rhs_binding * rhs_binding -> wme_binding

    val fieldToString: field -> string
				
    val layoutField: field -> Layout.t
    val layoutWmeBinding: wme_binding -> Layout.t
					 
end
