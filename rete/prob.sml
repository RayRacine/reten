structure ReteTypes =
struct

datatype rete_node = 
	 EmptyNode |
	 BetaNode of {parent: rete_node, children: rete_node list ref} |
	 JoinNode of {parent: rete_node, children: rete_node list ref, alphaMemory: alpha_node} |
	 ProductionNode of {parent: rete_node, alphaNetwork: alpha_network}
withtype alpha_node = {successors: rete_node list ref}
and  alpha_network = {an: {successors: rete_node list ref}}
end
