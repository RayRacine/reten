functor JoinNodeTestFn (structure B: BINDING
			structure C: CONDITION where type B.field = B.field)
	: JOIN_NODE_TEST =
struct

  structure C1 = C
  structure B = B

  structure L = Layout

  datatype join_node_test = JoinNodeTest of {field1: B.field, distance: int, field2: B.field}
					 
  fun mkTest (f1,dist,f2) =  JoinNodeTest {field1=f1, distance=dist, field2 = f2}

  fun distance (JoinNodeTest {distance,...}) = distance

  fun fields (JoinNodeTest {field1,field2,...}) = (field1,field2)

  fun field1 (JoinNodeTest {field1,...}) = field1

  fun field2 (JoinNodeTest {field2,...}) = field2
			    				  
  fun mkJoinTestsForCondition (cond,earlierConditions) =
      let val mkT = SOME o mkTest
	  val select = List.filter Option.isSome
	  val values = List.map Option.valOf
	  val tests = values o select
      in
	  tests [case C1.variableInFirstAvailableCondition (C1.subjectPattern cond, earlierConditions) of
		     SOME (c, cnt) => mkT (C1.B.SUBJECT, cnt, c)
		   | NONE => NONE,
		 case C1.variableInFirstAvailableCondition (C1.predicatePattern cond, earlierConditions) of
		     SOME (c, cnt) => mkT (C1.B.PREDICATE, cnt, c)
		   | NONE => NONE,
		 case C1.variableInFirstAvailableCondition (C1.objectPattern cond, earlierConditions) of
		     SOME (c , cnt) => mkT (C1.B.OBJECT, cnt, c)
		   | NONE => NONE]
      end

  fun equal (JoinNodeTest {field1=f1, distance=d, field2=f2}, 
	     JoinNodeTest {field1=f1',distance=d',field2=f2'}) =
      d = d' andalso f1 = f1' andalso f2 = f2'

  fun layoutJoinNodeTest (JoinNodeTest {field1,distance,field2}) =
      L.seq [L.str "[",
	     L.seq (L.separate ([L.str "Field1=", B.layoutField field1,
				 L.str "Field2=", B.layoutField field2,
				 L.str "Distance=", L.str (Int.toString distance)],", ")),
	     L.str "]"]
      
  fun layoutJoinNodeTests tests = L.align [L.str "JoinNodeTests",
					   L.indent(L.align (List.map layoutJoinNodeTest tests),1)]      
end

 structure JoinNodeTest = JoinNodeTestFn (structure C = Condition structure B = Binding) 
