structure Tokens = Tokens

type pos = int
type svalue = Tokens.svalue
type ('a,'b) token = ('a,'b) Tokens.token
type lexresult = (svalue,pos) token

val pos = ref 0
val eof = fn () => Tokens.EOF (!pos,!pos)
val error = fn (e,l : int,_) => 
               print("line " ^ (Int.toString l) ^ ": " ^ e ^ "\n")

%%
%header (functor ReteLexFun(structure Tokens: Rete_TOKENS));
alpha=[A-Za-z.];
digit=[0-9];
ws =[\ \t];
%%
\n     => (pos := (!pos) + 1; lex());
{ws}+  => (lex());
";"    => (Tokens.SEMI (!pos, !pos));
"use"  => (Tokens.USE (!pos, !pos));
"rule" => (Tokens.RULE (!pos, !pos));
"print" => (Tokens.PRINT(!pos, !pos));
"assert" => (Tokens.ASSERT (!pos, !pos));
"fact" => (Tokens.FACT (!pos, !pos));
{alpha}({alpha}|{digit})* => (Tokens.SYMBOL (yytext,!pos,!pos));
"("  => (Tokens.LPAREN (!pos, !pos));
")"  => (Tokens.RPAREN (!pos, !pos));
"=>" => (Tokens.PROD_OP (!pos, !pos));
"?"  => (Tokens.VAR_MARKER (!pos, !pos));
.    => (error ("rete: ignoring bad character " ^ yytext,!pos,!pos); lex());
