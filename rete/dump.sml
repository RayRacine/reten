structure Dump: sig
    
    val indent: int -> unit
    val nl: unit -> unit
		       
end =
struct

fun nl() = print "\n"
	   
fun indent 0 = ()
  | indent n = (print "."; indent (n-1))	       

end
