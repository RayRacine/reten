signature CONDITION =  
sig  

    structure S1: SYMBOL 		 
    structure B: BINDING where type S.symbol = S1.symbol

    datatype pattern = Variable of S1.symbol 
		     | Constant of S1.symbol

    type condition

    val patternToString: pattern -> string
    val toString: condition -> string
    val layoutCondition: condition -> Layout.t
    val parseCondition: string * string * string -> condition
    val mkCondition: pattern * pattern * pattern -> condition

    val subjectPattern: condition -> pattern
    val predicatePattern: condition -> pattern
    val objectPattern: condition -> pattern

    val keyValue: string * condition -> string * string * string
    val matches: condition * (S1.symbol * S1.symbol * S1.symbol) -> bool
    (* val variableInCondition: string -> condition -> Binding.field option *)

    val variableInFirstAvailableCondition: pattern * condition list -> (B.field * int) option

								    
end
