structure Rule: RULE  =
struct

structure L = Layout
structure C = Condition

type condition = C.condition
type lhs = C.condition list
	  
datatype production_action = PRINT | ASSERT | RETRACT
					      
datatype rhs = 
	 Print of condition list |
	 Assert of condition list
		
type rule = lhs * rhs
 
fun mkLhs cs = cs

fun mk lhs rhs = (lhs,rhs)
  
fun lhs (lhs,_) = lhs

fun rhs (_,rhs) = rhs

fun layoutProductionAction action = 
    case action of
	PRINT => L.str "Print"
      | ASSERT => L.str "Assert"
      | RETRACT => L.str "Retract"

fun layoutRule rule = L.str "FIX ME. Not done."

end
