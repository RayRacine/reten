signature ASSERTION_QUEUE = 
sig
    type 'a assertion_queue
	 
    val mkAssertionQ: unit -> 'a assertion_queue
    val isEmpty: 'a assertion_queue -> bool
    val enqueue: 'a assertion_queue * 'a -> unit
    val dequeue: 'a assertion_queue -> 'a 				    
end
