structure Production =
struct

exception UnBoundRHSProductionVariable

structure AN = AlphaNetwork
structure BN = BetaNetwork

structure B = Binding
structure C = Condition
structure JNT = JoinNodeTest
structure R = Rule
structure RN = ReteNode

type lhs = Rule.lhs

datatype rhs = datatype Rule.rhs

type production = Rule.rule	  

fun mkProduction rule = rule
		  
fun buildAssertionBinding (c1, c2, c3) (conds) = 
    let fun assertionBinding c = 
	    case c of 
		C.Variable _ => (case C.variableInFirstAvailableCondition (c,conds) of 
				     SOME (f,d) => B.VariableBinding {field=f, distance=d}
				   | NONE => raise UnBoundRHSProductionVariable)
	      | C.Constant s => B.ConstantBinding s
    in
	print "Cond: ";
	print (C.toString (c1,c2,c3));
	print "\n";
	print "conds: ";
	app (fn c => (print (C.toString c); print ", ")) conds;
	print "\n";
	B.mkWmeBinding (assertionBinding c1, assertionBinding c2, assertionBinding c3)
    end	
    
fun buildAssertProduction [] conds = []
  | buildAssertProduction (c::cs) conds = (buildAssertionBinding c conds):: (buildAssertProduction cs conds)
					  
fun buildPrintProduction cs conds =  buildAssertProduction cs conds
				     
fun buildRHSProduction rhs conds = 
    case rhs of
	R.Print cs => (R.PRINT, buildPrintProduction cs conds)
      |	R.Assert cs => (R.ASSERT, buildAssertProduction cs conds)
		       
fun addProductionConditions alphanet currNode [] econds = (currNode,econds)
  | addProductionConditions alphanet currentNode (c::conds) earlierConditions  =
    let val currentNode = RN.buildShareBetaMemoryNode currentNode
	val tests = JNT.mkJoinTestsForCondition (c,earlierConditions)
	val am = AN.buildShareAlphaMemory alphanet c
	val currentNode = RN.buildShareJoinNode currentNode am tests
    in
	addProductionConditions alphanet currentNode conds (c::earlierConditions)
    end		
    
fun addProduction (_,_,([],_)) = ()
  | addProduction (amem,bmem,(allconds, rhs)) = 
    let val _ =	print "allconds: ";
	val _ = app (fn c => (print (C.toString c); print ", ")) allconds;
	val _ = print "\n";

	val betaNetwork = BN.betaTopNode bmem
	val an = amem
	val c::conds = allconds
	val currentNode = betaNetwork			  
	val earlierConditions = []
	val am = AN.buildShareAlphaMemory an c
	val tests = JNT.mkJoinTestsForCondition (c, earlierConditions)
	val currentNode = RN.buildShareJoinNode currentNode am tests		  
			  
    in
	let val (currNode,econds) = addProductionConditions an currentNode conds (c::earlierConditions)
	    val (action', bindings') = buildRHSProduction rhs econds
	    val pnode = RN.mkProductionNode(currNode,action',bindings')
	    val _ = RN.addChild (currNode,pnode)
(*	    val _ =  (print "add production--> \n"; Layout.outputln(RN.layoutReteNode pnode, TextIO.stdOut);
			Layout.outputln(RN.layoutReteNode currNode, TextIO.stdOut)) *)
	in	    
	    RN.updateNewNodeWithMatchesFromAbove pnode
	end
    end

end
