structure ReteN3 =
   struct
   
   structure W = ReteNode.Wme
   structure P = Production
   structure WM = WorkingMemory
   structure R = Rete	
       
   datatype reten3 = ReteN3 of {wm: WM.working_memory, rete: R.rete}
	
   val DEF_WM_SZ = 5000
		       
   fun mkReteN3 size = ReteN3 {wm = WM.mkWM size, rete = R.mkReteNetwork size}
		       
   (* FIX ME RPR *)
   (* VERY inefficent and to be rectified later *)
   (* Loop over ALL <arg!!> wmes and attempt to add them !!
    * If the WME is already in the rete or there is no alpha 
    * it will bounce??? DAMN, NO IT WONT. *)
   fun updateFromWorkingMemory (ReteN3 {wm,rete}) = 
       let val wmes = WM.wmWmes wm
       in
	   print "updateFromWorkingMemory is BROKEN.  DO NOT USE THIS.\n";
	   List.app (fn w => R.addWME rete w) wmes
       end
	   
   fun addWME ( r3 as ReteN3 { wm, rete } ) wme = 
       ( if WM.addWme wm wme
	 then R.addWME rete wme
	 else ();
	 processAssertions r3 )
       
   and processAssertions (r3 as ReteN3 {wm,rete}) = 
       Option.app (addWME r3) (R.nextAssertion rete)
   
   fun removeWME _ = print "FIX ME removeWME\n"
		     
   fun addProduction (r3 as ReteN3 {wm,rete}) production =  
       R.addProduction rete production
   (* STUBBED OUT UNTIL THE UPDATE PROBLEM IS RESOLVED.
    * UNTIL THIS IS FIXED ALL PRODUCTIONS MUST ME ADDED 
		     * PRIOR TO ADDING WMEs. *)
   (* updateFromWorkingMemory r3;
    processAssertions r3) *)
       
   fun removeProduction _ = print "FIX ME removeProduction\n"
			    
   fun workingMemory (ReteN3 {wm,...}) = WM.wmWmes wm	
					 
   fun output(ReteN3 {wm,rete}, os) = (print "WM\n";
				       WM.writeWM (wm,os);
				       print "Rete AM\n";
				       R.writeAlphaMemory (rete,os);
				       print "\n";
				       print "Rete BM\n";
				       R.showBetaMemory rete)
   end
     
