functor ReteTypesFn (S: SYMBOL): RETE_TYPES = 
struct 

exception BadNodeUsage of string
exception UnexpectedException of string
exception UnBoundRHSProductionVariable
exception UnKnownProductionException			
exception NonExistentAlphaMemory

structure S = S
structure C = Condition
structure B = Binding
structure JNT = JoinNodeTest

type production_action = Rule.production_action

datatype wme = NilWme |
	 Wme of {subject: S.symbol, 
		 predicate: S.symbol, 
		 object: S.symbol,
		 (* alpha memories containing this wme *)
		 alphaMemItems: item_in_alpha_memory list ref, 
		 (* tokens where the wme is this wme *)
		 tokens: token BaseDList.dlist, 
		 negativeJoinResults: negative_join_result list ref}
		      
and token =  
    NilToken |
    Token of {(* points to the higher token, for items 1...i-1 *)
	      parent: token,
	      (* this is the ith item in the token chain *)
	      wme: wme,
	      (* points to the memory this token is in *)
	      node: rete_node,
	      (* the ones with parent=this token *)
	      children: token BaseDList.dlist, 
	      variant: token_variant option,
	      (* A Token may participate in 3 double linked lists, we need snipability *)
	      in_memory_prev: token BaseDList.dnode ref,
	      in_memory_next: token BaseDList.dnode ref,
	      in_wme_prev: token BaseDList.dnode ref,
	      in_wme_next: token BaseDList.dnode ref,
	      in_parent_prev: token BaseDList.dnode ref,
	      in_parent_next: token BaseDList.dnode ref}
	     
and rete_node = 
    NilReteNode 
  | BetaNode of {guid: int, 
		 parent: rete_node, 
		 children: rete_node BaseDList.dlist, 
		 in_children_prev: rete_node BaseDList.dnode ref,
		 in_children_next: rete_node BaseDList.dnode ref,
		 items: token BaseDList.dlist,  
		 allChildren: rete_node BaseDList.dlist}  
  | JoinNode of {guid: int, 
		 parent: rete_node, 
		 children: rete_node BaseDList.dlist,  
		 in_children_prev: rete_node BaseDList.dnode ref,
		 in_children_next: rete_node BaseDList.dnode ref,
		 alphaMemory: alpha_node, 
		 in_alpha_prev: rete_node BaseDList.dnode ref,
		 in_alpha_next: rete_node BaseDList.dnode ref,
		 tests: JNT.join_node_test list ref,
		 nearestAncestor: rete_node} 
  | ProductionNode of {guid: int, 
		       parent: rete_node, 
		       in_children_prev: rete_node BaseDList.dnode ref,
		       in_children_next: rete_node BaseDList.dnode ref,
		       action: production_action, 
		       bindings : B.wme_binding list}
  | NegativeNode of {items: token BaseDList.dlist,  
		     children: rete_node BaseDList.dlist, 
		     in_children_prev: rete_node BaseDList.dnode ref,
		     in_children_next: rete_node BaseDList.dnode ref,
		     amem: alpha_node, 
		     in_alpha_prev: rete_node BaseDList.dnode ref,
		     in_alpha_next: rete_node BaseDList.dnode ref,
		     tests: JNT.join_node_test list, 
		     nearestAncestor: rete_node}
  | NCCNode of {parent: rete_node,
		children: rete_node BaseDList.dlist, 
		in_children_prev: rete_node BaseDList.dnode ref,
		in_children_next: rete_node BaseDList.dnode ref,
		items: token BaseDList.dlist, 
		partner: rete_node}
  | NCCPartnerNode of {parent: rete_node,
		       nccnode: rete_node, 
		       children: rete_node BaseDList.dlist, 
		       in_children_prev: rete_node BaseDList.dnode ref,
		       in_children_next: rete_node BaseDList.dnode ref,
		       conjunctions: int, 
		       resultBuffer: token list ref}

and alpha_node = AlphaNode of {guid: int, 
			       items: item_in_alpha_memory BaseDList.dlist, 
			       successors: rete_node BaseDList.dlist, 
			       refcount: int ref}

and negative_join_result = NegativeJoinResult of {owner: token, 
						  wme: wme}


and item_in_alpha_memory = ItemInAlphaMemory of {wme: wme, 
						 amem: alpha_node,
						 in_alpha_prev: item_in_alpha_memory BaseDList.dnode ref,
						 in_alpha_next: item_in_alpha_memory BaseDList.dnode ref}

withtype tokens = token list
and token_variant = {owner: token ref,
		     joinResults: negative_join_result list ref,
		     nccResults: token BaseDList.dlist,
		     (* may participate in a negation or ncc result dll *)
		     in_result_prev: token BaseDList.dnode ref,
		     in_result_next: token BaseDList.dnode ref}
		    		   
end

structure ReteTypes = ReteTypesFn (Symbol)
