signature BETA_NETWORK =
sig

    structure RN: RETE_NODE

    type beta_network
	 
    val mkBetaNetwork: unit -> beta_network
    val betaTopNode: beta_network -> RN.rete_node
end
