signature PRODUCTION =
sig

    exception UnBoundRHSProductionVariable
	    
    structure R: RULE
    structure A: ALPHA_NETWORK
    structure B: BETA_NETWORK

    type production

    val mkProduction: Rule.rule -> production
    val addProduction: A.alpha_network * B.beta_network * R.rule -> unit

end
