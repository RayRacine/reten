(* imperative double linked lists *)

signature DLIST_ELEMENT =
sig
    type element 
    val links: element -> element ref *  element ref
end

structure BaseReteNodeList =
struct
  
  exception CorruptedList
  structure RT = ReteTypes
		 
  fun isEmpty (RT.ReteNodeList {first, last}) = let fun isEmpty' (RT.ReteNodeList _) (RT.ReteNodeList _) = true
						      | isEmpty' _ _ = false
						in
						    isEmpty' (!first) (!last)
						end
    | isEmpty _ = false
		  
		  
  fun empty (tl as RT.ReteNodeList {first,last}) = (first := tl;
						      last := tl)
    | empty _ = raise CorruptedList
						     
  fun mkEmpty () = let val toklst = RT.ReteNodeList {first=ref RT.NilReteNode, last=ref RT.NilReteNode}
		   in
		       case toklst of
			   RT.ReteNodeList {first,last} => (first := toklst; last  := toklst)
			 | _ => raise CorruptedList
		   end		       
end
  
structure ReteNodeInChildrenElement =
struct 
  structure RT = ReteTypes
  type element = ReteTypes.rete_node
  fun links rnode =
      case rnode of
	  RT.BetaNode {in_children_prev,in_children_next,...} => (in_children_prev,in_children_next)
	| RT.JoinNode {in_children_prev,in_children_next,...} => (in_children_prev,in_children_next)
	| RT.ProductionNode {in_children_prev,in_children_next,...} => (in_children_prev,in_children_next)
	| RT.NegativeNode {in_children_prev,in_children_next,...} => (in_children_prev,in_children_next)
	| RT.NCCNode {in_children_prev,in_children_next,...} => (in_children_prev,in_children_next)
	| RT.NCCPartnerNode {in_children_prev,in_children_next,...} => (in_children_prev,in_children_next)
	| _ => raise BaseReteNodeList.CorruptedList
end

structure ReteNodeInAllChildrenElement =   
struct
  structure RT = ReteTypes
  type element = ReteTypes.rete_node
  fun links rnode =
      case rnode of
	  RT.BetaNode {in_children_prev,in_children_next,...} => (in_children_prev,in_children_next)
	| _ => raise BaseReteNodeList.CorruptedList
end

structure ReteNodeInAlphaElement =
struct
  structure RT = ReteTypes
  type element = ReteTypes.rete_node
  fun links rnode =
      case rnode of
	  RT.JoinNode {in_alpha_prev,in_alpha_next,...} => (in_alpha_prev,in_alpha_next)
	| RT.NegativeNode {in_alpha_prev,in_alpha_next,...} => (in_alpha_prev,in_alpha_next)
	| _ => raise BaseReteNodeList.CorruptedList		     
end

functor ReteNodeDList (DListElement: DLIST_ELEMENT where type element = ReteTypes.rete_node) = 
struct
  
  structure RT = ReteTypes
  structure DL = DListElement

  fun cons (rnode, rlst as RT.ReteNodeList {first,last}) = 
      let val currfirst = !first
	  val (in_prev,in_next) = DL.links rnode
	  val currfirst_previous = case currfirst of
				       RT.ReteNodeList _ => last
				     | _ => let val (in_prev,_) = DL.links currfirst
					    in
						in_prev
					    end

      in
	  in_prev := rlst;
	  in_next := currfirst;
	  first := rnode;
	  currfirst_previous := rnode;
	  rlst
      end
    | cons _ = raise BaseReteNodeList.CorruptedList

  fun snip rnode = 
      let val (in_prev,in_next) = DL.links rnode
	  val prevrnode = !in_prev
	  val nextrnode = !in_next
	  val prevrnode_next = case prevrnode of
				   RT.ReteNodeList {first,...} => first
				 | _ => let val (_,in_next) = DL.links prevrnode 
					in  
					    in_next 
					end
	  val nextrnode_prev = case nextrnode of 
				   RT.ReteNodeList {last,...} => last
				 | _ => let val (in_prev,_) = DL.links nextrnode
					in
					    in_prev
					end
      in
	  prevrnode_next := prevrnode;
	  nextrnode_prev := nextrnode;
	  in_prev := RT.NilReteNode;
	  in_next := RT.NilReteNode
      end

  fun foreach (RT.ReteNodeList {first,last}) proc = let val rnode = !first
							    in
								case rnode of
								    RT.ReteNodeList _ => ()
								  | RT.NilReteNode => raise BaseReteNodeList.CorruptedList
								  | _ => let val (in_next,_) = DL.links rnode						     
									     val _ = proc rnode
									 in
									     foreach (!in_next) proc
									 end
							    end
    | foreach RT.NilReteNode _ = raise BaseReteNodeList.CorruptedList					   
    | foreach rnode proc = let val (_,in_next) = DL.links rnode
				       val _ = proc rnode
				   in
				       case !in_next of
					   RT.ReteNodeList _ => ()
					 | RT.NilReteNode => raise BaseReteNodeList.CorruptedList
					 | _ => foreach (!in_next) proc
				   end

end

structure ReteNodeLists =
struct
  structure ReteNodeInAlpha = ReteNodeDList (ReteNodeInAlphaElement)
  structure ReteNodeInChildren = ReteNodeDList (ReteNodeInChildrenElement)
  structure ReteNodeInAllChildren = ReteNodeDList (ReteNodeInAllChildrenElement)
end

(*
structure ReteNodeInAlpha =
struct

structure RT = ReteTypes  
			
  fun inAlphaLinks rnode =
      case rnode of
	 RT.JoinNode {in_alpha_prev,in_alpha_next,...} => (in_alpha_prev,in_alpha_next)
	| RT.NegativeNode {in_alpha_prev,in_alpha_next,...} => (in_alpha_prev,in_alpha_next)
	| _ => raise ReteNodeList.CorruptedList

  fun consInAlpha (rnode, rlst as RT.ReteNodeList {first,last}) = 
      let val currfirst = !first
	  val (in_alpha_prev,in_alpha_next) = inAlphaLinks rnode
	  val currfirst_previous = case currfirst of
				       RT.ReteNodeList _ => last
				     | _ => let val (in_alpha_prev,_) = inAlphaLinks currfirst
					    in
						in_alpha_prev
					    end

      in
	  in_alpha_prev := rlst;
	  in_alpha_next := currfirst;
	  first := rnode;
	  currfirst_previous := rnode;
	  rlst
      end
    | consInAlpha  _ = raise ReteNodeList.CorruptedList

  fun snipInAlpha rnode = 
      let val (in_alpha_prev,in_alpha_next) = inAlphaLinks rnode
	  val prevrnode = !in_alpha_prev
	  val nextrnode = !in_alpha_next
	  val prevrnode_next = case prevrnode of
				   RT.ReteNodeList {first,...} => first
				 | _ => let val (_,in_alpha_next) = inAlphaLinks prevrnode 
					in  
					    in_alpha_next 
					end
	  val nextrnode_prev = case nextrnode of 
				   RT.ReteNodeList {last,...} => last
				 | _ => let val (in_alpha_prev,_) = inAlphaLinks nextrnode
					in
					    in_alpha_prev
					end
      in
	  prevrnode_next := prevrnode;
	  nextrnode_prev := nextrnode;
	  in_alpha_prev := RT.NilReteNode;
	  in_alpha_next := RT.NilReteNode
      end

  fun foreachInAlpha (RT.ReteNodeList {first,last}) proc = let val rnode = !first
							    in
								case rnode of
								    RT.ReteNodeList _ => ()
								  | RT.NilReteNode => raise ReteNodeList.CorruptedList
								  | _ => let val (in_alpha_next,_) = inAlphaLinks rnode
									     val _ = proc rnode
									 in
									     foreachInAlpha (!in_alpha_next) proc
									 end
							    end
    | foreachInAlpha RT.NilReteNode _ = raise ReteNodeList.CorruptedList	
    | foreachInAlpha rnode proc = let val (_,in_alpha_next) = inAlphaLinks rnode
				       val _ = proc rnode
				  in
				      case !in_alpha_next of
					  RT.ReteNodeList _ => ()
					| RT.NilReteNode => raise ReteNodeList.CorruptedList
					| _ => foreachInAlpha (!in_alpha_next) proc
				  end



					   
end

structure ItemInAlphaList =
struct
  
  structure RT = ReteTypes

  fun isEmpty (RT.ItemInAlphaList {first, last}) = let fun isEmpty' (RT.ItemInAlphaList _) (RT.ItemInAlphaList _) = true
						   | isEmpty' _ _ = false
					     in
						 isEmpty' (!first) (!last)
					     end
    | isEmpty _ = false


  fun empty() (tl as RT.ItemInAlphaList {first,last}) = (first := tl;
						   last := tl)

  fun mkEmpty () = let val toklst as RT.ItemInAlphaList {first,last} = RT.ItemInAlphaList {first=ref RT.NilItem, last=ref RT.NilItem}
		   in
		       first := toklst;
		       last  := toklst;
		       toklst
		   end

  fun cons_in_wme (tok as RT.ItemInAlphaMemory {in_wme_prev,in_wme_next,...}) (toklst as RT.ItemInAlphaList {first,last}) = 
      let val currfirst = !first
	  val currfirst_previous = case currfirst of
				       RT.ItemInAlphaList _ => last
				     | RT.ItemInAlphaMemory {in_wme_prev,...} => in_wme_prev
				     | _ => raise ReteNodeList.CorruptedList
      in
	  in_wme_prev := toklst;
	  in_wme_next := currfirst;
	  first := tok;
	  currfirst_previous := tok;
	  toklst
      end
    | cons_in_wme _ _ = raise ReteNodeList.CorruptedList

  fun cons_in_alpha (tok as RT.ItemInAlphaMemory {in_alpha_prev,in_alpha_next,...}) (toklst as RT.ItemInAlphaList {first,last}) = 
      let val currfirst = !first
	  val currfirst_previous = case currfirst of
				       RT.ItemInAlphaList _ => last
				     | RT.ItemInAlphaMemory {in_alpha_prev,...} => in_alpha_prev
				     | _ => raise ReteNodeList.CorruptedList
      in
	  in_alpha_prev := toklst;
	  in_alpha_next := currfirst;
	  first := tok;
	  currfirst_previous := tok;
	  toklst
      end
    | cons_in_alpha _ _ = raise ReteNodeList.CorruptedList

  fun snip_in_wme (toklst as RT.ItemInAlphaList {first,last}) (token as RT.ItemInAlphaMemory {in_wme_prev, in_wme_next,...}) = 
      let val prevtoken = !in_wme_prev
	  val nexttoken = !in_wme_next
	  val prevtoken_next = case prevtoken of
				   RT.ItemInAlphaList _ => first
				 | RT.ItemInAlphaMemory {in_wme_next,...} => in_wme_next
				 | RT.NilItem => raise ReteNodeList.CorruptedList
	  val nexttoken_prev = case nexttoken of 
				   RT.ItemInAlphaList _ => last
				 | RT.ItemInAlphaMemory {in_wme_prev,...} => in_wme_prev
				 | RT.NilItem => raise ReteNodeList.CorruptedList
      in
	  prevtoken_next := prevtoken;
	  nexttoken_prev := nexttoken;
	  in_wme_prev := RT.NilItem;
	  in_wme_next := RT.NilItem
      end
    | snip_in_wme _ _ = raise ReteNodeList.CorruptedList

  fun foreach_in_wme (RT.ItemInAlphaList {first,last}) proc = (case !first of
								RT.ItemInAlphaMemory {in_wme_next,...} => 
								let val _ = proc (!first)
								in
								    foreach_in_wme (!in_wme_next) proc
								end
							      | RT.ItemInAlphaList _ => ()
							      | RT.NilItem => raise ReteNodeList.CorruptedList )
    | foreach_in_wme (tok as RT.ItemInAlphaMemory {in_wme_next,...}) proc = let val _ = proc tok
								      in
									  case !in_wme_next of
									      RT.ItemInAlphaList _ => ()
									    | RT.ItemInAlphaMemory _  => foreach_in_wme (!in_wme_next) proc
									    | RT.NilItem => raise ReteNodeList.CorruptedList
								      end
    | foreach_in_wme RT.NilItem _ = raise ReteNodeList.CorruptedList

  fun snip_in_alpha (toklst as RT.ItemInAlphaList {first,last}) (token as RT.ItemInAlphaMemory {in_alpha_prev, in_alpha_next,...}) = 
      let val prevtoken = !in_alpha_prev
	  val nexttoken = !in_alpha_next
	  val prevtoken_next = case prevtoken of
				   RT.ItemInAlphaList _ => first
				 | RT.ItemInAlphaMemory {in_alpha_next,...} => in_alpha_next
				 | RT.NilItem => raise ReteNodeList.CorruptedList
	  val nexttoken_prev = case nexttoken of 
				   RT.ItemInAlphaList _ => last
				 | RT.ItemInAlphaMemory {in_alpha_prev,...} => in_alpha_prev
				 | RT.NilItem => raise ReteNodeList.CorruptedList
      in
	  prevtoken_next := prevtoken;
	  nexttoken_prev := nexttoken;
	  in_alpha_prev := RT.NilItem;
	  in_alpha_next := RT.NilItem
      end
    | snip_in_alpha _ _ = raise ReteNodeList.CorruptedList

  fun foreachItemInAlpha (RT.ItemInAlphaList {first,last}) proc = (case !first of
								       RT.ItemInAlphaMemory {in_alpha_next,...} => 
								       let val _ = proc (!first)
								       in
									   foreachItemInAlpha (!in_alpha_next) proc
								       end
								     | RT.ItemInAlphaList _ => ()
								     | RT.NilItem => raise ReteNodeList.CorruptedList )
    | foreachItemInAlpha (tok as RT.ItemInAlphaMemory {in_alpha_next,...}) proc = let val _ = proc tok
										in
										    case !in_alpha_next of
											RT.ItemInAlphaList _ => ()
										      | RT.ItemInAlphaMemory _  => foreachItemInAlpha (!in_alpha_next) proc
										      | RT.NilItem => raise ReteNodeList.CorruptedList
										end
    | foreachItemInAlpha RT.NilItem _ = raise ReteNodeList.CorruptedList


end

structure TokenList =
struct
  
  structure RT = ReteTypes
		 
  fun isEmpty (RT.TokenList {first, last}) = let fun isEmpty' (RT.TokenList _) (RT.TokenList _) = true
						   | isEmpty' _ _ = false
					     in
						 isEmpty' (!first) (!last)
					     end
    | isEmpty _ = false


  fun empty() (tl as RT.TokenList {first,last}) = (first := tl;
						   last := tl)

  fun mkEmpty () = let val toklst as RT.TokenList {first,last} = RT.TokenList {first=ref RT.NilToken, last=ref RT.NilToken}
		   in
		       first := toklst;
		       last  := toklst;
		       toklst
		   end

  fun cons_in_memory (tok as RT.Token {in_memory_prev,in_memory_next,...}) (toklst as RT.TokenList {first,last}) = 
      let val currfirst = !first
	  val currfirst_previous = case currfirst of
				       RT.TokenList _ => last
				     | RT.Token {in_memory_prev,...} => in_memory_prev
				     | _ => raise ReteNodeList.CorruptedList
      in
	  in_memory_prev := toklst;
	  in_memory_next := currfirst;
	  first := tok;
	  currfirst_previous := tok;
	  toklst
      end
    | cons_in_memory _ _ = raise ReteNodeList.CorruptedList

  fun cons_in_wme (tok as RT.Token {in_wme_prev,in_wme_next,...}) (toklst as RT.TokenList {first,last}) = 
      let val currfirst = !first
	  val currfirst_previous = case currfirst of
				       RT.TokenList _ => last
				     | RT.Token {in_wme_prev,...} => in_wme_prev
				     | _ => raise ReteNodeList.CorruptedList
      in
	  in_wme_prev := toklst;
	  in_wme_next := currfirst;
	  first := tok;
	  currfirst_previous := tok;
	  toklst
      end
    | cons_in_wme _ _ = raise ReteNodeList.CorruptedList

  fun cons_in_parent (tok as RT.Token {in_parent_prev,in_parent_next,...}) (toklst as RT.TokenList {first,last}) = 
      let val currfirst = !first
	  val currfirst_previous = case currfirst of
				       RT.TokenList _ => last
				     | RT.Token {in_parent_prev,...} => in_parent_prev
				     | _ => raise ReteNodeList.CorruptedList
      in
	  in_parent_prev := toklst;
	  in_parent_next := currfirst;
	  first := tok;
	  currfirst_previous := tok;
	  toklst
      end
    | cons_in_parent _ _ = raise ReteNodeList.CorruptedList

  fun snip_in_memory (toklst as RT.TokenList {first,last}) (token as RT.Token {in_memory_prev, in_memory_next,...}) = 
      let val prevtoken = !in_memory_prev
	  val nexttoken = !in_memory_next
	  val prevtoken_next = case prevtoken of
				   RT.TokenList _ => first
				 | RT.Token {in_memory_next,...} => in_memory_next
				 | RT.NilToken => raise ReteNodeList.CorruptedList
	  val nexttoken_prev = case nexttoken of 
				   RT.TokenList _ => last
				 | RT.Token {in_memory_prev,...} => in_memory_prev
				 | RT.NilToken => raise ReteNodeList.CorruptedList
      in
	  prevtoken_next := prevtoken;
	  nexttoken_prev := nexttoken;
	  in_memory_prev := RT.NilToken;
	  in_memory_next := RT.NilToken
      end
    | snip_in_memory _ _ = raise ReteNodeList.CorruptedList

  fun snip_in_wme (toklst as RT.TokenList {first,last}) (token as RT.Token {in_wme_prev, in_wme_next,...}) = 
      let val prevtoken = !in_wme_prev
	  val nexttoken = !in_wme_next
	  val prevtoken_next = case prevtoken of
				   RT.TokenList _ => first
				 | RT.Token {in_wme_next,...} => in_wme_next
				 | RT.NilToken => raise ReteNodeList.CorruptedList
	  val nexttoken_prev = case nexttoken of 
				   RT.TokenList _ => last
				 | RT.Token {in_wme_prev,...} => in_wme_prev
				 | RT.NilToken => raise ReteNodeList.CorruptedList
      in
	  prevtoken_next := prevtoken;
	  nexttoken_prev := nexttoken;
	  in_wme_prev := RT.NilToken;
	  in_wme_next := RT.NilToken
      end
    | snip_in_wme _ _ = raise ReteNodeList.CorruptedList


  fun snip_in_parent (toklst as RT.TokenList {first,last}) (token as RT.Token {in_parent_prev, in_parent_next,...}) = 
      let val prevtoken = !in_parent_prev
	  val nexttoken = !in_parent_next
	  val prevtoken_next = case prevtoken of
				   RT.TokenList _ => first
				 | RT.Token {in_parent_next,...} => in_parent_next
				 | RT.NilToken => raise ReteNodeList.CorruptedList
	  val nexttoken_prev = case nexttoken of 
				   RT.TokenList _ => last
				 | RT.Token {in_parent_prev,...} => in_parent_prev
				 | RT.NilToken => raise ReteNodeList.CorruptedList
      in
	  prevtoken_next := prevtoken;
	  nexttoken_prev := nexttoken;
	  in_parent_prev := RT.NilToken;
	  in_parent_next := RT.NilToken
      end
    | snip_in_parent _ _ = raise ReteNodeList.CorruptedList


  fun foreach_in_memory (RT.TokenList {first,last}) proc = (case !first of
								RT.Token {in_memory_next,...} => 
								let val _ = proc (!first)
								in
								    foreach_in_memory (!in_memory_next) proc
								end
							      | RT.TokenList _ => ()
							      | RT.NilToken => raise ReteNodeList.CorruptedList )
    | foreach_in_memory (tok as RT.Token {in_memory_next,...}) proc = let val _ = proc tok
								      in
									  case !in_memory_next of
									      RT.TokenList _ => ()
									    | RT.Token _  => foreach_in_memory (!in_memory_next) proc
									    | RT.NilToken => raise ReteNodeList.CorruptedList
								      end
    | foreach_in_memory RT.NilToken _ = raise ReteNodeList.CorruptedList


  fun foreach_in_wme (RT.TokenList {first,last}) proc = (case !first of
							     RT.Token {in_wme_next,...} => 
							     let val _ = proc (!first)
							     in
								 foreach_in_wme (!in_wme_next) proc
							     end
							   | RT.TokenList _ => ()
							   | RT.NilToken => raise ReteNodeList.CorruptedList )
    | foreach_in_wme (tok as RT.Token {in_wme_next,...}) proc = let val _ = proc tok
								in
								    case !in_wme_next of
									RT.TokenList _ => ()
								      | RT.Token _  => foreach_in_wme (!in_wme_next) proc
								      | RT.NilToken => raise ReteNodeList.CorruptedList
								end
    | foreach_in_wme RT.NilToken _ = raise ReteNodeList.CorruptedList
					   
					   
  fun foreachInParent (RT.TokenList {first,last}) proc = (case !first of
								RT.Token {in_parent_next,...} => 
								let val _ = proc (!first)
								in
								    foreachInParent (!in_parent_next) proc
								end
							      | RT.TokenList _ => ()
							      | RT.NilToken => raise ReteNodeList.CorruptedList )
    | foreachInParent (tok as RT.Token {in_parent_next,...}) proc = let val _ = proc tok
								      in
									  case !in_parent_next of
									      RT.TokenList _ => ()
									    | RT.Token _  => foreachInParent (!in_parent_next) proc
									    | RT.NilToken => raise ReteNodeList.CorruptedList
								      end
    | foreachInParent RT.NilToken _ = raise ReteNodeList.CorruptedList
					      
end
*)
(*
structure RList (*: RLIST *)  =
struct

  exception IndexException
  exception CorruptedRlist

  datatype 'a rlist = Nil 
		    | FLNode of {first: 'a rlist ref, last: 'a rlist ref} 
		    | Node of {previous: 'a rlist ref, next: 'a rlist ref}

  type 'a rnode = 'a rlist

  fun mkEmpty () = let val n as FLNode {first,last} = FLNode {first = ref Nil, last = ref Nil}
		   in
		       first := n;
		       last  := n;
		       n
		   end
		   
  fun mkNode () = Node {previous = ref Nil,next = ref Nil}

  fun isEmpty (fl as FLNode {first, last}) = let fun isEmpty' (FLNode _) (FLNode _) = true
						   | isEmpty' _ _ = false
					     in
						 isEmpty' (!first) (!last)
					     end
    | isEmpty _ = false
						 
  fun cons e (dl as FLNode {first,last}) = let val currfirst = !first
					       val node = Node {previous = ref dl, next = ref currfirst, element = e}
					       val currfirst_previous = case currfirst of
									    FLNode _ => last
									  | Node {previous,...} => previous
									  | _ => raise CorruptedRlist
					   in
					       first := node;
					       currfirst_previous := node;
					       dl
					   end
    | cons _ _ = raise CorruptedRlist

					       
  fun snip (dl as FLNode {first,last}) (node as Node {previous, next,...}) = 
      let val pnode = !previous
	  val nnode = !next				     
	  val pnodenext = case pnode of
			      FLNode _ => first
			    | Node {next,...} => next
			    | Nil => raise CorruptedRlist
	  val nnodeprevious = case nnode of 
				  FLNode _ => last
				| Node {previous,...} => previous
				| Nil => raise CorruptedRlist
      in
	  pnodenext := nnode;
	  nnodeprevious := pnode;
	  previous := Nil;
	  next := Nil
      end
    | snip _ _ = raise CorruptedRlist
	  
  fun isFirst (Node {previous,...}) = case !previous of
					  FLNode _ => true
					| _ => false

  fun isLast  (Node {next,...}) = case !next of
				       FLNode _ => true
				     | _ => false

  fun first (FLNode {first,...}) = !first
  fun last  (FLNode {last,...}) = !last
  fun element (Node {element,...}) = element

  fun size (FLNode {first,...}) = let fun count node n = case node of 
							     Node {next,...} => count (!next) (n+1)
							   | _ => n
				  in
				      count (!first) 0
				  end
					 
  local
      fun chain node 0 = (case node of
			      Node _ => node
			    | _ => raise IndexException)
	| chain node n = case node of
			     Node {next,...} => chain (!next) (n-1)
			   | _ => raise IndexException
  in
  fun nth (FLNode {first,...}) n = chain (!first) n
    | nth (node as Node _) n = chain node n
    | nth Nil _ = raise CorruptedRlist
  end
	     
  fun foreach (FLNode {first,last}) proc = (case !first of
						Node {next,element,...} => let val _ = proc element
									   in
									       case !next of 
										   FLNode _ => ()
										 | _ => foreach (!next) proc
									   end
					      | FLNode _ => ()
					      | Nil => raise CorruptedRlist )
    | foreach (Node {next, element, ...}) proc = let val _ = proc element
						 in
						     case !next of
							 FLNode _ => ()
						       | Node _  => foreach (!next) proc
						       | Nil => raise CorruptedRlist
						 end
    | foreach Nil proc = raise CorruptedRlist
			       
  fun test () = let val l = mkEmpty()		  
		in
		    cons "ray" l;
		    cons "eve" l;
		    cons "cory" l;
		    l
		end 
end
*)
