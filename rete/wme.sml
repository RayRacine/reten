functor WmeFn (structure S: SYMBOL
               structure B: BINDING) : WME  =
struct
  
  structure TL = TokenLists
		 
  structure S = S
  structure B = B
		
  structure D = Dump
  structure L = Layout
		
  type field = Binding.field
  type item_in_alpha = ReteTypes.item_in_alpha_memory
  type token = ReteNode.token
	       
  datatype wme = datatype ReteNode.wme
			  
  val log = Log.log (LoggerManager.ctorLogger ("WME",Log.DEBUG1))
	    
  fun addTokenToWme (wme,tok) =
      case wme of
	  Wme {tokens,...} =>  TL.TokenInWme.cons (tok,tokens)
	| NilWme => raise RT.BadNodeUsage "Adding tokento NilWme."
			  
  fun wmeField (Wme {subject,predicate,object, ...},fid) =
      case fid of
	  B.SUBJECT => subject
	| B.PREDICATE => predicate
	| B.OBJECT => object 
		      
  fun mkWme (subj, pred, obj) = 
      Wme {subject=subj, predicate=pred, object=obj,
	   alphaMemItems= ref [], 
	   tokens= BaseDList.make(), 
	   negativeJoinResults=ref []}
      
  val workingmem = ref ([]: wme list)
		   
  fun addWmeToWM awme =     
      workingmem := awme :: (!workingmem)
      
  fun addItemToWME (item,Wme {alphaMemItems, ...}) =
      alphaMemItems := item::(!alphaMemItems)
      
  fun workingMemoryAsList() = !workingmem
			      
  fun wmeAsString (Wme {subject,predicate,object,...}) = (S.toString subject, 
							  S.toString predicate, 
							  S.toString object)
    | wmeAsString NilWme = let val n = "Nil"
			   in
			       (n,n,n)
			   end
			   
  fun wmeEq ( Wme {subject=s1,predicate=p1, object=o1, ...}, 
	      Wme {subject=s2, predicate=p2, object=o2, ...} ) =
      S.symEq(s1,s2) andalso S.symEq(p1,p2) andalso S.symEq(o1,o2)
						    
  structure HK: HASH_KEY =
  struct
    type hash_key = wme
    fun hashVal w = let val (subj,pred,obj) = wmeAsString w
			val hk = HashString.hashString
		    in
			hk subj + hk pred + hk obj
		    end
		    
    fun sameKey (k1,k2) = wmeEq(k1,k2)
  end 
  
  fun layoutWME w = let val (subject,predicate,object) = wmeAsString w
		    in
			L.seq [L.str "WME ", L.record [("Subject",L.str subject),
						       ("Predicate",L.str predicate),
						       ("Object",L.str object)]]
		    end
		    
  fun layoutWMEs wmes = L.align (List.map layoutWME wmes)
			
end

structure Wme = WmeFn(structure S = Symbol 
                      structure B = Binding)
		
