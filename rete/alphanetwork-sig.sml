signature ALPHA_NETWORK =
sig
    
    exception NonExistentAlphaMemory

    structure C: CONDITION
    structure RN: RETE_NODE
	      
    type amkey
    type alpha_network
	 
    val varSym: string
    val lookup: alpha_network * amkey -> RN.alpha_node
    val find: alpha_network * amkey -> RN.alpha_node option
    val insert: alpha_network -> amkey * RN.alpha_node -> unit
    val conditionToAMKey: C.condition -> amkey
    val mkAlphaNetwork: int -> alpha_network
    val listAlphaNodes: alpha_network -> RN.alpha_node list

    val buildShareAlphaMemory: alpha_network -> C.condition -> RN.alpha_node
					 
end



