structure BetaNetwork =
struct

structure BL = BaseDList
structure RN = ReteNode

datatype beta_network = BetaNetwork of RN.rete_node (* beta_node *)

(****************************************************************************** 
 * The BN is a single dummy beta topnode.
 * Some of the join/activation stuff iterates over a token list.
 * Items is a list of tokens.  So we seed items with one empty token list.
 * Note join stuff returns true on empty list so right side WMEs from the AM
 * feed right down.
 *******************************************************************************)

fun mkBetaNetwork() = BetaNetwork (RN.mkTopBetaNode())

fun betaTopNode (BetaNetwork bnode) = bnode


(*fun dumpBetaMemory (retenode,level) =
    let fun doKids kids level = print "FIX ME dumpBetaMemory\n" (* app (fn kid => dumpBetaMemory(kid,level)) kids *)
    in
	print "FIX ME dumpBetaMem\n"
	      (*	case retenode of
			    JoinNode n => (dumpJoinNode(n,level); doKids (#children n) (level+1))
			  | BetaNode n => (dumpBetaNode(n,level); doKids (#children n) (level+1))
			  | NilReteNode => ()
			  | ProductionNode _ => print "BM dump Prod node.\n" *)
    end    *)
	    
end

(* structure BetaNetwork = BetaNetworkFn (structure BL = BaseDList) *)

