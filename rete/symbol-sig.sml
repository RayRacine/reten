signature SYMBOL =
sig
    type symbol 

    val symEq: symbol * symbol -> bool
    val toString: symbol -> string
    val fromString: string -> symbol
    val sym: string -> symbol
end
