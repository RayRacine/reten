signature STATS =
sig

    type stats

    datatype event = CREATE | SHARE | DELETE

    val mkStats: unit -> stats
    val currentStats: unit -> stats
    val withStats: (stats * (unit -> 'a)) -> 'a
    val clearStats: stats -> unit

    val cntWme: stats *  event -> unit
    val cntProduction: stats * event -> unit
    val cntAlpha: stats * event -> unit
    val cntBeta: stats * event -> unit
    val cntJoin: stats * event -> unit

    type stattotals

    val alphaStats: stats -> stattotals
    val betaStats: stats -> stattotals
    val joinStats: stats -> stattotals
    val prodStats: stats -> stattotals
    val wmeStats: stats -> stattotals

    val layout: stats -> Layout.t

end


structure Stats :> STATS  =
struct
  
  type stat = {creates: int ref, shares: int ref, deletes: int ref}
  type stats = {alpha:stat, beta:stat, join:stat, prod:stat, wme:stat}
  type stattotals = {creates:int, shares:int, deletes:int}
	       
  datatype event = CREATE | SHARE | DELETE				   

  fun mkStat () = {creates = ref 0, shares = ref 0, deletes = ref 0}
		 
  fun mkStats () = {alpha=mkStat(),
		    beta=mkStat(),
		    join=mkStat(),
		    prod=mkStat(),
		    wme=mkStat()}
		  
  val currStats: stats ref = ref (mkStats ())

  fun currentStats () = ! currStats

  fun withStats (stats, thunk) = 
      let val oldStats = !currStats
      in
	  currStats := stats;
	  let val a = thunk ()
	  in
	      currStats := oldStats;
	      a
	  end handle exn => (currStats := oldStats; raise exn)
      end

				 
  fun adjustStat (cnt,{creates,shares,deletes},event) = 
    let fun adjustStat bucket cnt = 
	    bucket := (!bucket) + cnt
    in
	case event of
	    CREATE => adjustStat creates 1
	  | SHARE  => adjustStat shares 1
	  | DELETE => adjustStat deletes 1		      
    end
	
  local
      fun incStat (stat:stat,event) = adjustStat(1,stat,event)
  in
    fun cntAlpha ({alpha,...}:stats, event) = incStat(alpha,event)
    fun cntBeta ({beta,...}:stats, event) = incStat(beta,event)
    fun cntJoin ({join,...}:stats, event) = incStat(join,event)
    fun cntProduction ({prod,...}:stats, event) = incStat(prod,event)
    fun cntWme ({wme,...}:stats, event) = incStat(wme,event)
  end
	  
  local
      fun getStat {creates,shares,deletes} = {creates=(!creates),shares=(!shares),deletes=(!deletes)}
  in
    fun alphaStats ({alpha,...}:stats) = getStat alpha
    fun betaStats ({beta,...}:stats) = getStat beta
    fun joinStats ({join,...}:stats) = getStat join
    fun prodStats ({prod,...}:stats) = getStat prod
    fun wmeStats ({wme,...}:stats) = getStat wme
  end
    
  fun clearStat {creates,shares,deletes} =
      (creates := 0;
       shares  := 0;
       deletes := 0)
      
  fun clearStats {alpha,beta,join,wme,prod} =
      (clearStat alpha;
       clearStat beta;
       clearStat join;
       clearStat wme;
       clearStat prod)

  local

      structure LO = Layout

      val create = LO.str "creates = "
      val shares = LO.str "shares  = "
      val deletes = LO.str "deletes = "
  in
  
  fun layoutStat name (stat:stat) =
      let fun lo heading getter = LO.seq [heading, LO.str (Int.toString (!(getter stat)))]
      in
	  LO.align [LO.str name, 
		    LO.indent (LO.align [lo create #creates,
					 lo shares #shares,
					 lo deletes #deletes], 3)]
      end

  fun layout {alpha, beta, join, prod, wme} =
      LO.align [layoutStat "Alpha" alpha,
		layoutStat "Beta" beta,
		layoutStat "Join" join,
		layoutStat "Production" prod,
		layoutStat "Wme" wme]
  end

end

