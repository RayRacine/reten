signature WME =
sig 

    structure S: SYMBOL
    structure B: BINDING where type S.symbol = S.symbol

    type wme
(*    type item_in_alpha *)
    type token

    structure HK: HASH_KEY 

    val mkWme: S.symbol * S.symbol * S.symbol -> wme
    val values: wme -> {subject: S.symbol, predicate: S.symbol, object: S.symbol}


(*     val addItemToWME: item_in_alpha * wme -> unit *)
    val wmeField: wme * B.field -> S.symbol
    val addTokenToWme: wme * token -> unit	

    val wmeEq: wme * wme -> bool			  
				      
    val layout: wme -> Layout.t
    val layoutWMEs: wme list -> Layout.t

end
