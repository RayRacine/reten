structure WorkingMemory: WORKING_MEMORY = 
struct

  exception NonExistentWME

  structure RN = ReteNode
  structure WMKey = RN.Wme.HK
  structure WMMap = HashTableFn(WMKey)
  structure L = Layout
							    
  type wme_key = WMKey.hash_key
  type working_memory = RN.wme WMMap.hash_table
			
  val DEF_SZ = 5000
	       
  fun mkWM sz = WMMap.mkTable(Option.getOpt(sz,DEF_SZ), NonExistentWME)

  fun addWme wm wme =
	  case WMMap.find wm wme of
	      SOME _ => false
	    | NONE => (WMMap.insert wm (wme, wme); true)

  fun removeWme wm wme = (WMMap.remove wm wme; ())

  fun wmWmes wm = WMMap.listItems wm

  fun wmSize wm = WMMap.numItems wm

  fun writeWM(wm,os) = List.app (fn w => L.outputln (RN.Wme.layout w, TextIO.stdOut)) (WMMap.listItems wm)

  end 
  
(* structure WorkingMemory = WorkingMemoryFn(ReteNode) *)
