signature BASEDLIST =
sig
    exception EmptyList
	      
    datatype 'a dnode = 
	     NilDNode
	   | DNode of 'a
		      
    datatype 'a dlist = DList of {first: 'a dnode ref, 
				  last: 'a dnode ref,
				  size: int ref}
				 
    val make: unit -> 'a dlist
    val isEmpty: 'a dlist -> bool
    val first: 'a dlist -> 'a
    val last: 'a dlist -> 'a
    val swap: 'a dlist * 'a dlist -> unit
    val size: 'a dlist -> int
end

signature DLIST =
sig 
    type t
	 
    type 'a dlist

    val make: unit -> t dlist
    val isEmpty: t dlist -> bool
    val first: t dlist -> t
    val last: t dlist -> t
    val swap: t dlist * t dlist -> unit
    val size: t dlist -> int
    val cons: t * t dlist -> unit
    val pop: t dlist -> t
    val foreach: t dlist * (t -> unit) -> unit
    val hasOnlyOneElement: t dlist -> bool
    val snip: t * t dlist -> unit
    val findFirstSelected: t dlist * (t -> bool) -> t option
    val foldl: (t * 'b -> 'b) * 'b * t dlist -> 'b
    val foldr: (t * 'b -> 'b) * 'b * t dlist -> 'b
    val toList: t dlist -> t list
end

signature DNODE =
sig
    type t
    type 'a dnode
    val links: t -> t dnode ref * t dnode ref
end
    
structure BaseDList:> BASEDLIST =
struct
  exception EmptyList
	    
  datatype 'a dnode = 
	   NilDNode
	 | DNode of 'a
		    
  datatype 'a dlist = DList of {first: 'a dnode ref, 
				last: 'a dnode ref,
				size: int ref}
			       
  fun make() = DList {first = ref NilDNode, 
		      last = ref NilDNode,
		      size = ref 0}
	       
  fun isEmpty (DList {first,last,...}) = case (!first,!last) of
					     (NilDNode,NilDNode) => true
					   | _ => false		      
						  
  fun size (DList {size,...}) = !size
				
  fun getNode node = case node of
			 NilDNode => raise EmptyList
		       | DNode n => n
				    
  fun first (DList {first,...}) = getNode (!first)
				  
  fun last (DList {last,...}) = getNode (!last)

  fun swap (DList {first=f1,last=l1,size=s1}, DList {first=f2,last=l2,size=s2}) =
      let val ftmp = f1
	  val ltmp = l1
	  val stmp = s1
      in
	  f1 := !f2;
	  l1 := !l2;
	  f2 := !ftmp;
	  l2 := !ltmp;
	  s1 := !s2;
	  s2 := !stmp
      end

end

functor DList(structure L: DNODE where type 'a dnode = 'a BaseDList.dnode): DLIST =
struct

  structure BL = BaseDList

  exception EmptyList = BL.EmptyList

  datatype dlist = datatype BaseDList.dlist

  type t = L.t 

  val make = BL.make
  val isEmpty = BL.isEmpty
  val first = BL.first
  val last = BL.last
  val swap = BL.swap
  val size = BL.size
    
  fun inc sz = sz := !sz + 1

  fun dec sz = let val sz' = !sz - 1
	       in
		   if sz' < 0
		   then sz := 0
		   else sz := sz'
	       end

  fun cons (node, l as BL.DList {first,last,size,...}) = 
      (if isEmpty l
       then (first := BL.DNode node;
	     last  := BL.DNode node)
       else let val (_,next) = L.links node
		val BL.DNode oldfirst = !first
		val (prev,_) = L.links oldfirst
	    in
		first := BL.DNode node;
		next := BL.DNode oldfirst;
		prev := !first						    
	    end;
       inc size)
		      
  fun snip (node, l as BL.DList {first,last,size,...}) = 
      let val (prev,next) = L.links node
	  fun unlink() = (prev := BL.NilDNode;
			  next := BL.NilDNode)
      in
	  (case (!prev,!next) of
	       (BL.NilDNode, BL.NilDNode) => (first := BL.NilDNode;
				       last := BL.NilDNode;
				       unlink())
	     | (BL.NilDNode, BL.DNode nn) => (first := !next;
					      let val (nnprev,_) = L.links nn
					      in
						  nnprev := BL.NilDNode 
					      end;
					      unlink())
	     | (BL.DNode pn, BL.NilDNode) => (last := !prev;
					      let val (_,pnnext) = L.links pn
					      in
						  pnnext := BL.NilDNode
					      end;
					      unlink())
	     | (BL.DNode pn, BL.DNode nn) => let val (pnnext,_) = L.links pn
						 val (_, nnprev) = L.links nn
					     in
						 
						 pnnext := !next;
						 nnprev := !prev;
						 unlink()
					     end);
	  dec size
      end

  fun pop dl = 
      if isEmpty dl
      then raise EmptyList
      else let val top = first dl
	       val _ = snip (top, dl)
	   in
	       top
	   end

  fun foreach (BL.DList {first,...}, proc) = 
      let fun apply node = case node of
			       BL.NilDNode => ()
			     | BL.DNode n => let val _ = proc n
						 val (_,next) = L.links n
					     in
						 apply (!next)
					     end
      in
	  apply (!first)
      end 

  fun hasOnlyOneElement (BL.DList {first,...}) =
      case !first of
	  BL.DNode node => let val (_,next) = L.links node
			   in
			       case !next of
				   BL.NilDNode => true
				 | _ => false
			   end
	| BL.NilDNode => false 
		      
  fun findFirstSelected (BL.DList {first,...}, selector) =
      let fun test node = case node of
			      BL.NilDNode => NONE
			    | BL.DNode n => if (selector n) 
					    then SOME n
					    else let val (_,next) = L.links n
						 in
						     test (!next)
						 end
      in
	  test (!first)
      end
	  
  fun foldl (f, c, dl as BL.DList {first,...}) =
      let val max = ref 0
	  fun apply (node,accum) = (max := !max + 1;
				    if !max >= 100
				    then (print "DL.sml MAX escaping.\n"; max := 0; accum)
				    else case node of
					     BL.NilDNode => accum
					   | BL.DNode n => let val accum' = f(n,accum)
							       val (_,next) = L.links n
							   in
							       apply(!next,accum')
							   end)
      in
	  apply (!first,c)
      end
	  
  fun foldr (f, c, BL.DList {last,...}) = 
      let fun apply (node,accum) = case node of 
				       BL.NilDNode => accum
				     | BL.DNode n => let val (prev,_) = L.links n
						     in
							 apply(!prev, f (n, accum))
						     end
      in
	  apply (!last,c)
      end
	  
  fun toList dlst = List.rev(foldl (op ::,[],dlst)) 
		    
end


(* I NEED A UNIT TEST TO FINISH THIS *)
(*structure TList = 
struct

type 'a dnode = 'a BaseDList.dnode

datatype int_node = INode of {x: int, next: int_node dnode ref, prev: int_node dnode ref}
			     
structure IntDList = DList(structure L =  
			   struct
			     type t = int_node
			     datatype dnode = datatype BaseDList.dnode
			     fun links (INode {prev,next,...}) = (prev,next)
			   end)

val _ = let val dlst = BaseDList.make()
	    fun mkINode i = INode {x=i,prev=ref BaseDList.NilDNode,next=ref BaseDList.NilDNode}
	    fun nl() = print "\n";
	in
	    print "Test DList\n";
 	    IntDList.cons(mkINode 3,dlst); 
(*	    IntDList.cons(mkINode 2,dlst);
	    IntDList.cons(mkINode 1,dlst);*)
	    IntDList.foreach (dlst,fn (INode {x,...}) => print (Int.toString x));

	    nl();

	    let val cons = IntDList.foldl(op ::,[],dlst)
	    in
		List.app (fn (INode {x,...}) =>  print (Int.toString x)) cons
	    end;

	    nl();

	    let val cons = IntDList.foldr(op ::,[],dlst)
	    in
		List.app (fn (INode {x,...}) =>  print (Int.toString x)) cons
	    end;

	    nl();

	    let val sum = IntDList.foldl(fn (INode {x,...},accum) => x + accum,0,dlst)
	    in
		print "Sum: ";
		print (Int.toString sum)
	    end;	    
	    nl();

	    print ("Size: " ^ (Int.toString (List.length (IntDList.toList dlst))))
	end
		     
end
*)		     

			

