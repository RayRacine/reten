signature RULE =
sig
    
    structure C: CONDITION
		 
    type lhs
	 
    datatype rhs = 
	     Print of C.condition list |
	     Assert of C.condition list
		       
    type rule
	 
    datatype production_action = PRINT | ASSERT | RETRACT
					

    val mk: lhs -> rhs -> rule
    val mkLhs: C.condition list -> lhs	  
    val lhs: rule -> lhs
    val rhs: rule -> rhs
    val layoutProductionAction: production_action -> Layout.t
    val layoutRule: rule -> Layout.t
end
