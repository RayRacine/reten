signature WORKING_MEMORY =
sig
    
    structure RN: RETE_NODE

    type working_memory
	 
    val mkWM: int option -> working_memory
    val wmSize: working_memory -> int
    val addWme: working_memory -> RN.wme -> bool
    val removeWme: working_memory -> RN.wme -> unit
    val wmWmes: working_memory -> RN.wme list
    val writeWM: working_memory * TextIO.outstream -> unit
						 
end
