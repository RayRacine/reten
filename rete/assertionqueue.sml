structure AssertionQ =
struct

  structure Q = Queue
  
  type 'a assertion_queue = 'a Q.queue
		
  (* When a production node is activated and causes an Assertion
   * all assertions are collected in the assertQ. When all productions
   * in the current phase are done firing, then the WME's in the
   * assertQ are processed. *)
		
  fun mkAssertionQ() = Queue.mkQueue()
		       
  fun dequeue q = Queue.dequeue q
		  
  fun enqueue (q,w) = Queue.enqueue(q,w)
		      
  fun isEmpty q = Queue.isEmpty q

end

(* structure AssertionQ = AssertionQFn(ReteNode) *)
