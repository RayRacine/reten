structure Test = 
struct

structure RT = ReteTypes
structure BR = BaseRete
structure D = ReteDump
structure R = Rete
structure W = Wme

structure Simple = 
struct
val tc1 = BR.parseCondition ("?x","on","?y")
val tc2 = BR.parseCondition ("?y","left-of","?z")
val tc3 = BR.parseCondition ("?z", "color", "red")
	  
val tw1 = W.mkWme("B1","on","B2")
val tw2 = W.mkWme("B1","on","B3")
val tw3 = W.mkWme("B1", "color", "red")
val tw4 = W.mkWme("B2", "on", "table")
val tw5 = W.mkWme("B2", "left-of", "B3" )
val tw6 = W.mkWme("B2","color","blue")
val tw7 = W.mkWme("B3","left-of","B4")
val tw8 = W.mkWme("B3", "on", "table")
val tw9 = W.mkWme("B3", "color", "red")
	      
val tp1 = ([tc1,tc2,tc3], RT.Assert [])
val wm1 = [tw1,tw2,tw3,tw4,tw5,tw6,tw7,tw8,tw9]

end

val c1 = BR.parseCondition ("?x", "on", "?y")
val c1' = BR.parseCondition ("?x", "on", "?y")
val c2 = BR.parseCondition ("?x", "color", "red")
val c3 = BR.parseCondition ("?y", "color", "red")
val c4 = BR.parseCondition ("?x", "?y", "red")
val c5 = BR.parseCondition ("?x", "?y", "?z")
val c6 = BR.parseCondition ("?x", "shape", "round")
val c7 = BR.parseCondition ("?x", "texture", "smooth")

val w1 = W.mkWme ("ball", "color", "red")
val w2 = W.mkWme ("block", "color", "red")
val w3 = W.mkWme ("ball", "on", "block")
val w4 = W.mkWme ("ball", "shape", "round")
val w5 = W.mkWme ("ball", "texture", "smooth")

val a1 = RT.Assert [BR.parseCondition ("?x", "redOn", "?y")]

val p1 = ([c1,c2,c6,c7], RT.Assert [BR.parseCondition ("?x", "fallsFrom", "?y")])
val p2 = [c4] (* test that I get 2 matches, as 2 red things *)
val p3 = [c1]
	 
val rete = R.mkReteNetwork()

fun simpWM()= app (fn wme => R.addWME rete wme) Simple.wm1
fun simpTest() = R.addProduction rete Simple.tp1
	   
fun testIt() = let val wmes = [w5,w4,w3,w1]
	       in
		   R.addProduction rete p1;
		   app (fn wme => R.addWME rete wme) wmes
	       end


fun showIt() = (print "Rete AM\n";
		D.showAlphaMemory rete;
		print "\n";
		print "Rete BM\n";
		D.showBetaMemory rete)

fun go() = (simpTest();simpWM())

exception TestException of string

(***** WME Tests *****)
fun test_wmeField() =
    if W.wmeField RT.SUBJECT w1 = "ball" 
    andalso W.wmeField RT.PREDICATE w1 = "color"
    andalso W.wmeField RT.OBJECT w1 = "red"
    then true
    else false

(**** Alpha Network ****)

fun test_buildShareAlphaMemory() =
    let val an = BR.mkAlphaNetwork 5
    in
	BR.buildShareAlphaMemory an c1;
	BR.buildShareAlphaMemory an c2; (* c2 same c3 *)
	BR.buildShareAlphaMemory an c3;
	BR.buildShareAlphaMemory an c1';
	RT.ConditionMap.numItems an = 2
    end		


(**** JOIN Tests ****)
			   
fun test_getJoinTestsFromCondition() = 
    let val ans = [{distance=1,field1=RT.SUBJECT,field2=RT.SUBJECT},
		   {distance=0, field1=RT.PREDICATE, field2=RT.SUBJECT}]
	val ts = BR.getJoinTestsFromCondition c4 [c3,c2,c1]

	fun checkLists [] [] = true
	  | checkLists (r::results) (a::answers) = 
	    if r = a then checkLists results answers
	    else raise TestException "Failed"
	  | checkLists _ _ = raise TestException "Bad test data."
    in
	checkLists ts ans
    end
	

fun testPerformJoinTests () = true
(* failing because uses old toke = wme list *)
(* fun testPerformJoinTests() =
    let val ts = BR.getJoinTestsFromCondition c3 [c2,c1]
	val ws = [w2,w3]
    in
	(not (BR.performJoinTests(ts,ws,w1))) andalso BR.performJoinTests(ts,ws,w2)
    end *)

fun test_buildShareJoinNode() = false

fun id x = x
fun all() = List.all id
		     [test_getJoinTestsFromCondition(),
		      testPerformJoinTests(),
		      test_wmeField(),
		      test_buildShareAlphaMemory(),
		      test_buildShareJoinNode()]

end
		
