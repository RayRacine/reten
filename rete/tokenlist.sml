(* imperative double linked lists *)

structure TokenLists =
struct
  structure RT = ReteNode

  structure TokenInMemory = DList(structure L =
				  struct
				    type t = RT.token
				    datatype dnode = datatype BaseDList.dnode
						     fun links token = 
							 case token of
							     RT.Token {in_memory_prev,in_memory_next,...} => (in_memory_prev,in_memory_next)
							   | RT.NilToken => (print "===========> OOPS";raise RT.BadNodeUsage "Linking a NilToken")
				  end
				  structure B = BaseDList)

  structure TokenInParent = DList(structure L =
				  struct
				    type t = RT.token
				    datatype dnode = datatype BaseDList.dnode
						     fun links token = 
							 case token of
							     RT.Token {in_parent_prev,in_parent_next,...} => (in_parent_prev,in_parent_next)
							   | RT.NilToken => raise RT.BadNodeUsage "Linking a NilToken"
				  end
				  structure B = BaseDList)		       

  structure TokenInWme = DList(structure L =
			       struct
				 type t = RT.token
				 datatype dnode = datatype BaseDList.dnode
				 fun links token = 
				     case token of
					 RT.Token {in_wme_prev,in_wme_next,...} => (in_wme_prev,in_wme_next)
				       | RT.NilToken => raise RT.BadNodeUsage "Linking a NilToken"
			       end
			       structure B = BaseDList)

  structure TokenInResult = DList(structure L =
				  struct
				    type t = RT.token
				    datatype dnode = datatype BaseDList.dnode
				    fun links token  = 
					case token of
					    RT.Token {variant,...} => (case variant of
									   NONE => raise RT.BadNodeUsage "Token has not variant aspect."
									 | SOME {in_result_prev, in_result_next,...} => (in_result_prev,in_result_next))
					  | RT.NilToken => raise RT.BadNodeUsage "Linking a NilToken"
				   end
				   structure B = BaseDList)
							 
end
