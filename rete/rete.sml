structure Rete =

struct
  
  structure W = ReteNode.Wme
		
  structure RN = ReteNode
  structure P = Production
  structure AN = AlphaNetwork
  structure BN = BetaNetwork
  structure L = Layout
  structure D = Dump
  structure Q = AssertionQ
  structure WM = WorkingMemory
  structure S = Symbol
		 
  type wme = RN.wme
  type production = P.production
  type alpha_network = AN.alpha_network
  type beta_network = BN.beta_network

  datatype rete = Rete of {amem: alpha_network, bmem:beta_network}
			  
  val log = Log.log (LoggerManager.ctorLogger ("Rete",Log.DEBUG3))
	    
  val alphaSize = 5000
		  
  (* FIX ME setTop thing is for debugging right now *)
  fun mkReteNetwork sz = 
      let val rete = Rete {amem = AN.mkAlphaNetwork (Option.getOpt(sz,alphaSize)),
			   bmem = BN.mkBetaNetwork ()}
	  val Rete {bmem,...} = rete
      in
	  ReteNode.setTop (BN.betaTopNode bmem);
	  rete
      end
      
  val mkWme = RN.Wme.mkWme
	      
  fun addWME (Rete {amem,bmem,...}) wme = 
      let val {subject,predicate,object} = RN.Wme.values wme
	  val an = amem 
	  val var = AN.varSym
	  val sep = "_"
	  fun addOneCombo combo = 
	      let val am = AN.find(an,combo)
	      in
		  case am of
		      SOME am => ( log Log.DEBUG3 ("Found AM for WME in Rete with key: " ^ (AN.AMKey.toString combo));
				   RN.alphaMemoryActivation am wme)
		    | NONE => ( log Log.DEBUG1 ("AM not found in Rete using key: " ^ (AN.AMKey.toString combo)); 
				() )
	      end	      	      
	  val subj = S.toString subject
	  val pred = S.toString predicate
      in
	  log Log.DEBUG3 "Adding WME: ";
	  L.outputln(L.indent (W.layout wme, 1), TextIO.stdOut);
	  List.app addOneCombo  
	           [(var,var,var), 
		    (subj,var,var),
		    (subj,pred,var),
		    (subj,var,S.toString object),
		    (var,pred,var),
		    (var,pred,S.toString object),
		    (var,var,S.toString object),
		    (subj,pred,S.toString object)]
      end
      
  fun nextAssertion rete =
      if Q.isEmpty RN.assertQ
      then NONE
      else SOME (Q.dequeue RN.assertQ)
	   
  fun removeWME rete wme = () (* RN.removeWme rete wme *)
			   
  fun addProduction (rete as Rete {amem,bmem}) production = 
      P.addProduction (amem,bmem,production)
      
  fun removeProduction rete production = ()
					 
  fun showBetaMemory (Rete {bmem,...}) =
      let val q = Queue.mkQueue()

	  fun qnodes nodes = 
	      List.app (fn n => Queue.enqueue (q,n)) nodes

          (* Consider adding node coloring later to dump a node at most once. *)
	  fun bfw () = 
	      let val curr = Queue.dequeue q
	      in
		  Layout.outputln(Layout.indent(ReteNode.layout curr,2), TextIO.stdOut);
		  qnodes (ReteNode.getChildrenAsList curr);
		  if Queue.isEmpty q
		  then ()
		  else bfw()
	      end
      in
	  Queue.enqueue (q, BN.betaTopNode bmem);
	  bfw()
      end
      
  fun writeAlphaMemory (Rete {amem,...}, os) =
      let val ans = AN.listAlphaNodes amem
	  fun writeAM lo_am = Layout.outputln(Layout.indent(ReteNode.layoutAlphaNode lo_am,2), os)
      in
	  if List.null ans
	  then TextIO.output (os, "AlphaMemory is empty.\n")
	  else List.app writeAM ans
      end
      
  fun showStats os =
      Layout.outputln (Layout.indent (Stats.layout (Stats.currentStats ()),2), os)

end
