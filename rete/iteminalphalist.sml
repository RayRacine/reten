structure ItemInAlphaLists = 
struct
  structure RT = ReteTypes
  structure InWme = DList(structure L = 
			  struct			    
			    type t = RT.item_in_alpha_memory
			    datatype dnode = datatype BaseDList.dnode
			    fun links (RT.ItemInAlphaMemory {in_alpha_prev,in_alpha_next,...}) = (in_alpha_prev,in_alpha_next)
			  end
			  structure B = BaseDList)
  structure InAlpha = DList(structure L =
			    struct	    
			      type t = RT.item_in_alpha_memory
			      datatype dnode = datatype BaseDList.dnode
			      fun links (RT.ItemInAlphaMemory {in_alpha_prev,in_alpha_next,...}) = (in_alpha_prev,in_alpha_next)
			    end
			  structure B = BaseDList)
end
