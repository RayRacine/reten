structure Irc = 
struct

val nick = "rete_bot"
val user = "ray___"
val clienthost = "aragon"
val serverhost = "irc.freenode.net"
val realname = "n3_bot"
val port = 6666

fun logIn nick user clienthost serverhost port realname =
    let val realname' = ":" ^ realname			
	val localhost =  valOf(NetHostDB.getByName serverhost)
	val inaddr = NetHostDB.addr localhost
	val addr = INetSock.toAddr(inaddr, port)
	val sock = INetSock.TCP.socket()
		   
	fun call sock =
	    let
		val _ = Socket.connect(sock, addr)
		val _ = Socket.sendVec(sock, Word8VectorSlice.full (Byte.stringToBytes ("NICK " ^ nick ^ "\r\n")))
		val _ = Socket.sendVec(sock, Word8VectorSlice.full 
						 (Byte.stringToBytes 
						      ("USER " ^ user ^ " " ^ clienthost ^ " " ^ serverhost ^ " " ^ realname' ^ "\r\n")))
		val msg  = Socket.recvVec(sock, 1000)
		val text = Byte.bytesToString msg
		val msg1 = Socket.recvVec(sock, 1000)
		val text1 = Byte.bytesToString msg1
	    in
		print text;
		print text1;
		Socket.close sock
	    end
		handle x => (Socket.close sock; raise x)
    in
	call sock
    end
	handle OS.SysErr (msg, _) => raise Fail (msg ^ "\n")

fun test() = logIn nick user clienthost serverhost port realname
end
