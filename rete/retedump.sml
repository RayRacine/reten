structure ReteDump(*: sig

    val showBetaMemory: ReteTypes.rete -> unit

end*) =
struct

open ReteTypes

structure C = Condition
structure B = Binding
structure RT = ReteTypes
structure RS = ReteString
structure JNT = JoinNodeTest
structure WME = Wme
structure D = Dump
structure L = Layout

type join_node_test = JNT.join_node_test

fun dumpPattern a =
    print (C.patternToString a)
			
fun dumpConditions cs = 
    let fun dumpConditions' [] = ()
	  | dumpConditions' (c::[]) = print (C.conditionToString c)
	  | dumpConditions' (c::cs) = (print (C.conditionToString c);
				       dumpConditions' cs)
    in
	print "-Production: ";
	dumpConditions' cs;
	print " -> \n"
    end


fun wmeToString (RN.Wme w) =
    " Subject=" ^ (#subject w) ^
    " Predicate="  ^ (#predicate w) ^
    " Object=" ^ (#object w)
  | wmeToString RN.NilWme = " NilWme"

		    
fun dumpTokens(toks,lvl) = if BaseDList.isEmpty toks
			   then print "Tokens list is empty.  Nothing to dump.\n"
			   else TokenLists.TokenInParent.foreach(toks,fn t => L.outputln (ReteNode.layoutToken t,TextIO.stdOut))
				
fun dumpItemsInAlpha(items,level) = 
    let fun dmpItem item = let val RN.ItemInAlphaMemory {wme,...} = item
			   in
			       L.outputln (L.indent (Wme.layoutWME wme,level+1), TextIO.stdOut)
			   end
    in
	if BaseDList.isEmpty items
	then (D.indent (level+1); print "Items in Alpha is empty.\n")
	else ItemInAlphaLists.InAlpha.foreach(items,dmpItem)
    end 
					  
fun dumpJoinNodeTests (tests,level) =
    (print "DUMP JN\n";
     L.outputln(L.indent(JoinNodeTest.layoutJoinNodeTests tests,2), TextIO.stdOut))
										    
fun nid name id = name ^ (Int.toString id)

fun dumpReteNodeId reteNode = 
    case reteNode of 
	JoinNode n => print (nid "JN" (#guid n))
      | BetaNode n => print (nid "BN" (#guid n))
      | ProductionNode n => print (nid "PN" (#guid n))

fun dumpAlphaNode (AlphaNode alphaNode,level) = 
    (D.indent level;
     print ("AN" ^ (Int.toString (#guid alphaNode)) ^ ": ");
     dumpItemsInAlpha (#items alphaNode, level+1);
     D.indent level;
     print "Successors:";
     ReteNodeLists.ReteNodeInAlpha.foreach(#successors alphaNode,fn n => (print " "; dumpReteNodeId n));
     print "\n\n")
    
fun dumpJoinNode (RN.JoinNode {guid,alphaMemory,tests,...}, level) =
    (D.indent level;
     print ((nid "JN" guid) ^ ": \n");
     D.indent (level + 1);
     let val AlphaNode {guid,...} = alphaMemory
     in 	 
	 print ((nid "AM: " guid) ^ "\n")
     end;
     dumpJoinNodeTests(!tests,level+1)) 
    
(*fun dumpBetaNode ({guid,items,...},level) =
    (D.indent level;
     print ((nid "BN" guid) ^ ": \n");
     D.indent (level+1);
     print ("Items-" ^ (Int.toString (BaseDList.size items)) ^ ":\n");
     dumpTokens (items,level+2);
     print "\n")

fun dumpReteNode node =
    case node of
	NilReteNode => print "EMPTYNODE\n"
      | JoinNode jn => dumpJoinNode (node,0)
      | BetaNode bn => dumpBetaNode (bn,0)
      | ProductionNode _ => print "PROD NODE \n" *)


end
