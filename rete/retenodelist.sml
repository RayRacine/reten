(* imperative double linked lists *)

structure ReteNodeLists =
struct
  structure RT = ReteTypes
  structure ReteNodeInAlpha = DList(structure L =
				    struct				      
				    type t = RT.rete_node
				    datatype dnode = datatype BaseDList.dnode
				    fun links rnode =
					case rnode of
					    RT.JoinNode {in_alpha_prev,in_alpha_next,...} => (in_alpha_prev,in_alpha_next)
					  | RT.NegativeNode {in_alpha_prev,in_alpha_next,...} => (in_alpha_prev,in_alpha_next)
					  | _ => raise RT.BadNodeUsage "Invalid Rete node in alpha list"
				    end
  structure B = BaseDList)
			      
  structure ReteNodeInChildren = DList(structure L =
				       struct
				       type t = RT.rete_node
				       datatype dnode = datatype BaseDList.dnode
				       fun links rnode =
					   case rnode of
					       RT.BetaNode {in_children_prev,in_children_next,...} => (in_children_prev,in_children_next)
					     | RT.JoinNode {in_children_prev,in_children_next,...} => (in_children_prev,in_children_next)
					     | RT.ProductionNode {in_children_prev,in_children_next,...} => (in_children_prev,in_children_next)
					     | RT.NegativeNode {in_children_prev,in_children_next,...} => (in_children_prev,in_children_next)
					     | RT.NCCNode {in_children_prev,in_children_next,...} => (in_children_prev,in_children_next)
					     | RT.NCCPartnerNode {in_children_prev,in_children_next,...} => (in_children_prev,in_children_next)
				       end
  structure B = BaseDList)
				 
  structure ReteNodeInAllChildren = DList(structure L = 
					  struct
					  type t = RT.rete_node
					  datatype dnode = datatype BaseDList.dnode
					  fun links rnode =
					      case rnode of
						  (*	  RT.BetaNode {in_allchildren_prev,in_allchildren_next,...} => (in_allchildren_prev,in_allchildren_next) *)
						      _ => raise RT.BadNodeUsage "Invalid rete node in AllChildren list."
					  end
  structure B = BaseDList)
end


