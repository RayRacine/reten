signature RETE_NODE =
sig

    structure JNT: JOIN_NODE_TEST
    structure AQ: ASSERTION_QUEUE
    structure B: BINDING
    structure R: RULE
    structure Wme: WME
    structure S: SYMBOL

    type rete_node
    type alpha_node   
    type wme


    val mkAlphaMemory: unit -> alpha_node
    val mkTopBetaNode: unit -> rete_node
    val mkProductionNode: rete_node * R.production_action * B.wme_binding list -> rete_node
(*    val mkWme: S.symbol * S.symbol * S.symbol -> W.wme *)

    val removeWme: wme -> unit

    val assertQ: wme AQ.assertion_queue
    val buildShareBetaMemoryNode: rete_node -> rete_node
    val buildShareJoinNode: rete_node -> alpha_node -> JNT.join_node_test list -> rete_node
    val updateNewNodeWithMatchesFromAbove: rete_node -> unit
    val alphaMemoryActivation: alpha_node -> wme -> unit
    val addChild: rete_node * rete_node -> unit
    val getChildrenAsList: rete_node -> rete_node list
    val setTop: rete_node -> unit (* for debugging. remove later *)

    val layoutAlphaNode: alpha_node -> Layout.t
    val layout: rete_node -> Layout.t   
end 

