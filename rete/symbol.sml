structure Symbol: SYMBOL =
struct
  
  type symbol = string
		
  fun symEq (s1,s2) = s1 = s2

  fun toString s1 = s1

  fun fromString s = s

  fun sym s = s
end
