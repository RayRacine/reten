structure Main =
struct
  
  structure R = Rete
  structure RN = ReteNode
  structure NID = NodeId
  structure F = Flatten
  structure L = Literal
  structure U = Uri
  structure C = Condition
		
  val rete = R.mkReteNetwork (SOME 5000)
	     
  fun announce fname = (print "Parsing ";
			print fname;
			print "\n\n   ---------------  \n")
		       
  fun showIt() = (print "Rete AM\n";
		  R.writeAlphaMemory (rete,TextIO.stdOut);
		  print "\n";
		  print "Rete BM\n";
		  R.showBetaMemory rete;
		  print "Stats \n";
		  R.showStats TextIO.stdOut)
		 
  fun flatnodeToCondition fnode =
      let val mk = Symbol.fromString
	  val const = C.Constant o mk
	  val var = C.Variable o mk
      in
	  case fnode of
	      F.UriRef u => (const o U.toString) u
	    | F.Anon nid => (const o NID.toString) nid
	    | F.Literal l => (const o L.toString) l
	    | F.Variable v => var v
	    | F.Formula _ => raise Fail "formula"
      end
      
  fun tripleToCondition (F.Flat {subj, pred, obj}) = 
      let val c1 = flatnodeToCondition subj
	  val c2 = flatnodeToCondition pred
	  val c3 = flatnodeToCondition obj
      in
	  C.mkCondition (c1,c2,c3)
      end
      
  fun formulaToConditions formula = 
      List.map tripleToCondition formula
      
  fun addWme (F.Flat {subj, pred, obj}) =
      case subj of
	  F.Formula form => let val lhs = formulaToConditions form
				val rhs = case obj of
					      F.Formula form => formulaToConditions form
					    | _ => raise Fail "Ill formed rule."
				val rule = Rule.mk (Rule.mkLhs lhs) (Rule.Assert rhs)
				val production = Production.mkProduction rule
			    in
				R.addProduction rete production
			    end
	| _ => let val swme = F.toSymbol subj
		   val pwme = F.toSymbol pred
		   val owme = F.toSymbol obj
	       in
		   R.addWME rete (R.mkWme (swme,pwme,owme))
	       end
	       
  fun main (_,argv) =
      let val infile = List.hd (argv)
	  val ast = N3Parser.n3ParseFile infile
      in
	  Layout.outputln (N3Ast.layoutDocument ast, TextIO.stdOut);
	  print "-------------------------\n"; 
          let val trips = Flatten.document ast
	  in
	      List.app (fn t => Layout.outputln (Flatten.layout t, TextIO.stdOut)) trips;
	      print "\n";
	      List.app addWme trips;
	      showIt()
	  end
      end
end


val _ = Main.main(CommandLine.name(), CommandLine.arguments())
	
