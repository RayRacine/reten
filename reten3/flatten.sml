
(* Flatten an N3 parsed AST to a list of triples.
 * A Formula is a list of triples. *)

structure Flatten : sig
    structure N3A: N3_AST

    structure U: URI
    structure NID: NODE_ID
    structure Q: QNAME
    structure L: LITERAL
    structure S: SYMBOL

    type variable = string

    datatype flattriple = Flat of {subj: flatnode,
				   pred: flatnode,
				   obj: flatnode}

    and flatnode = UriRef of U.uri
		 | Anon of NID.node_id
		 | Literal of L.literal
		 | Variable of string
		 | Formula of flattriple list

    val formulae: flatnode -> flattriple list

    val fromUri: U.uri -> flatnode
    val fromAnon: NID.node_id -> flatnode
    val fromVariable: variable -> flatnode
    val fromLiteral: L.literal -> flatnode

    val toSymbol: flatnode -> S.symbol

    val statement: N3A.statement -> flattriple list
    val document: N3A.document -> flattriple list
   
    val isScalar: flatnode -> bool

    val flatnodeToString: flatnode -> string
    val layout: flattriple -> Layout.t

end =

struct

structure N3A = N3Ast
structure NID = NodeId
structure U = Uri
structure Q = QName
structure L = Literal
structure S = Symbol

structure LO = Layout

type variable = string

datatype flattriple = Flat of {subj: flatnode,
			       pred: flatnode,
			       obj: flatnode}

and flatnode = UriRef of U.uri
	     | Anon of NID.node_id
	     | Literal of L.literal
	     | Variable of string
	     | Formula of formulae
withtype formulae = flattriple list

fun fromUri u = UriRef u

fun fromAnon nid = Anon  nid

fun fromLiteral l = Literal l

fun fromVariable v = Variable v

fun toSymbol fnode = 
    let val mk = Symbol.fromString 
    in
	case fnode of
	    UriRef u => (mk o U.toString) u
	  | Anon nid => (mk o NID.toString) nid
	  | Literal l => (mk o L.toString) l
	  | Variable v => mk v
	  | Formula _ => raise Fail "formula"
    end

fun symbol s = case s of
		   N3A.S.Uri u => UriRef u
		 | N3A.S.QName q => UriRef (U.fromString (Q.toString q))
				
fun node node = case node of 
		    N3A.Symbol s => (symbol s, [])
		  | N3A.Numeric n => (Literal (L.fromReal n),[])
		  | N3A.Literal (l,lang) => (Literal (L.plain l NONE), []) (* fix me NONE *)
		  | N3A.Anonymous props => (UriRef (Uri.fromString "Anon"),[])
		  | N3A.PathList plist => (Literal (L.plain "PathList" NONE), [])
		  | N3A.Formula content => (Formula (List.concat (map statement content)), [])
		  | N3A.Variable v => (Variable v, [])
			      
and canonicalize s = s

and symbolToExplicitUri symbol = canonicalize symbol
			 
and segments (n, [], accum) = (n, [], accum)
  | segments (n, s::segs, accum) = 
      let val nid = NID.generate()
      in
	  case s of
	      N3A.Bang n' => let val subj = n
				 val pred = let val (n,_) = node n' in n end
				 val obj = Anon nid
				 val triple = Flat {subj=subj, pred=pred, obj=obj}
			     in
				 segments (Anon nid, segs, triple::accum)
			     end
			     
	    | N3A.Hat n' => let val subj = Anon nid
				val pred = let val (n,_) = node n' in n end
				val obj = n
				val triple = Flat {subj=subj,pred=pred,obj=obj}
			    in
				segments (Anon nid, segs, triple::accum)
			    end
      end
				   
and path (N3A.Path (n,segs)) = 
      let val (fnode,trips) = node n
	  val (node,_,trips) = segments (fnode, segs, trips)
      in
	  (node,trips)
      end
      		  
and declaration _ = []

and existential _ = []

and universal _ = []
				
and verb v = case v of 
		 N3A.VPath p => path p
	       | N3A.AKW => (UriRef RDFSyntax.rdfType, [])
	       (* need to mirror reverse triple *)
	       | N3A.IsOfKW p => path p 
	       | N3A.HasKW p => path p
	       | N3A.EQUAL => (UriRef RDFSyntax.owlSameAs, [])
	       (* need to mirror reverse triple *)
	       | N3A.LEFT_DARROW => (UriRef RDFSyntax.logImplies, []) 
	       | N3A.RIGHT_DARROW => (UriRef RDFSyntax.logImplies, [])
		 
and property (N3A.Property (pred, paths)) = let val (vpred, vtrips) = verb pred
						fun flattenPath [] objects triples = (objects, triples)
						  | flattenPath (p::ps) objects triples = let val (node,trips) = path p      
											  in
											      flattenPath ps (node::objects) (trips @ triples)
											  end					  
						val (objects, triples) = flattenPath paths [] vtrips
					    in
						((vpred, objects), triples)
					    end 

and statement s : flattriple list  = 
    case s of 
	N3A.Declaration d => declaration d
      | N3A.Universal u => universal u
      | N3A.Existential e => existential e
      | N3A.SimpleStatement (subjectPath, propertylist) => let val (subj, pathtriples) = path subjectPath
							       fun flattenProps props accum = 
								   case props of 
								       [] => accum
								     | (p::ps) => let val (pos, trips) = property p
										      val flatpos = let val (p,os) = pos
												    in
													map (fn ano => Flat {subj=subj,pred=p,obj=ano}) os
												    end
										  in 
										      flattenProps ps (flatpos @ accum)
										  end
							   in
							       flattenProps propertylist []
							   end	
							   
and document stmts =
    let fun fDoc (N3A.Statements [], accum) = List.rev accum
	  | fDoc (N3A.Statements (s::ss), accum) = fDoc (N3A.Statements ss, statement s @ accum)
    in
	fDoc (stmts,[])
    end     

fun formulae flatnode =
    case flatnode of
	Formula f => f
      | _ => raise (Fail "Node is not a Formula.")

fun isScalar flatnode = 
    case flatnode of
	Formula _ => false
      | _ => true

fun flatnodeToString fnode = 
    case fnode of
	UriRef u => U.toString u
      | Anon nid => NID.toString nid
      | Literal l => L.toString l
      | Variable v => v
      | Formula content => raise Fail "Node is not a scalar node."

local 
    val space = LO.str " "
    val eos = LO.str "."
    val lcurl = LO.str "{"
    val rcurl = LO.str "}"
in
    
fun layout (Flat {subj,pred,obj})= 
    let fun lonode fnode = case fnode of
			       UriRef u => U.layout u
			     | Anon nid => NID.layout nid
			     | Literal l => L.layout l
			     | Variable v => LO.str v
			     | Formula content => LO.seq [lcurl, LO.align (map layout content), rcurl]
    in
	LO.seq [lonode subj, space,
		lonode pred, space,
		lonode obj, space, eos]
    end
    
end


end
